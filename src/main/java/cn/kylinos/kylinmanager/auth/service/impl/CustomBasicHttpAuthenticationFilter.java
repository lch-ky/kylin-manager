package cn.kylinos.kylinmanager.auth.service.impl;

import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.RedisUtil;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class CustomBasicHttpAuthenticationFilter extends BasicHttpAuthenticationFilter {
    public static final String ATTEMPT_LIMIT_PREFIX = "login-attempt-limit:";

    public static final Integer ATTEMPT_LIMIT = 10;

    public static final Integer ATTEMPT_LIMIT_LOCK_TIME = 300; // Lock 5 minutes

    @Override
    protected String[] getPrincipalsAndCredentials(String authorizationHeader, ServletRequest request) {
        return super.getPrincipalsAndCredentials(authorizationHeader, request);
    }

    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        return super.executeLogin(request, response);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        String accessIp = request.getRemoteAddr();
        String username = "_";
        String limitKey = ATTEMPT_LIMIT_PREFIX + username + ":" + accessIp;
        int attemptTimes = Integer.parseInt(RedisUtil.get(limitKey, "0"));

        if (attemptTimes > ATTEMPT_LIMIT ) {
            // Login attempt exceeded
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.setStatus(200);
            httpServletResponse.setContentType("application/json;charset=utf-8");

            PrintWriter out = httpServletResponse.getWriter();
            RestResponse rs = RestResponse.createErrorResponse(MsgCodes.RESPCODE_TOO_MANY_LOGIN_ATTEMPTS);
            out.println(JSON.toJSON(rs));
            out.flush();
            out.close();
            return false;
        } else if (!super.onAccessDenied(request, response)) {
            RedisUtil.incr(limitKey, ATTEMPT_LIMIT_LOCK_TIME);
            RestResponse rs = RestResponse.createErrorResponse("401");

            // Login failed
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.setStatus(200);
            httpServletResponse.setContentType("application/json;charset=utf-8");

            PrintWriter out = httpServletResponse.getWriter();
            out.println(JSON.toJSON(rs));
            out.flush();
            out.close();
            return false;
        } else {
            RedisUtil.delete(limitKey);
            return true;
        }
    }

}
