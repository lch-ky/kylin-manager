package cn.kylinos.kylinmanager.auth.service.impl;

import cn.kylinos.kylinmanager.auth.dao.UserRepository;
import cn.kylinos.kylinmanager.auth.model.User;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserUtils {
    private static UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        UserUtils.userRepository = userRepository;
    }

    public static User getCurrentUser() {
        String username = (String) SecurityUtils.getSubject().getPrincipal();
        User user = userRepository.findByUsername(username);
        return user;
    }
}
