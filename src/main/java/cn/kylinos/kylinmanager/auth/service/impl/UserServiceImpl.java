package cn.kylinos.kylinmanager.auth.service.impl;

import cn.kylinos.kylinmanager.auth.dao.UserRepository;
import cn.kylinos.kylinmanager.auth.model.User;
import cn.kylinos.kylinmanager.auth.service.UserService;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    @RequiresRoles("admin")
    @Transactional
    public SearchResult<User> findUsers(String username, SearchParam searchParam) {
        return AdvancedSearch.findBySearch(hasUser(username), searchParam, userRepository);
    }

    static Specification<User> hasUser(String username) {
        return (user, cq, cb) -> cb.equal(user.get("username"), username);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
