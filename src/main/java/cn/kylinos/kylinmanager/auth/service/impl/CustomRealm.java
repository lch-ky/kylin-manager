package cn.kylinos.kylinmanager.auth.service.impl;

import cn.kylinos.kylinmanager.auth.dao.RoleRepository;
import cn.kylinos.kylinmanager.auth.dao.UserRepository;
import cn.kylinos.kylinmanager.auth.model.Role;
import cn.kylinos.kylinmanager.auth.model.User;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class CustomRealm extends AuthorizingRealm {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;


    /**
     * 认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        User user = this.userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }
        String password = user.getPasswordHash();
        String salt = user.getPasswordSalt();
        return new SimpleAuthenticationInfo(username, password, ByteSource.Util.bytes(salt), getName());
    }

    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        String username = (String) super.getAvailablePrincipal(principalCollection);
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        User user = this.userRepository.findByUsername(username);
        if (user == null) {
            return authorizationInfo;
        }
        Set<Role> roles = user.getRoles();
        Set<String> roleStrs = new HashSet<>();
        if (roles != null) {
            roles.forEach(role -> roleStrs.add(role.getName()));
        }
        authorizationInfo.setRoles(roleStrs);
        roleStrs.forEach(roleStr -> {
            Role role = this.roleRepository.findByName(roleStr);
            if (role == null) {
                authorizationInfo.addStringPermissions(new HashSet<>());;
            } else {
                Set<String> permissionStrs = new HashSet<>();
                if (role.getPermissions() != null) {
                    role.getPermissions().forEach(permission -> permissionStrs.add(permission.getName()));
                }
                authorizationInfo.addStringPermissions(permissionStrs);
            }
        });
        return authorizationInfo;
    }

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        matcher.setHashAlgorithmName("SHA-512");
        matcher.setHashIterations(1024);
        super.setCredentialsMatcher(matcher);
    }
}

