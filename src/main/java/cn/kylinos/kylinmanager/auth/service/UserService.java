package cn.kylinos.kylinmanager.auth.service;

import cn.kylinos.kylinmanager.auth.model.User;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;

public interface UserService {
    SearchResult<User> findUsers(String username, SearchParam searchParam);

    User findByUsername(String username);
}
