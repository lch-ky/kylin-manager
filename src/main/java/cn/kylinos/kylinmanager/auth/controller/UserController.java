package cn.kylinos.kylinmanager.auth.controller;

import cn.kylinos.kylinmanager.auth.service.UserService;
import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.util.MsgUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @Operation(summary = "获取用户列表",
            responses = {@ApiResponse(description = "获取用户列表", content = @Content(mediaType = "application/json"))})
    @GetMapping("/list")
    public RestResponse list(@Parameter(description = "用户名") @RequestParam(name = "username") String username,
                             @Parameter(description = "列表参数") SearchParam searchParam) {
        return RestResponse.createSuccessResponse(userService.findUsers(username, searchParam));
    }

    @GetMapping("/locale")
    public String locale() {
        Locale locale = LocaleContextHolder.getLocale();
        log.info("Locale = {}", locale);
        return MsgUtil.get(locale, "welcome.message");
    }
}
