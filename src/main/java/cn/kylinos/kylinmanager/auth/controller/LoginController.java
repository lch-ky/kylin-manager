package cn.kylinos.kylinmanager.auth.controller;

import cn.kylinos.kylinmanager.auth.dto.LoginInputDTO;
import cn.kylinos.kylinmanager.auth.dto.LoginUserDTO;
import cn.kylinos.kylinmanager.auth.model.Permission;
import cn.kylinos.kylinmanager.auth.model.Role;
import cn.kylinos.kylinmanager.auth.model.User;
import cn.kylinos.kylinmanager.auth.service.UserService;
import cn.kylinos.kylinmanager.auth.service.impl.CustomBasicHttpAuthenticationFilter;
import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.RedisUtil;
import com.alibaba.fastjson.JSONObject;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class LoginController {

    private static final transient Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserService userService;

    @Operation(summary = "登录接口",
            responses = {@ApiResponse(description = "用户登录", content = @Content(mediaType = "application/json"))})
    @PostMapping("/login")
    public RestResponse login(ServletRequest request,
                              @Parameter(description = "注册输入") @RequestBody LoginInputDTO loginInputDTO){
        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();

        if (!currentUser.isAuthenticated()) {
            String accessIp = request.getRemoteAddr();
//            String username = params.getString("username");
            String limitKey = CustomBasicHttpAuthenticationFilter.ATTEMPT_LIMIT_PREFIX + "_:" + accessIp;
            int attemptTimes = Integer.parseInt(RedisUtil.get(limitKey, "0"));
            if (attemptTimes > CustomBasicHttpAuthenticationFilter.ATTEMPT_LIMIT) {
                return RestResponse.createErrorResponse(MsgCodes.RESPCODE_TOO_MANY_LOGIN_ATTEMPTS);
            }

            try {
                currentUser.login(new UsernamePasswordToken(loginInputDTO.getUsername(), loginInputDTO.getPassword()));
            } catch (IncorrectCredentialsException e) {
                RedisUtil.incr(limitKey, CustomBasicHttpAuthenticationFilter.ATTEMPT_LIMIT_LOCK_TIME);
                return RestResponse.createErrorResponse(MsgCodes.RESPCODE_FAILED_LOGIN);
            }
            RedisUtil.delete(limitKey);
        }

        User user = userService.findByUsername(loginInputDTO.getUsername());

        LoginUserDTO loginUserDTO = modelMapper.map(user, LoginUserDTO.class);
        loginUserDTO.setSessionId(session.getId().toString());
        loginUserDTO.setRoles(user.getRoles().stream().map(Role::getName).collect(Collectors.toSet()));
        Set<String> permissions = new HashSet<String>();
        for (Role role : user.getRoles()) {
            permissions.addAll(role.getPermissions().stream().map(Permission::getName).collect(Collectors.toSet()));
        }
        loginUserDTO.setPermissions(permissions);
        return RestResponse.createSuccessResponse(loginUserDTO);
    }

    @Operation(summary = "注销接口",
            responses = {@ApiResponse(description = "用户注销", content = @Content(mediaType = "application/json"))})
    @GetMapping("/logout")
    public RestResponse logout() {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        return RestResponse.createSuccessResponse(null);
    }

}
