package cn.kylinos.kylinmanager.auth.model;

import cn.kylinos.kylinmanager.util.search.Searchable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User implements Serializable, Searchable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "mail")
    private String mail;

    @Column(name = "password_hash")
    @JsonIgnore
    private String passwordHash;

    @Column(name = "password_salt")
    @JsonIgnore
    private String passwordSalt;

    @Column(name = "locale")
    private String locale;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "last_login_on")
    private Instant lastLoginOn;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updatedAt")
    private Instant updated_at;

    @Column(name = "comment")
    private String comment;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "role_users", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public static Map<String, String> aliases() {
        Map<String, String> aliases = new HashMap<String, String>();
        aliases.put("name", "username");
        return aliases;
    }
}
