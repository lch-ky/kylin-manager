package cn.kylinos.kylinmanager.auth.dao;

import cn.kylinos.kylinmanager.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}
