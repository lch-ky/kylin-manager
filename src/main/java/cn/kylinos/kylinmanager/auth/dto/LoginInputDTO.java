package cn.kylinos.kylinmanager.auth.dto;

import lombok.Data;

@Data
public class LoginInputDTO {
    private String username;

    private String password;
}
