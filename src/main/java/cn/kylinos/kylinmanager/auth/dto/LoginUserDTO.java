package cn.kylinos.kylinmanager.auth.dto;

import lombok.Data;

import java.time.Instant;
import java.util.Set;

@Data
public class LoginUserDTO {
    private Integer id;

    private String username;

    private String firstname;

    private String lastname;

    private String locale;

    private Boolean active;

    private Instant lastLoginOn;

    private Set<String> roles;

    private Set<String> permissions;

    private String sessionId;
}
