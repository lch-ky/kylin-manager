package cn.kylinos.kylinmanager.customscript.model;


import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "kylin_script_executions")
public class ScriptExecution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "host_id")
    private long hostId;

    @Column(name = "ip")
    private String ip;

    @Column(name = "command")
    private String command;

    @Column(name = "options")
    private String options;


    private long userId;

    private String effectiveUser;

    private long rCode;

    private String rContent;

    private long rRet;

    private String rStdout;

    private String rStderr;

    private long duration;

    private String state;

    private long jobInvocationGroupId;

    private long scriptLogId;



;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;


}
