package cn.kylinos.kylinmanager.customscript.dto;

import cn.kylinos.kylinmanager.customscript.model.ScriptTemplate;
import lombok.Data;

import java.time.Instant;
import java.util.Optional;

@Data
public class ScriptTemplateUpdateParam {
    private Integer id;

    private String name;

    private String description;

    private String content;

    private Instant createdAt;

    private Instant updatedAt;

    public void updateScriptTemplate(ScriptTemplate scriptTemplate) {
        Optional.of(id).ifPresent(scriptTemplate::setId);
        Optional.ofNullable(name).ifPresent(scriptTemplate::setName);
        Optional.ofNullable(description).ifPresent(scriptTemplate::setDescription);
        Optional.ofNullable(content).ifPresent(scriptTemplate::setContent);
        scriptTemplate.setCreatedAt(Instant.now());
        scriptTemplate.setUpdatedAt(Instant.now());
    }
}
