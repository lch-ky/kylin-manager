package cn.kylinos.kylinmanager.customscript.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class ScriptTemplateDTO {
    private Integer id;

    private String name;

    private String description;

    private String content;

    private Instant createdAt;

    private Instant updatedAt;

    private Integer userId;

    private String userName;
}
