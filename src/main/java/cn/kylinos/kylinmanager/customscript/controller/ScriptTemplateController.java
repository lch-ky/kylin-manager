package cn.kylinos.kylinmanager.customscript.controller;

import cn.kylinos.kylinmanager.common.RestResponse;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.customscript.dto.ScriptTemplateDTO;
import cn.kylinos.kylinmanager.customscript.dto.ScriptTemplateUpdateParam;
import cn.kylinos.kylinmanager.customscript.model.ScriptTemplate;
import cn.kylinos.kylinmanager.customscript.service.ScriptTemplateService;

import cn.kylinos.kylinmanager.util.MsgCodes;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/scripts")
@Slf4j
public class ScriptTemplateController {

    @Autowired
    private ScriptTemplateService scriptTemplateService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("")
    public RestResponse index(SearchParam searchParam) {
        SearchResult<ScriptTemplate> result = scriptTemplateService.index(searchParam);
        SearchResult<ScriptTemplateDTO> dtoResult = result.mapResults(scriptTemplate -> {
            ScriptTemplateDTO scriptTemplateDTO = modelMapper.map(scriptTemplate, ScriptTemplateDTO.class);
            scriptTemplateDTO.setUserId(scriptTemplate.getUser().getId());
            scriptTemplateDTO.setUserName(scriptTemplate.getUser().getUsername());
            return scriptTemplateDTO;
        });
        return RestResponse.createSuccessResponse(dtoResult);
    }

    @GetMapping("/isdisplay/{scriptTemplateId}")
    public RestResponse isDisplay(@PathVariable  Integer scriptTemplateId) {
        return RestResponse.createSuccessResponse(scriptTemplateService.isDisplay(scriptTemplateId));
    }

    @GetMapping("/{scriptTemplateId}")
    public RestResponse index(@PathVariable Integer scriptTemplateId) {
        ScriptTemplate scriptTemplate = scriptTemplateService.findScriptTemplateIdById(scriptTemplateId);
        if (scriptTemplate == null) {
            return RestResponse.createErrorResponse(MsgCodes.RESPCODE_PRODUCT_NOT_FOUND);
        }
        ScriptTemplateDTO scriptTemplateDTO = modelMapper.map(scriptTemplate, ScriptTemplateDTO.class);
        scriptTemplateDTO.setUserId(scriptTemplate.getUser().getId());
        scriptTemplateDTO.setUserName(scriptTemplate.getUser().getUsername());
        return RestResponse.createSuccessResponse(scriptTemplateDTO);
    }

    @PostMapping()
    public RestResponse create(@RequestBody ScriptTemplate scriptTemplate) {
        ScriptTemplate result = scriptTemplateService.create(scriptTemplate);
        ScriptTemplateDTO scriptTemplateDTO = modelMapper.map(result, ScriptTemplateDTO.class);
        scriptTemplateDTO.setUserId(result.getUser().getId());
        scriptTemplateDTO.setUserName(result.getUser().getUsername());
        return RestResponse.createSuccessResponse(scriptTemplateDTO);
    }

    @PutMapping("/{scriptTemplateId}")
    public RestResponse update(@PathVariable() Integer scriptTemplateId, @RequestBody ScriptTemplateUpdateParam scriptTemplateParam) {
        scriptTemplateParam.setId(scriptTemplateId);
        ScriptTemplate scriptTemplateNew = scriptTemplateService.update(scriptTemplateParam);
        ScriptTemplateDTO scriptTemplateDTO = modelMapper.map(scriptTemplateNew, ScriptTemplateDTO.class);
        scriptTemplateDTO.setUserId(scriptTemplateNew.getUser().getId());
        scriptTemplateDTO.setUserName(scriptTemplateNew.getUser().getUsername());
        return RestResponse.createSuccessResponse(scriptTemplateDTO);
    }

    @DeleteMapping("/{scriptTemplateId}")
    public RestResponse delete(@PathVariable Integer scriptTemplateId) {
        scriptTemplateService.delete(scriptTemplateId);
        return RestResponse.createSuccessResponse(null);
    }

    @PostMapping("/{scriptTemplateId}/run")
    public RestResponse run(@PathVariable Integer scriptTemplateId, @RequestBody String hostIp) {

        return RestResponse.createSuccessResponse(scriptTemplateService.run(scriptTemplateId,hostIp));
    }

}
