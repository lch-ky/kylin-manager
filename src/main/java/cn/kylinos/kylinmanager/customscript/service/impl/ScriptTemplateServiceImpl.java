package cn.kylinos.kylinmanager.customscript.service.impl;

import cn.kylinos.kylinmanager.auth.service.impl.UserUtils;
import cn.kylinos.kylinmanager.common.KylinException;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.customscript.dao.ScriptTemplateRepository;
import cn.kylinos.kylinmanager.customscript.dto.ScriptTemplateUpdateParam;
import cn.kylinos.kylinmanager.customscript.model.ScriptTemplate;
import cn.kylinos.kylinmanager.customscript.service.ScriptTemplateService;
import cn.kylinos.kylinmanager.logs.constants.LogConst;
import cn.kylinos.kylinmanager.logs.model.KylinParentLog;
import cn.kylinos.kylinmanager.logs.model.KylinSubLog;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.factory.SpringTaskManager;
import cn.kylinos.kylinmanager.springasynctask.task.ScriptSync;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.HashMap;

@Service
@Transactional
public class ScriptTemplateServiceImpl implements ScriptTemplateService {

    @Autowired
    private ScriptTemplateRepository scriptTemplateRepository;

    @Autowired
    private LogService logService;

    @Autowired
    private SpringTaskManager springTaskManager;

    public SearchResult<ScriptTemplate> index(SearchParam searchParam) {
        return AdvancedSearch.findBySearch(isDisplay(true), searchParam, scriptTemplateRepository);
    }

    static Specification<ScriptTemplate> isDisplay(Boolean display) {
        return (scriptTemplate, cq, cb) -> cb.equal(scriptTemplate.get("display"), display);
    }

    @Override
    public ScriptTemplate findScriptTemplateIdById(Integer scriptTemplateId) {
        return scriptTemplateRepository.findScriptTemplateById(scriptTemplateId);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class)
    public ScriptTemplate create(ScriptTemplate scriptTemplateIdParam) {

        scriptTemplateIdParam.setUser(UserUtils.getCurrentUser());
        scriptTemplateIdParam.setDisplay(true);
        scriptTemplateIdParam.setCreatedAt(Instant.now());
        scriptTemplateIdParam.setUpdatedAt(Instant.now());
        return scriptTemplateRepository.save(scriptTemplateIdParam);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class)
    public ScriptTemplate update(ScriptTemplateUpdateParam scriptTemplateIdParam) {
        ScriptTemplate scriptTemplate = scriptTemplateRepository.findScriptTemplateById(scriptTemplateIdParam.getId());
        if (scriptTemplate == null) {
            throw new KylinException(MsgCodes.RESPCODE_PRODUCT_NOT_FOUND);
        }

        scriptTemplateIdParam.updateScriptTemplate(scriptTemplate);
        return scriptTemplateRepository.save(scriptTemplate);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class)
    public void delete(Integer scriptTemplateId) {
        ScriptTemplate scriptTemplate = scriptTemplateRepository.findScriptTemplateById(scriptTemplateId);
        if (scriptTemplate == null) {
            throw new KylinException(MsgCodes.RESPCODE_PRODUCT_NOT_FOUND);
        }
        scriptTemplateRepository.deleteById(scriptTemplateId);
    }

    @Override
    public Boolean isDisplay(Integer scriptTemplateId) {
        return findScriptTemplateIdById(scriptTemplateId).getDisplay();
    }

    @Override
    public KylinParentLog run(Integer scriptTemplateId, String hostIp) {
        return run(scriptTemplateId, hostIp, 120);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class)
    public KylinParentLog run(Integer scriptTemplateId, String hostIp, Integer timeout) {
        ScriptTemplate scriptTemplate = scriptTemplateRepository.findScriptTemplateById(scriptTemplateId);
        if (scriptTemplate == null) {
            throw new KylinException(MsgCodes.RESPCODE_REPO_NOT_FOUND);
        }
        KylinParentLog parentLog = logService.createParentLogs(LogConst.ACTION_SCRIPT_SYNC, LogConst.STATUS_READY,
                LogConst.RESULT_PENDING, LogConst.MODULE_SCRIPT, null);
        KylinSubLog subLog = logService.createSubLogs(LogConst.ACTION_SCRIPT_SYNC,  LogConst.STATUS_READY,
                LogConst.RESULT_PENDING, LogConst.MODULE_SCRIPT, null, parentLog.getId(), null, null);
        HashMap<String, String> params = new HashMap<>(1);
        params.put("hostIp", hostIp);
        params.put("script", scriptTemplate.getContent());
        params.put("timeout", timeout.toString());
        try {
            springTaskManager.addTask(subLog.getId(), new ScriptSync(), params);
        } catch (Exception e) {
            logService.updateLogs(subLog.getId(), LogConst.STATUS_DONE, LogConst.RESULT_FAILED, e.getMessage(), e.getMessage());
            throw new KylinException(MsgCodes.RESPCODE_CREATE_TASK_FAILURE, e.getMessage());
        }
        return parentLog;
    }

}
