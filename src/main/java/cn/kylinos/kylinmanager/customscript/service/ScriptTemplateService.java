package cn.kylinos.kylinmanager.customscript.service;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.customscript.dto.ScriptTemplateUpdateParam;
import cn.kylinos.kylinmanager.customscript.model.ScriptTemplate;
import cn.kylinos.kylinmanager.logs.model.KylinParentLog;


public interface ScriptTemplateService {


    SearchResult<ScriptTemplate> index(SearchParam searchParam);

    ScriptTemplate findScriptTemplateIdById(Integer scriptTemplateId);

    ScriptTemplate create(ScriptTemplate scriptTemplateParam);

    ScriptTemplate update(ScriptTemplateUpdateParam scriptTemplateParam);

    void delete(Integer scriptTemplateId);

    Boolean isDisplay(Integer scriptTemplateId);

    KylinParentLog run(Integer productId, String hostIp);

    KylinParentLog run(Integer productId, String hostIp, Integer timeout);

}
