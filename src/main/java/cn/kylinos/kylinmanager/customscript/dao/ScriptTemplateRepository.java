package cn.kylinos.kylinmanager.customscript.dao;

import cn.kylinos.kylinmanager.customscript.model.ScriptTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScriptTemplateRepository extends JpaRepository<ScriptTemplate, Integer>, JpaSpecificationExecutor<ScriptTemplate> {
    //ScriptTemplate findByName(String name);

    //ScriptTemplate findByNameAndVersionAndCpuArch(String name, String version, String CpuArch);

    //List<ScriptTemplate> findKylinScriptTemplatesByNameOrUserIdOrContentOrDescription(String param, Integer param2, String param3, String param4);

    ScriptTemplate findScriptTemplateById(Integer id);

    ScriptTemplate findScriptTemplatesByDisplayTrue();
}
