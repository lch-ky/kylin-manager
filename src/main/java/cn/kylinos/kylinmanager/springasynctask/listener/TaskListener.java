package cn.kylinos.kylinmanager.springasynctask.listener;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.model.TaskResult;
import cn.kylinos.kylinmanager.util.SpringUtil;
import org.quartz.*;

public class TaskListener implements SchedulerListener, JobListener {

    private final String listenerName;

    private final LogService logService = SpringUtil.getBean(LogService.class);

    public TaskListener(String name)
    {
        listenerName = name;
    }


    @Override
    public void triggerFinalized(Trigger trigger) {
    }

    @Override
    public void triggerPaused(TriggerKey triggerKey) {
    }

    @Override
    public void triggersPaused(String s) {

    }

    @Override
    public void triggerResumed(TriggerKey triggerKey) {

    }

    @Override
    public void triggersResumed(String s) {
    }

    @Override
    public void jobAdded(JobDetail jobDetail) {

    }

    @Override
    public void jobDeleted(JobKey jobKey) {
        int logId = Integer.parseInt(jobKey.getName());
        logService.updateLogs(logId, "cancel", "done", "", "");
    }

    @Override
    public void jobPaused(JobKey jobKey) {
        int logId = Integer.parseInt(jobKey.getName());
        logService.updateLogs(logId, "pause", "done", "", "");
    }

    @Override
    public void jobsPaused(String s) {

    }

    @Override
    public void jobResumed(JobKey jobKey) {
        int logId = Integer.parseInt(jobKey.getName());
        logService.updateLogs(logId, "running", "", "","");
    }

    @Override
    public void jobsResumed(String s) {

    }

    @Override
    public void schedulerError(String s, SchedulerException e) {

    }

    @Override
    public void schedulerInStandbyMode() {

    }

    @Override
    public void schedulerStarted() {

    }

    @Override
    public void schedulerStarting() {

    }

    @Override
    public void schedulerShutdown() {

    }

    @Override
    public void schedulerShuttingdown() {

    }

    @Override
    public void schedulingDataCleared() {

    }

    @Override
    public void jobScheduled(Trigger trigger) {

    }

    @Override
    public void jobUnscheduled(TriggerKey triggerKey) {

    }

    @Override
    public String getName()
    {
        return listenerName;
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext jobExecutionContext) {
        int logId = Integer.parseInt(jobExecutionContext.getJobDetail().getKey().getName());
        logService.updateLogs(logId, "running", "done", "", "");
    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext jobExecutionContext) {

    }

    @Override
    public void jobWasExecuted(JobExecutionContext jobExecutionContext, JobExecutionException e) {
        int logId = Integer.parseInt(jobExecutionContext.getJobDetail().getKey().getName());
        TaskResult taskResult = (TaskResult) jobExecutionContext.getResult();
        logService.updateLogs(logId, taskResult.getResult(), "done", taskResult.getTitle(),
                taskResult.getMessage());
    }
}
