package cn.kylinos.kylinmanager.springasynctask.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskResult {
    private boolean result = true;
    private String title = "";
    private String message = "";

    public String getResult()
    {
        return result ?"success":"failed";
    }
}
