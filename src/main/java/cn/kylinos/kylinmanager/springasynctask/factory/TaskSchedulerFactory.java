package cn.kylinos.kylinmanager.springasynctask.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

@Component
public class TaskSchedulerFactory {


    @Autowired
    private JobFactory jobFactory;

    @Bean
    public SchedulerFactoryBean  initScheduler(DataSource dataSource)
    {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        try
        {
            Properties props = new Properties();
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("quartz.properties");
            props.load(in);

            schedulerFactoryBean.setQuartzProperties(props);
            schedulerFactoryBean.setDataSource(dataSource);
            schedulerFactoryBean.setJobFactory(jobFactory);
            schedulerFactoryBean.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return schedulerFactoryBean;
    }
}
