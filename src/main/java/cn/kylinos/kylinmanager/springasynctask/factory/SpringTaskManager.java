package cn.kylinos.kylinmanager.springasynctask.factory;

import cn.kylinos.kylinmanager.springasynctask.listener.TaskListener;
import cn.kylinos.kylinmanager.springasynctask.task.TaskBase;
import org.quartz.*;
import org.quartz.impl.matchers.KeyMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import java.util.HashMap;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@Component
public class SpringTaskManager {


    @Autowired
    SchedulerFactoryBean schedulerFactoryBean;

    public void addTask(int logId, TaskBase taskBase, HashMap<String, String> data) throws Exception
    {
        String taskGroup = String.format("%d", logId);
        String taskName = String.format("%d", logId);
        JobDataMap jobDataMap = new JobDataMap(data);
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        JobKey jobKey = new JobKey(taskName, taskGroup);
        JobDetail jobDetail = newJob(taskBase.getClass())
                .withIdentity(taskName, taskGroup)
                .usingJobData(jobDataMap)
                .build();
        TriggerKey triggerKey = new TriggerKey(taskName, taskGroup);
        Trigger trigger = newTrigger()
                .withIdentity(triggerKey)
                .startNow()
                .withSchedule(simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
        TaskListener taskListener = new TaskListener(taskName);
        Matcher<JobKey> matcher = KeyMatcher.keyEquals(jobKey);
        scheduler.getListenerManager().addSchedulerListener(taskListener);
        scheduler.getListenerManager().addJobListener(taskListener, matcher);
        scheduler.scheduleJob(jobDetail, trigger);
    }

    public void pauseTask(int logId) throws Exception
    {
        String taskGroup = String.format("%d", logId);
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        TriggerKey triggerKey = new TriggerKey(String.format("%d", logId), taskGroup);
        JobKey jobKey = new JobKey(String.format("%d", logId), taskGroup);
        Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
        if (triggerState == Trigger.TriggerState.NORMAL)
        {
            scheduler.pauseJob(jobKey);
        }
    }

    public void cancelTask(int logId) throws Exception
    {
        String taskGroup = String.format("%d", logId);
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        TriggerKey triggerKey = new TriggerKey(String.format("%d", logId), taskGroup);
        JobKey jobKey = new JobKey(String.format("%d", logId), taskGroup);
        Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
        if (triggerState == Trigger.TriggerState.NORMAL || triggerState == Trigger.TriggerState.PAUSED)
        {
            scheduler.deleteJob(jobKey);
        }
    }

    public void resumeTask(int logId) throws Exception
    {
        String taskGroup = String.format("%d", logId);
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        TriggerKey triggerKey = new TriggerKey(String.format("%d", logId), taskGroup);
        JobKey jobKey = new JobKey(String.format("%d", logId), taskGroup);
        Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
        if (triggerState == Trigger.TriggerState.PAUSED)
        {
            scheduler.resumeJob(jobKey);
        }
    }

    public void addTimedTask(int logId, TaskBase taskBase, HashMap<String, String> data, String timeRule) throws Exception
    {
        String taskGroup = String.format("%d", logId);
        String taskName = String.format("%d", logId);
        JobDataMap jobDataMap = new JobDataMap(data);
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        JobKey jobKey = new JobKey(taskName, taskGroup);
        JobDetail jobDetail = newJob(taskBase.getClass())
                .withIdentity(taskName, taskGroup)
                .usingJobData(jobDataMap)
                .build();

        TriggerKey triggerKey = new TriggerKey(taskName, taskGroup);
        Trigger trigger = newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(CronScheduleBuilder.cronSchedule(timeRule))
                .build();

        TaskListener taskListener = new TaskListener(taskName);
        Matcher<JobKey> matcher = KeyMatcher.keyEquals(jobKey);
        scheduler.getListenerManager().addSchedulerListener(taskListener);
        scheduler.getListenerManager().addJobListener(taskListener, matcher);
        scheduler.scheduleJob(jobDetail, trigger);
    }
}
