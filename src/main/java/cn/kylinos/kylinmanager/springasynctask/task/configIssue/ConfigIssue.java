package cn.kylinos.kylinmanager.springasynctask.task.configIssue;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.configitem.service.ConfigService;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.model.TaskResult;
import cn.kylinos.kylinmanager.springasynctask.task.TaskBase;
import cn.kylinos.kylinmanager.util.clientrpc.ClientRpcFactory;
import com.alibaba.fastjson.JSONObject;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ConfigIssue extends TaskBase {

    @Autowired
    private HostService hostService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private LogService logService;

    @Autowired
    private ClientRpcFactory clientRpcFactory;

    @Override
    public void executeInternal(JobExecutionContext jobExecutionContext)
    {
        TaskResult taskResult = new TaskResult();
        try
        {
            JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
            int hostId = Integer.parseInt(jobDataMap.getString("hostId"));
            KylinHost kylinHost = hostService.findHostById(hostId);
            List<Integer> configIds = JSONObject.parseArray(jobDataMap.getString("configIds")).toJavaList(Integer.class);
            List<KylinConfig> kylinConfigs = configService.findByIds(configIds);
            JSONObject issueJson = createIssueJson(kylinConfigs);
            clientRpcFactory.getClientInstance(kylinHost.getIp()).writeAll(issueJson);
        }
        catch (Exception e)
        {
            taskResult.setResult(false);
        }
        finally
        {
            jobExecutionContext.setResult(taskResult);
        }
    }

    private JSONObject createIssueJson(List<KylinConfig> kylinConfigs)
    {
        JSONObject issueJson = new JSONObject();
        issueJson.put("kernel", new JSONObject());
        issueJson.put("app", new JSONObject());
        issueJson.put("security", new JSONObject());
        for(KylinConfig kylinConfig: kylinConfigs)
        {
            String configType = kylinConfig.getConfigType();
            if("内核参数".equals(configType))
            {
                issueJson.getJSONObject("kernel").put(kylinConfig.getName(), kylinConfig.getBaseline());
            }
            else if("应用配置".equals(configType))
            {
                JSONObject app = issueJson.getJSONObject("app");
                if(app.get(kylinConfig.getAttribute()) == null)
                {
                    app.put(kylinConfig.getAttribute(), new JSONObject());
                }
                app.getJSONObject(kylinConfig.getAttribute()).put(kylinConfig.getName(), kylinConfig.getBaseline());
            }
            else if("安全配置".equals(configType))
            {
                JSONObject security = issueJson.getJSONObject("security");
                if(security.get(kylinConfig.getAttribute()) == null)
                {
                    security.put(kylinConfig.getAttribute(), new JSONObject());
                }
                security.getJSONObject(kylinConfig.getAttribute()).put(kylinConfig.getName(), kylinConfig.getBaseline());
            }
        }
        return issueJson;
    }
}
