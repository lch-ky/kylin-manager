package cn.kylinos.kylinmanager.springasynctask.task.scan;

import cn.kylinos.kylinmanager.common.config.ConfigStruct;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.host.model.KylinHostConfig;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.springasynctask.model.TaskResult;
import cn.kylinos.kylinmanager.springasynctask.task.TaskBase;
import cn.kylinos.kylinmanager.util.clientrpc.ClientRpcFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class HostScan extends TaskBase {

    @Autowired
    private HostService hostService;

    @Autowired
    private ClientRpcFactory clientRpcFactory;

    @Override
    public void executeInternal(JobExecutionContext jobExecutionContext)
    {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        TaskResult taskResult = new TaskResult();
        try
        {
            int hostId = Integer.parseInt(jobDataMap.getString("hostId"));
            String hostIp = hostService.findHostById(hostId).getIp();
            ConfigStruct configStruct = clientRpcFactory.getClientInstance(hostIp).getAllConfig();
            List<KylinHostConfig> kylinConfigList = configStruct.toList().stream().map(k -> toHostConfig(k, hostId))
                    .collect(Collectors.toList());
            hostService.createAndUpdateConfigs(hostId, kylinConfigList);

        }
        catch (Exception e)
        {
            taskResult.setResult(false);
        }
        finally
        {
            jobExecutionContext.setResult(taskResult);
        }

    }

    private KylinHostConfig toHostConfig(KylinConfig kylinConfig, Integer hostId)
    {
        KylinHostConfig kylinHostConfigs = new KylinHostConfig();
        kylinHostConfigs.setName(kylinConfig.getName());
        kylinHostConfigs.setConfigType(kylinConfig.getConfigType());
        kylinHostConfigs.setAttribute(kylinConfig.getAttribute());
        kylinHostConfigs.setBaseline(kylinConfig.getBaseline());
        kylinHostConfigs.setFilePath(kylinConfig.getFilePath());
        kylinHostConfigs.setDescription(kylinConfig.getDescription());
        kylinHostConfigs.setFlag(kylinConfig.getFlag());
        kylinHostConfigs.setHostId(hostId);
        return kylinHostConfigs;
    }
}
