package cn.kylinos.kylinmanager.springasynctask.task.agent;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.config.AgentConfig;
import cn.kylinos.kylinmanager.springasynctask.model.TaskResult;
import cn.kylinos.kylinmanager.springasynctask.task.TaskBase;
import cn.kylinos.kylinmanager.util.clientrpc.ClientRpcFactory;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

public class AgentUpdate extends TaskBase {

    @Autowired
    private HostService hostService;

    @Autowired
    private AgentConfig agentConfig;

    @Autowired
    ClientRpcFactory clientRpcFactory;

    @Override
    public void executeInternal(JobExecutionContext jobExecutionContext)
    {
        TaskResult taskResult = new TaskResult();
        try
        {
            JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
            int hostId = Integer.parseInt(jobDataMap.getString("hostId"));
            KylinHost kylinHost = hostService.findHostById(hostId);
            String packagePath = agentConfig.getAgentLastest();
            clientRpcFactory.getClientInstance(kylinHost.getIp()).update(packagePath, 0);
        }
        catch (Exception e)
        {
            taskResult.setResult(false);
        }
        finally
        {
            jobExecutionContext.setResult(taskResult);
        }

    }
}
