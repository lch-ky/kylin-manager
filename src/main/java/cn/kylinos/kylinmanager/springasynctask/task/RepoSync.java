package cn.kylinos.kylinmanager.springasynctask.task;

import cn.kylinos.kylinmanager.common.KylinException;
import cn.kylinos.kylinmanager.patch.constant.RepoConst;
import cn.kylinos.kylinmanager.patch.model.Repository;
import cn.kylinos.kylinmanager.patch.model.Rpm;
import cn.kylinos.kylinmanager.patch.model.RpmAttr;
import cn.kylinos.kylinmanager.patch.model.RpmDep;
import cn.kylinos.kylinmanager.patch.service.RepositoryService;
import cn.kylinos.kylinmanager.patch.service.RpmService;
import cn.kylinos.kylinmanager.springasynctask.model.TaskResult;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.MsgUtil;
import cn.kylinos.kylinmanager.util.repocontent.RepoContentUtils;
import cn.kylinos.kylinmanager.util.repoparser.RepomdParser;
import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class RepoSync extends TaskBase {
    private TaskResult taskResult;

    private static final Long TOTAL_SPACE_THRESHOLD = 1000*1024*1024L;
    private static final Long TOTAL_FREE_SPACE_THRESHOLD = 100*1024*1024L;
    private static final Double TOTAL_SPACE_WARN_THRESHOLD = 0.9;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RpmService rpmService;

    @Value("${repo.basePath}")
    private String repoBasePath;

    public RepoSync() {
        this.taskResult = new TaskResult();
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            run(jobExecutionContext);
        } catch (Exception e) {
            taskResult.setResult(false);
            taskResult.setMessage(e.getMessage());
            log.error(e.getMessage(), e);
        } finally {
            jobExecutionContext.setResult(taskResult);
        }
    }

    private void run(JobExecutionContext jobExecutionContext) {
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        Integer repoId = 0;
        try {
            repoId = jobDataMap.getIntegerFromString(RepoConst.SYNC_REPO_ID_FIELD);
        } catch (NumberFormatException e) {
            throw new KylinException(true, "0", MsgUtil.get(MsgCodes.PATCH_REPO_INVALID_ID_FORMAT));
        }
        Repository repository = repositoryService.findRepositoryById(repoId);
        if (repository == null) {
            throw new KylinException(true, "0", MsgUtil.get(MsgCodes.PATCH_REPO_NOT_FOUND));
        }
        Map<String, Package> packageMap;
        try {
            packageMap = RepomdParser.parseRepo(repository.getUrl());
            log.info("Parse repo done. package count = {}", packageMap.size());
        } catch (IOException e) {
            throw new KylinException(true, "0", e.getMessage());
        } catch (ParserConfigurationException e) {
            throw new KylinException(true, "0", MsgUtil.get(MsgCodes.PATCH_REPO_PARSE_ERROR));
        } catch (SAXException e) {
            throw new KylinException(true, "0", MsgUtil.get(MsgCodes.PATCH_REPOMD_PARSE_ERROR, e.getMessage()));
        }

        String dirName = repositoryService.getRepoPath(repository);
        Path repoPath = Path.of(repoBasePath, dirName);
        RepoContentUtils.checkRepoPath(repoPath);
        if (! checkFreeSpace(packageMap, dirName)) {
            taskResult.setMessage("WARN: A little of space left after synchronizing repository.");
        }

        for (Package pack : packageMap.values()) {
            try {
                downloadPack(pack, dirName);
            } catch (KylinException e) {
                // Download error, save to log and continue
                log.warn("Download pack {} with error: {}", pack.getNvrea(), e.getMessage());
                taskResult.setMessage(String.format("%s\nDownload pack %s with error: %s",
                        taskResult.getMessage(), pack.getNvrea(), e.getMessage()));
                continue;
            }
            try {
                savePack(pack, repository);
            } catch (KylinException e) {
                // Save error, save to log and continue
                log.warn("Save pack {} with error: {}", pack.getNvrea(), e.getMessage());
                taskResult.setMessage(String.format("%s\nSave pack %s with error: %s",
                        taskResult.getMessage(), pack.getNvrea(), e.getMessage()));
            }
        }
        RepoContentUtils.updateRepoData(repoPath);
    }

    /**
     *
     * @param packageMap
     * @param dirName
     * @return true - check passed; false - check passed with warning; exception thrown - check failed
     */
    private Boolean checkFreeSpace(Map<String, Package> packageMap, String dirName) {
        long totalSpace = 0L;
        for (Package pack : packageMap.values()) {
            if (pack.getPackSize() == null) {
                log.warn("WARN: one pack pack size is null");
                continue;
            }
            totalSpace += pack.getPackSize();
        }
        File testFile = new File(repoBasePath, dirName);
        if (testFile.getFreeSpace() < totalSpace) {
            log.error("No enough space while syncing repo. {} bytes needed, {} bytes free", totalSpace, testFile.getFreeSpace());
            throw new KylinException(MsgCodes.RESPCODE_NO_ENOUGH_SPACE);
        }
        if (testFile.getFreeSpace() < TOTAL_SPACE_THRESHOLD) {
            if (testFile.getTotalSpace() * TOTAL_SPACE_WARN_THRESHOLD < testFile.getUsableSpace() + totalSpace) {
                log.warn("Less than 10% free space left after syncing repo. {} bytes left",
                        testFile.getTotalSpace() - testFile.getUsableSpace() - totalSpace);
                return false;
            }
        } else if (testFile.getTotalSpace() - TOTAL_FREE_SPACE_THRESHOLD < testFile.getUsableSpace() + totalSpace ) {
            log.warn("Less than 100MB free space left after syncing repo. {} bytes left",
                    testFile.getTotalSpace() - testFile.getUsableSpace() - totalSpace);
            return false;
        }
        return true;
    }

    private void downloadPack(Package pack, String dirName) {
        Path repoPath = Path.of(repoBasePath, dirName);
        RepoContentUtils.syncRepoRpm(repoPath, pack);
    }

    private void savePack(Package pack, Repository repository) {
        Rpm rpm = new Rpm();
        rpm.setName(pack.getName());
        rpm.setArch(pack.getArch());
        rpm.setVersion(pack.getVersion());
        rpm.setEpoch(pack.getEpoch());
        rpm.setRelease(pack.getRelease());
        rpm.setSummary(pack.getSummary());
        rpm.setFilename(pack.getHref().substring(pack.getHref().lastIndexOf('/') + 1));
        rpm.setSourcerpm(pack.getSrpm());
        if ("SHA256".equalsIgnoreCase(pack.getChecksumType())) {
            rpm.setSha256(pack.getChecksum());
        } else {
            // TODO Calculate SHA-256 checksum if checksum type is not SHA-256
        }
        Set<RpmAttr> rpmAttrSet = new HashSet<>(17);
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_LICENSE, pack.getLicense()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_CHECKSUM_TYPE, pack.getChecksumType()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_CHECKSUM, pack.getChecksum()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_DESC, pack.getDescription()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_PACKAGER, pack.getPackager()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_URL, pack.getUrl()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_HREF, pack.getHref()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_FILETIME, String.valueOf(pack.getFileTime())));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_BUILDTIME, String.valueOf(pack.getBuildTime())));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_PACKSIZE, String.valueOf(pack.getPackSize())));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_INSTALLED_SIZE, String.valueOf(pack.getInstalledSize())));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_ARCHIVE_SIZE, String.valueOf(pack.getArchiveSize())));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_VENDOR, pack.getVendor()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_GROUP, pack.getGroup()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_BUILD_HOST, pack.getBuildHost()));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_HEADER_RANGE_START, String.valueOf(pack.getHeaderRangeStart())));
        rpmAttrSet.add(new RpmAttr(rpm, RepoConst.RPM_ATTR_HEADER_RANGE_END, String.valueOf(pack.getHeaderRangeEnd())));
        rpm.setRpmAttrSet(rpmAttrSet);

        Set<RpmDep> rpmDepSet = new HashSet<>();

        Optional.ofNullable(pack.getRequires()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_REQUIRES, rpm, entry))
                        .collect(Collectors.toSet())) );

        Optional.ofNullable(pack.getProvides()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_PROVIDES, rpm, entry))
                        .collect(Collectors.toSet())) );

        Optional.ofNullable(pack.getConflicts()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_CONFLICTS, rpm, entry))
                        .collect(Collectors.toSet())) );

        Optional.ofNullable(pack.getObsoletes()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_OBSOLETES, rpm, entry))
                        .collect(Collectors.toSet())) );

        Optional.ofNullable(pack.getEnhances()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_ENHANCES, rpm, entry))
                        .collect(Collectors.toSet())) );

        Optional.ofNullable(pack.getSuggests()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_SUGGESTS, rpm, entry))
                        .collect(Collectors.toSet())) );

        Optional.ofNullable(pack.getRecommends()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_RECOMMENDS, rpm, entry))
                        .collect(Collectors.toSet())) );

        Optional.ofNullable(pack.getSupplements()).ifPresent(
                entries -> rpmDepSet.addAll(entries.stream().map(
                        entry -> new RpmDep(RepoConst.RPM_DEP_SUPPLEMENTS, rpm, entry))
                        .collect(Collectors.toSet())) );
        rpm.setRpmDepSet(rpmDepSet);
        Rpm createdRpm = rpmService.createRpm(rpm);
        try {
            repositoryService.addRpm(repository, createdRpm);
        } catch (DuplicateKeyException e) {
            log.info("Repository {}/{} has rpm {}/{} already", repository.getId(), repository.getName(), createdRpm.getId(), createdRpm.getName());
        }
    }
}
