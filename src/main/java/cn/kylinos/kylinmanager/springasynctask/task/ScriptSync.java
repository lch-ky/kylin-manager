package cn.kylinos.kylinmanager.springasynctask.task;

import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.model.TaskResult;
import cn.kylinos.kylinmanager.util.clientrpc.ClientRpcFactory;
import cn.kylinos.kylinmanager.util.clientrpc.ClientRpcResult;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Base64;

@Slf4j
public class ScriptSync extends TaskBase {

    private TaskResult taskResult;

    @Autowired
    private ClientRpcFactory clientRpcFactory;

    public ScriptSync() {
        this.taskResult = new TaskResult();
    }

    @Autowired
    private LogService logService;

    @Autowired
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            run(jobExecutionContext);
        } catch (Exception e) {
            taskResult.setResult(false);
            taskResult.setMessage(e.getMessage());
            log.error(e.getMessage(), e);
        } finally {
            jobExecutionContext.setResult(taskResult);
        }
    }

    public void run(JobExecutionContext jobExecutionContext) {
        try
        {
            JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
            String hostIp = jobDataMap.getString("hostIp");
            String script = jobDataMap.getString("script");
            Integer timeout = jobDataMap.getIntegerFromString("timeout");
            String base64encoded  = Base64.getEncoder().encodeToString(script.getBytes("utf-8"));
            ClientRpcResult clientRpcResult = clientRpcFactory.getClientInstance(hostIp).execScript(base64encoded, Arrays.asList(new String[]{""}),timeout);
            clientRpcResult.getCode();
            clientRpcResult.getContent();
        }
        catch (Exception e)
        {
            taskResult.setResult(false);
        }
        finally
        {
            jobExecutionContext.setResult(taskResult);
        }
    }

}
