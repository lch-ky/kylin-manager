package cn.kylinos.kylinmanager.springasynctask.task.agent;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.config.AgentConfig;
import cn.kylinos.kylinmanager.logs.model.KylinSubLog;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.model.TaskResult;
import cn.kylinos.kylinmanager.springasynctask.task.TaskBase;
import cn.kylinos.kylinmanager.util.CommandString;
import cn.kylinos.kylinmanager.util.sshmanager.SshManager;
import cn.kylinos.kylinmanager.util.sshmanager.SshResult;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class AgentCreate extends TaskBase {

    @Autowired
    private HostService hostService;

    @Autowired
    private AgentConfig agentConfig;

    @Autowired
    private LogService logService;

    @Override
    public void executeInternal(JobExecutionContext jobExecutionContext)
    {
        TaskResult taskResult = new TaskResult();
        try
        {
            JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
            String sshUser = jobDataMap.getString("user");
            String sshHost = jobDataMap.getString("host");
            String sshPwd = jobDataMap.getString("pwd");
            KylinHost kylinHost = registerHost(sshUser, sshHost, sshPwd);
            Integer logId = Integer.valueOf(jobExecutionContext.getJobDetail().getKey().getName());
            KylinSubLog kylinSubLog = logService.findLogsBySubId(logId);
            if (kylinHost != null)
            {
                hostService.insertHost(kylinHost);
                kylinSubLog.setIp(kylinHost.getIp());
                kylinSubLog.setArch(kylinHost.getArchitecture());
                kylinSubLog.setSystem(kylinHost.getOperatingSystem());
                kylinSubLog.setVersion(kylinHost.getVersion());
            }
            else
            {
                taskResult.setMessage("网络连接失败");
                taskResult.setResult(false);
                kylinSubLog.setIp(sshHost);
            }
            logService.UpdateSubLog(kylinSubLog);
        }
        catch (Exception e)
        {
            taskResult.setResult(false);
            taskResult.setMessage(e.getMessage());
        }
        finally
        {
            jobExecutionContext.setResult(taskResult);
        }
    }

    private KylinHost registerHost(String user, String host, String pwd)
    {
        SshManager sshManager = new SshManager();
        KylinHost kylinHost = null;
        try
        {
            sshManager.initSession(user, host, pwd);
            String strInstallTime = sshManager.getCommandResult(CommandString.INSTALLTIME).getMessage();
            strInstallTime = strInstallTime.replace("\n", "");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date installTime = dateFormat.parse(strInstallTime);
            String arch = sshManager.getCommandResult(CommandString.ARCH).getMessage().replace("\n", "");

            String osStream = sshManager.getCommandResult(CommandString.OSSTREAM).getMessage();
            String[] osInfo = getOsinfo(osStream);
            String osName = osInfo[0];
            String osVersion = osInfo[1];

            String installEnv = sshManager.getCommandResult(CommandString.INSTALLENV).getMessage();
            installEnv = installEnv.replace("\n", "");
            if (!installEnv.isEmpty())
            {
                installEnv = installEnv.substring(installEnv.lastIndexOf(": ") + ": ".length());
            }


            String installGroup = sshManager.getCommandResult(CommandString.INSTALLGROUP).getMessage();
            installGroup = installGroup.replace("\n", "");
            installGroup = installGroup.replace("'", "\"");
            if (!installGroup.isEmpty())
            {
                installGroup = installGroup.substring(installGroup.lastIndexOf("["));
            }

            String uuidStream = sshManager.getCommandResult(CommandString.DMIDECODE).getMessage();
            String strUuid = getDmiuuid(uuidStream);

            installAgent(sshManager);
            String serverCrt = sshManager.getCommandResult(CommandString.AGENTCA).getMessage();
            String serverToken = sshManager.getCommandResult(CommandString.AGENTTOKEN).getMessage();

            if ((!serverCrt.isEmpty()) && (!serverToken.isEmpty()))
            {
                kylinHost = new KylinHost();
                kylinHost.setIp(host);
                kylinHost.setVersion(osVersion);
                kylinHost.setOperatingSystem(osName);
                kylinHost.setArchitecture(arch);
                kylinHost.setAgentVersion("1.0");
                kylinHost.setAgentCa(serverCrt);
                kylinHost.setAgentToken(serverToken);
                kylinHost.setInstallTime(installTime.toInstant());
                kylinHost.setInstallEnv(installEnv);
                kylinHost.setInstallGroups(installGroup);
                kylinHost.setCreatedAt(Instant.now());
                kylinHost.setUpdatedAt(Instant.now());
                kylinHost.setUuid(strUuid);
                kylinHost.setAgentStatus("false");
                kylinHost.setMigrateLock("false");
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            kylinHost = null;
        }
        finally
        {
            sshManager.disConnect();
        }
        return kylinHost;
    }

    private String[] getOsinfo(String osStream)
    {
        String[] osInfo = {"", ""};
        String[] lines = osStream.split("\n");
        String info = lines[1];
        String version = info.substring(info.indexOf("release ") + "release ".length(), info.indexOf("/"));
        osInfo[0] = version.contains("V10") ?"银河麒麟高级服务器操作系统":"中标麒麟高级服务器操作系统";
        if (version.contains("V7"))
        {
            osInfo[1] = "V7.0";
        }
        else if(version.contains("SP1"))
        {
            osInfo[1] = "V10(SP1)";
        }
        else
        {
            osInfo[1] = "V10(SP2)";
        }
        return osInfo;
    }

    private String getDmiuuid(String uuidStream)
    {
        String strUuid = "";
        String[] lines = uuidStream.split("\n");
        for (String line : lines)
        {
            line = line.replace("\n", "").replace(" ", "");
            if (line.contains("UUID:"))
            {
                strUuid = line.substring(line.indexOf("UUID:") + "UUID:".length());
                break;
            }
        }
        return strUuid;
    }

    private void installAgent(SshManager sshManager) throws Exception {
        String clientStream = readRromFile();
        String clientShell = MessageFormat.format(clientStream, sshManager.getClientIp(), agentConfig.getAgentServerAddress());
        sshManager.getCommandResult(String.format("echo '%s' > /tmp/client.sh", clientShell));
        sshManager.sendFile(agentConfig.getAgentLocation(), agentConfig.getAgentRemotePath());
        SshResult sshResult = sshManager.getCommandResult("cd /tmp;sh client.sh;");
        sshManager.getCommandResult("rm -f /tmp/client.sh");
        if(sshResult.getExitStatus() !=0)
        {
            throw new Exception("安装脚本执行异常" + sshResult.getMessage());
        }
    }

    String readRromFile() throws Exception
    {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(agentConfig.getAgentClientShell());
        if (in == null)
        {
            throw new Exception("未找到安装脚本");
        }
        InputStreamReader read = new InputStreamReader(in, StandardCharsets.UTF_8);
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br  = new BufferedReader(read))
        {
            String line;
            while ((line = br.readLine()) != null)
            {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }
}
