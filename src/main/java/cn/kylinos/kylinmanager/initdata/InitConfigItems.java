package cn.kylinos.kylinmanager.initdata;

import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.configitem.service.ConfigService;
import cn.kylinos.kylinmanager.configitem.service.StandardLibService;
import cn.kylinos.kylinmanager.util.ParserExcel;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Component
public class InitConfigItems {

    @Autowired
    private StandardLibService standardLibService;

    @Autowired
    private ConfigService configService;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) throws Exception {
        JSONArray standardArray = getStandardArray();
        for (int i = 0; i < standardArray.size(); i++)
        {
            JSONObject jsonObject = (JSONObject) standardArray.get(i);
            String osName = jsonObject.getString("os");
            String version = jsonObject.getString("version");
            String arch = jsonObject.getString("arch");
            String path = jsonObject.getString("path");
            if (!standardLibService.isStandardExist(osName, version, arch))
            {
                Integer standardId = standardLibService.createStandard(osName, version, arch);
                List<KylinConfig> kylinConfigList = parseExcel(standardId, osName, "configdata" + path);
                configService.addConfigs(kylinConfigList);
            }

        }
    }

    private List<KylinConfig> parseExcel(Integer standardId, String standardName, String filePath) throws Exception {
        List<KylinConfig> kylinConfigArrayList = new ArrayList<KylinConfig>();
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(filePath);
        ArrayList<ArrayList<String>> configData = ParserExcel.loadExcel(in);
        boolean isHeader = false;
        for(ArrayList<String> item: configData)
        {
            if (!isHeader)
            {
                isHeader = true;
                continue;
            }
            KylinConfig kylinConfig = new KylinConfig();
            kylinConfig.setName(item.get(0));
            kylinConfig.setBaseline(item.get(1));
            kylinConfig.setConfigType(item.get(2));
            kylinConfig.setAttribute(item.get(3));
            kylinConfig.setFlag(item.get(4).contains("0")?"false":"true");
            kylinConfig.setFilePath(item.get(5));
            if(item.size() < 7)
            {
                kylinConfig.setDescription("");
            }
            else
            {
                kylinConfig.setDescription(item.get(6));
            }
            kylinConfig.setStandardId(standardId);
            kylinConfig.setStandardName(standardName);
            kylinConfigArrayList.add(kylinConfig);

        }
        return kylinConfigArrayList;
    }


    private JSONArray getStandardArray() throws IOException
    {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("configdata/info.json");
        Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
        int ch = 0;
        StringBuilder sb = new StringBuilder();
        while ((ch = reader.read()) != -1) {
            sb.append((char) ch);
        }
        reader.close();
        String jsonStr = sb.toString();
        return JSONObject.parseArray(jsonStr);
    }
}
