package cn.kylinos.kylinmanager.config;

import cn.kylinos.kylinmanager.auth.service.impl.CustomBasicHttpAuthenticationFilter;
import cn.kylinos.kylinmanager.auth.service.impl.CustomFormAuthenticationFilter;
import org.apache.shiro.mgt.SessionsSecurityManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SessionsSecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        //自定义拦截器，重写FormAuthenticationFilter拦截器，阻止登录失败的跳转
        Map<String, Filter> filtersMap = new LinkedHashMap<String, Filter>();
        filtersMap.put("authc", new CustomFormAuthenticationFilter());
        filtersMap.put("authcBasic", new CustomBasicHttpAuthenticationFilter());
        shiroFilterFactoryBean.setFilters(filtersMap);

        // 拦截器.
        Map<String, String> map = new LinkedHashMap<String, String>();

        map.put("/logout", "authc,authcBasic");
        map.put("/login", "anon");
        map.put("/user/locale", "anon");
        map.put("/swagger-ui/**", "anon");
        map.put("/v3/api-docs", "anon");

        map.put("/**", "authc,authcBasic");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(map);


        return shiroFilterFactoryBean;
    }


}
