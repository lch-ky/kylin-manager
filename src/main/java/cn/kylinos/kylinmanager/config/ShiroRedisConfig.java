package cn.kylinos.kylinmanager.config;

import cn.kylinos.kylinmanager.auth.service.impl.CustomRealm;
import cn.kylinos.kylinmanager.auth.service.impl.ShiroSession;
import org.apache.shiro.mgt.SessionsSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ShiroRedisConfig {
    // Here is an example of inject redisSessionDAO and redisCacheManager by yourself if you've created your own sessionManager or securityManager
    @Autowired
    RedisSessionDAO redisSessionDAO;
    @Autowired
    RedisCacheManager redisCacheManager;

    @Bean
    public CustomRealm customRealm() {
        CustomRealm realm = new CustomRealm();
        return realm;
    }

    @Bean
    public SessionManager sessionManager() {
        ShiroSession sessionManager = new ShiroSession();
        // inject redisSessionDAO
        sessionManager.setSessionDAO(redisSessionDAO);
        return sessionManager;
    }
    @Bean
    public SessionsSecurityManager securityManager(List<Realm> realms, SessionManager sessionManager) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager(realms);
        //inject sessionManager
        securityManager.setSessionManager(sessionManager);
        securityManager.setRealm(customRealm());
        // inject redisCacheManager
        securityManager.setCacheManager(redisCacheManager);
        return securityManager;
    }

}
