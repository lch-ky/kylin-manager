package cn.kylinos.kylinmanager.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AgentConfig {

    @Value("${agent.package.path}")
    @Getter
    private String agentLocation;

    @Value("${agent.remote.path}")
    @Getter
    private String agentRemotePath;

    @Value("${agent.server.address}")
    @Getter
    private String agentServerAddress;

    @Value("${agent.script}")
    @Getter
    private String agentClientShell;

    @Value("${agent.lastest.path}")
    @Getter
    private String agentLastest;

}
