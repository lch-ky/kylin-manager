package cn.kylinos.kylinmanager.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PreDestroy;
import java.time.Duration;

@Configuration
@Slf4j
public class RedisConfig {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private String port;

    @Value("${spring.redis.password}")
    private String password;

    final JedisPoolConfig poolConfig = buildPoolConfig();

    private JedisPool jedisPool;

    @Bean
    public JedisPool jedisPool() {
        if (password.isEmpty())
        {
            jedisPool = new JedisPool(poolConfig, host, Integer.parseInt(port), 2000);
        }
        else
        {
            jedisPool = new JedisPool(poolConfig, host, Integer.parseInt(port), 2000, password);
        }
        return jedisPool;
    }

    private JedisPoolConfig buildPoolConfig() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setMaxIdle(128);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        return poolConfig;
    }

    @PreDestroy
    public void destroy() {
        log.info("JedisPool before destroying");
        jedisPool.close();
        log.info("JedisPool destroyed");
    }
}
