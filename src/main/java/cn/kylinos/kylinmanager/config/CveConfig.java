package cn.kylinos.kylinmanager.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CveConfig {
    @Value("${cve.uploadPath}")
    @Getter
    private String uploadPath;

}
