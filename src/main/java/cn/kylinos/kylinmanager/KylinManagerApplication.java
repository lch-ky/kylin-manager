package cn.kylinos.kylinmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KylinManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KylinManagerApplication.class, args);
    }
}