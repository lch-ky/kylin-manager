package cn.kylinos.kylinmanager.logs.constants;

public class LogConst {
    public static final String ACTION_REPO_SYNC = "Repository Synchronization";

    public static final String ACTION_SCRIPT_SYNC = "Script Synchronization";

    public static final String MODULE_REPO = "Repository Module";

    public static final String MODULE_SCRIPT = "Script Module";

    public static final String STATUS_READY = "ready";
    public static final String STATUS_RUNNING = "running";
    public static final String STATUS_DONE = "done";

    public static final String RESULT_PENDING = "pending";
    public static final String RESULT_SUCCESS = "success";
    public static final String RESULT_FAILED = "failed";

}
