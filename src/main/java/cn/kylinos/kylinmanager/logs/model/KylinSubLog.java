package cn.kylinos.kylinmanager.logs.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "kylin_sub_logs")
public class KylinSubLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "action", unique = true)
    private String action;
    @Column(name = "status")
    private String status;
    @Column(name = "result")
    private String result;
    @Column(name = "module")
    private String module;
    @Column(name = "rollback_flag")
    private String rollbackFlag;
    @Column(name = "detailshort")
    private String detailShort;
    @Column(name = "detail")
    private String detail;
    @Column(name = "parent_id")
    private Integer parentId;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "created_at")
    private Instant createdAt;
    @Column(name = "updated_at")
    private Instant updatedAt;
    @Column(name = "ip")
    private String ip;
    @Column(name = "system")
    private String system;
    @Column(name = "version")
    private String version;
    @Column(name = "arch")
    private String arch;
    @Column(name = "script_template_id")
    private Integer scriptTemplateId;
    @Column(name = "script_exection_id")
    private Integer scriptExectionId;
    @Column(name = "script_content")
    private String scriptContent;
    @Column(name = "script_ret")
    private String scriptRet;
    @Column(name = "script_creation_user")
    private String scriptCreationUser;

}
