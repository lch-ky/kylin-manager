package cn.kylinos.kylinmanager.logs.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Kylin_parent_logs")
public class KylinParentLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "action", unique = true)
    private String action;
    @Column(name = "status")
    private String status;
    @Column(name = "result")
    private String result;
    @Column(name = "module")
    private String module;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "rollback_flag")
    private String rollbackFlag;
    @Column(name = "detailshort")
    private String detailShort;
    @Column(name = "created_at")
    private Instant createdAt;
    @Column(name = "updated_at")
    private Instant updatedAt;
}
