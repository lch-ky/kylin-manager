package cn.kylinos.kylinmanager.logs.dao;

import cn.kylinos.kylinmanager.logs.model.KylinParentLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liliang
 */
@Repository
public interface ParentLogRepository extends JpaRepository<KylinParentLog, Integer> , JpaSpecificationExecutor<KylinParentLog> {

    List<KylinParentLog> findByUserId(Integer userId);

}
