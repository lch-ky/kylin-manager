package cn.kylinos.kylinmanager.logs.dao;

import cn.kylinos.kylinmanager.logs.model.KylinSubLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubLogRepository extends JpaRepository<KylinSubLog, Integer>, JpaSpecificationExecutor<KylinSubLog> {
    List<KylinSubLog> findByParentId(Integer parentId);

}
