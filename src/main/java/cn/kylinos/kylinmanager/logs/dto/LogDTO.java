package cn.kylinos.kylinmanager.logs.dto;

import cn.kylinos.kylinmanager.logs.model.KylinSubLog;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class LogDTO {

    private Integer id;

    private String action;

    private String status;

    private String result;

    private String module;

    private Integer userId;

    private String rollbackFlag;

    private String detail;

    private Instant createdAt;

    private Instant updatedAt;

    private List<KylinSubLog> kylinSubLogs;

}
