package cn.kylinos.kylinmanager.logs.service;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.logs.dto.LogDTO;
import cn.kylinos.kylinmanager.logs.model.KylinParentLog;
import cn.kylinos.kylinmanager.logs.model.KylinSubLog;

import java.util.List;

public interface LogService {

    /**
     * 返回所有父日志的集合，结合了高级搜索的功能，除了基础的搜索功能外，还支持搜索action、status、result、module、detail字段
     * @param action
     * @param status
     * @param result
     * @param module
     * @param detailShort
     * @param searchParam
     * @return
     */
    SearchResult<KylinParentLog> findAction(String action, String status, String result, String module, String detailShort, SearchParam searchParam);

    /**
     * 返回所有日志的集合，支持基础的搜索功能
     * @param searchParam
     * @return
     */
    List<LogDTO> findLogs(SearchParam searchParam);

    /**
     * 传入父id查询对应的父日志
     * @param parentId
     * @return
     */
    LogDTO findLogsByParentId(Integer parentId);

    /**
     * 传入子id查询对应的子日志
     * @param subId
     * @return
     */
    KylinSubLog findLogsBySubId(Integer subId);

    /**
     * 传入父id和参数查询对应的子日志列表
     * @param parentId
     * @param searchParam
     * @return
     */
    SearchResult<KylinSubLog> findSubLogsByParentId(Integer parentId, SearchParam searchParam);

    /**
     * 传入用户id查询对应的全体日志信息
     * @param id
     * @return
     */
    List<LogDTO> findLogsByUser(Integer id);

    /**
     * 更新父日志
     * @param id
     * @param status
     * @param result
     * @param detailShort
     * @return
     */
    KylinParentLog updateParentLogs(Integer id, String status, String result, String detailShort);

    /**
     * 更新子日志
     * @param id
     * @param status
     * @param result
     * @param detailShort
     * @param detail
     * @return
     */
    KylinSubLog updateSubLogs(Integer id, String status, String result, String detailShort, String detail);

    /**
     * 同时更新父子日志
     * @param id
     * @param status
     * @param result
     * @param detailShort
     * @param detail
     * @return
     */
    Boolean updateLogs(Integer id, String status, String result, String detailShort, String detail);

    /**
     * 创建父日志
     * @param action
     * @param status
     * @param result
     * @param module
     * @param rollbackFlag
     * @return
     */
    KylinParentLog createParentLogs(String action, String status, String result, String module, String rollbackFlag);


    /**
     * 创建子日志
     * @param action
     * @param status
     * @param result
     * @param module
     * @param rollbackFlag
     * @param parentId
     * @param hostId
     * @param scriptId
     * @return
     */
    KylinSubLog createSubLogs(String action, String status, String result, String module, String rollbackFlag, Integer parentId, Integer hostId, Integer scriptId);

    /**
     * 返回所有的父日志
     * @return
     */
    List<KylinParentLog> findAll();

    void UpdateSubLog(KylinSubLog kylinSubLog);
}
