package cn.kylinos.kylinmanager.logs.service.impl;

import cn.kylinos.kylinmanager.host.dao.HostRepository;
import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.logs.dao.ParentLogRepository;
import cn.kylinos.kylinmanager.logs.dao.SubLogRepository;
import cn.kylinos.kylinmanager.logs.dto.LogDTO;
import cn.kylinos.kylinmanager.logs.model.KylinParentLog;
import cn.kylinos.kylinmanager.logs.model.KylinSubLog;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class LogServiceImpl implements LogService {

    HashMap<String, String> test = new HashMap<>();

    @Autowired
    private ParentLogRepository parentLogRepository;

    @Autowired
    private SubLogRepository subLogRepository;

    @Autowired
    private HostRepository hostRepository;

    @Override
    public SearchResult<KylinParentLog> findAction(String action, String status, String result, String module, String detailShort, SearchParam searchParam) {

        if (action == null && status == null && result == null && module == null && detailShort == null) {
            return AdvancedSearch.findBySearch(searchParam, parentLogRepository);
        }
        return AdvancedSearch.findBySearch(hasAction(action).or(hasStatus(status)).or(hasResult(result)).or(hasModule(module)).or(hasDetailShort(detailShort)),
                searchParam, parentLogRepository);
    }

    static Specification<KylinParentLog> hasAction(String action) {
        return (parentLog, cq, cb) -> cb.equal(parentLog.get("action"), action);
    }
    static Specification<KylinParentLog> hasStatus(String status) {
        return (parentLog, cq, cb) -> cb.equal(parentLog.get("status"), status);
    }
    static Specification<KylinParentLog> hasResult(String result) {
        return (parentLog, cq, cb) -> cb.equal(parentLog.get("result"), result);
    }
    static Specification<KylinParentLog> hasModule(String module) {
        return (parentLog, cq, cb) -> cb.equal(parentLog.get("module"), module);
    }
    static Specification<KylinParentLog> hasDetailShort(String detailShort) {
        return (parentLog, cq, cb) -> cb.equal(parentLog.get("detailShort"), detailShort);
    }

    @Override
    public List<LogDTO> findLogs(SearchParam searchParam) {
        List<KylinParentLog> kylinParentLogs = parentLogRepository.findAll();
        return resultFormat(kylinParentLogs);
    }

    @Override
    public LogDTO findLogsByParentId(Integer parentId) {

        Optional<KylinParentLog> parentLog = parentLogRepository.findById(parentId);
        ModelMapper modelMapper = new ModelMapper();
        if (parentLog.isPresent()) {
            List<KylinSubLog> kylinSubLogs = subLogRepository.findByParentId(parentLog.get().getId());
            LogDTO logDTO = modelMapper.map(parentLog.get(), LogDTO.class);
            logDTO.setKylinSubLogs(kylinSubLogs);
            return logDTO;
        }
        return null;
    }

    @Override
    public KylinSubLog findLogsBySubId(Integer subId) {
        Optional<KylinSubLog> subLogs = subLogRepository.findById(subId);
        return subLogs.orElse(null);
    }

    @Override
    public SearchResult<KylinSubLog> findSubLogsByParentId(Integer parentId, SearchParam searchParam) {

        return AdvancedSearch.findBySearch(hasParent(parentId),searchParam, subLogRepository);
    }
    static Specification<KylinSubLog> hasParent(Integer parentId) {
        return (subLog, cq, cb) -> cb.equal(subLog.get("parentId"), parentId);
    }
    @Override
    public List<LogDTO> findLogsByUser(Integer id) {

        List<KylinParentLog> kylinParentLogs = parentLogRepository.findByUserId(id);
        return resultFormat(kylinParentLogs);
    }

    @Override
    public KylinParentLog updateParentLogs(Integer id, String status, String result, String detailShort) {

        Optional<KylinParentLog> kylinParentLogs = parentLogRepository.findById(id);
        if (kylinParentLogs.isPresent()) {
            KylinParentLog kylinParentLog = kylinParentLogs.get();
            kylinParentLog.setStatus(status);
            kylinParentLog.setResult(result);
            kylinParentLog.setDetailShort(detailShort);
            return kylinParentLogs.get();
        }else {
            return null;
        }
    }

    @Override
    public KylinSubLog updateSubLogs(Integer id, String status, String result, String detailShort, String detail) {

        Optional<KylinSubLog> kylinSubLog = subLogRepository.findById(id);
        if (kylinSubLog.isPresent()) {
            KylinSubLog sub = kylinSubLog.get();
            sub.setStatus(status);
            sub.setResult(result);
            sub.setDetailShort(detailShort);
            sub.setDetail(detail);
            sub.setUpdatedAt(Instant.now());
            subLogRepository.save(sub);
            return kylinSubLog.get();
        }else {
            return null;
        }
    }

    @Override
    public Boolean updateLogs(Integer id, String status, String result, String detailShort, String detail) {
        Optional<KylinSubLog> kylinSubLog = subLogRepository.findById(id);
        if (kylinSubLog.isPresent()) {
            KylinSubLog sub = kylinSubLog.get();
            sub.setStatus(status);
            sub.setResult(result);
            sub.setDetailShort(detailShort);
            sub.setDetail(detail);
            sub.setUpdatedAt(Instant.now());
            subLogRepository.save(sub);
            Integer parentId = kylinSubLog.get().getParentId();
            List<KylinSubLog> subLogs = subLogRepository.findByParentId(parentId);
            long countSuccess = subLogs.stream().filter(subLog -> "success".equals(subLog.getStatus())).count();
            long countRunning = subLogs.stream().filter(subLog -> "running".equals(subLog.getStatus())).count();
            int size = subLogs.size();
            if (List.of("running", "failed", "cancel", "pause").contains(status)) {
                setParentLog(parentId, status, result, detailShort);
            }else {
                if ("success".equals(status)) {
                    if (size == countSuccess) {
                        setParentLog(parentId, status, result, detailShort);
                    }
                }
                else {
                    if (countRunning == 0) {
                        setParentLog(parentId, status, result, detailShort);
                    }
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public KylinParentLog createParentLogs(String action, String status, String result, String module, String rollbackFlag) {
        KylinParentLog kylinParentLog = new KylinParentLog();
        kylinParentLog.setAction(action);
        kylinParentLog.setStatus(status);
        kylinParentLog.setResult(result);
        kylinParentLog.setModule(module);
        kylinParentLog.setRollbackFlag(rollbackFlag);
        kylinParentLog.setCreatedAt(Instant.now());
        kylinParentLog.setUpdatedAt(Instant.now());
        parentLogRepository.save(kylinParentLog);
        return kylinParentLog;
    }

    @Override
    public KylinSubLog createSubLogs(String action, String status, String result, String module, String rollbackFlag, Integer parentId, Integer hostId, Integer scriptId) {
        KylinSubLog kylinSubLog = new KylinSubLog();
        kylinSubLog.setAction(action);
        kylinSubLog.setStatus(status);
        kylinSubLog.setResult(result);
        kylinSubLog.setModule(module);
        kylinSubLog.setRollbackFlag(rollbackFlag);
        kylinSubLog.setParentId(parentId);
        kylinSubLog.setCreatedAt(Instant.now());
        kylinSubLog.setUpdatedAt(Instant.now());
        if (hostId != null) {
            Optional<KylinHost> kylinHosts = hostRepository.findById(hostId);
            if (kylinHosts.isPresent()) {
                kylinSubLog.setIp(kylinHosts.get().getIp());
                kylinSubLog.setSystem(kylinHosts.get().getOperatingSystem());
                kylinSubLog.setVersion(kylinHosts.get().getVersion());
                kylinSubLog.setArch(kylinHosts.get().getArchitecture());
            }
        }
        subLogRepository.save(kylinSubLog);
        return kylinSubLog;
    }

    @Override
    public List<KylinParentLog> findAll() {
        return parentLogRepository.findAll();
    }

    @Override
    public void UpdateSubLog(KylinSubLog kylinSubLog) {
        subLogRepository.saveAndFlush(kylinSubLog);
    }

    private List<LogDTO> resultFormat(List<KylinParentLog> kylinParentLogs) {

        List<LogDTO> logDtos = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (KylinParentLog kylinParentLog :
                kylinParentLogs) {
            List<KylinSubLog> kylinSubLogs = subLogRepository.findByParentId(kylinParentLog.getId());
            LogDTO logDTO = modelMapper.map(kylinParentLog, LogDTO.class);
            logDTO.setKylinSubLogs(kylinSubLogs);
            logDtos.add(logDTO);
        }
        return logDtos;
    }

    private void setParentLog(Integer parentId, String status, String result, String detailShort){
        Optional<KylinParentLog> parentLog = parentLogRepository.findById(parentId);
        if (parentLog.isPresent()){
            KylinParentLog parent = parentLog.get();
            parent.setStatus(status);
            parent.setResult(result);
            parent.setDetailShort(detailShort);
            parent.setUpdatedAt(Instant.now());
            parentLogRepository.save(parent);
        }
    }
}
