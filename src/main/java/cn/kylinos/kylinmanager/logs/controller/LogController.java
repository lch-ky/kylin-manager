package cn.kylinos.kylinmanager.logs.controller;

import cn.kylinos.kylinmanager.common.KylinException;
import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.logs.dto.LogDTO;
import cn.kylinos.kylinmanager.logs.model.KylinParentLog;
import cn.kylinos.kylinmanager.logs.model.KylinSubLog;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.factory.SpringTaskManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.List;

import static cn.kylinos.kylinmanager.util.ExportExcel.exportExcel;

@RestController
@RequestMapping("/logs")
@Slf4j
public class LogController {
    @Autowired
    private LogService logService;

    @Autowired
    private SpringTaskManager springTaskManager;

    @Operation(summary = "父日志信息接口",
            responses = {@ApiResponse(description = "日志信息", content = @Content(mediaType = "application/json"))})
    @GetMapping()
    public RestResponse list(String action, String status, String result, String module, String detail,
                             SearchParam searchParam) {
        return RestResponse.createSuccessResponse(logService.findAction(action, status, result, module, detail, searchParam));
    }

    @Operation(summary = "根据父日志id查询日志信息的接口",
            responses = {@ApiResponse(description = "子日志信息", content = @Content(mediaType = "application/json"))})
    @GetMapping("/{parentId}")
    public RestResponse listByParentId(@PathVariable Integer parentId){
        return RestResponse.createSuccessResponse(logService.findLogsByParentId(parentId));
    }

    @Operation(summary = "根据父日志查询所有的子日志信息接口",
            responses = {@ApiResponse(description = "子日志信息", content = @Content(mediaType = "application/json"))})
    @GetMapping("/{parentId}/sub")
    public RestResponse listBySubId(@PathVariable Integer parentId, SearchParam searchParam){
        return RestResponse.createSuccessResponse(logService.findSubLogsByParentId(parentId,searchParam));
    }

    @Operation(summary = "暂停任务",
            responses = {@ApiResponse(description = "成功或失败", content = @Content(mediaType = "application/json"))})
    @PutMapping("/{parentId}/pause")
    public RestResponse pauseTask(@PathVariable Integer parentId) {
        try {
            LogDTO log =  logService.findLogsByParentId(parentId);
            for (KylinSubLog subLog : log.getKylinSubLogs()) {
                springTaskManager.pauseTask(subLog.getId());
            }
            return RestResponse.createSuccessResponse("success");
        } catch (Exception e) {
            return RestResponse.createErrorResponse("5001","Pause task failed");
        }
    }

    @Operation(summary = "导出父日志信息表",
            responses = {@ApiResponse(description = "Excel表格", content = @Content(mediaType = "application/vnd.ms-excel"))})
    @GetMapping("/exportExcel")
    public void excelDownload(HttpServletResponse response) throws Exception {
        String[] head = {"id","action","status","result","module","userId","rollback_flag","detailShort","created_at","updated_at"};
        List<KylinParentLog> parentLogs = logService.findAll();
        HSSFWorkbook workbook = exportExcel(head, parentLogs, "log");
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment;filename=log.xls");
        response.flushBuffer();
        workbook.write(response.getOutputStream());
    }

    @Operation(summary = "取消任务",
            responses = {@ApiResponse(description = "成功或失败", content = @Content(mediaType = "application/json"))})
    @PutMapping("/{parentId}/cancel")
    public RestResponse cancelTask(@PathVariable Integer parentId) {
        try {
            LogDTO log =  logService.findLogsByParentId(parentId);
            for (KylinSubLog subLog : log.getKylinSubLogs()) {
                springTaskManager.cancelTask(subLog.getId());
            }
            return RestResponse.createSuccessResponse("success");
        } catch (Exception e) {
            return RestResponse.createErrorResponse("5001","Cancel task failed");
        }
    }

    @Operation(summary = "继续任务",
            responses = {@ApiResponse(description = "成功或失败", content = @Content(mediaType = "application/json"))})
    @PutMapping("/{parentId}/resume/")
    public RestResponse resumeTask(@PathVariable Integer parentId) {
        try {
            LogDTO log =  logService.findLogsByParentId(parentId);
            for (KylinSubLog subLog : log.getKylinSubLogs()) {
                springTaskManager.resumeTask(subLog.getId());
            }
            return RestResponse.createSuccessResponse("success");
        } catch (Exception e) {
            return RestResponse.createErrorResponse("5001","Resume task failed");
        }
    }

}
