package cn.kylinos.kylinmanager.host.service;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.host.model.KylinHostConfig;
import cn.kylinos.kylinmanager.host.model.KylinHostConfigChangeLog;

import java.util.List;

public interface HostService {
    KylinHost findHostByIp(String ip);
    KylinHost findHostById(int id);
    List<KylinHost> findAll();
    void insertHost(KylinHost kylinHost);
    boolean isHostExist(String host);
    SearchResult<KylinHost> findHostByPage(SearchParam searchParam);
    List<KylinHost> findByIdIn(List<Integer> ids);
    void deleteById(Integer id);

    void createAndUpdateConfigs(Integer hostId, List<KylinHostConfig> kylinHostConfigs);
    SearchResult<KylinHostConfig> findConfigByHostId(Integer hostId, SearchParam searchParam);
    SearchResult<KylinHostConfigChangeLog> findConfigChangeLogByHostId(Integer hostId, SearchParam searchParam);

    int findCountByVersion(List<String> kylinIps);
}
