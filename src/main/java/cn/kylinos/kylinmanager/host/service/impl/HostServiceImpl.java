package cn.kylinos.kylinmanager.host.service.impl;

import cn.kylinos.kylinmanager.host.dao.HostConfigChangeLogRepository;
import cn.kylinos.kylinmanager.host.dao.HostConfigRepository;
import cn.kylinos.kylinmanager.host.dao.HostRepository;
import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.model.KylinHostConfig;
import cn.kylinos.kylinmanager.host.model.KylinHostConfigChangeLog;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HostServiceImpl implements HostService {

    @Autowired
    private HostRepository hostRepository;

    @Autowired
    private HostConfigRepository hostConfigRepository;

    @Autowired
    private HostConfigChangeLogRepository hostConfigChangeLogRepository;

    @Override
    public KylinHost findHostByIp(String ip) {
        return hostRepository.findByIp(ip);
    }

    @Override
    public KylinHost findHostById(int id) {
        return hostRepository.findById(id);
    }

    @Override
    public List<KylinHost> findAll() {
        return hostRepository.findAll();
    }


    @Override
    public void insertHost(KylinHost kylinHost) {
        hostRepository.save(kylinHost);
    }

    @Override
    public boolean isHostExist(String host) {
        KylinHost kylinHost = hostRepository.findByIp(host);
        return kylinHost != null;
    }

    @Override
    public SearchResult<KylinHost> findHostByPage(SearchParam searchParam) {
        return AdvancedSearch.findBySearch(searchParam, hostRepository);
    }

    @Override
    public List<KylinHost> findByIdIn(List<Integer> ids) {
        return hostRepository.findByIdIn(ids);
    }

    @Override
    public void deleteById(Integer id) {
        hostRepository.deleteById(id);
    }

    @Override
    public void createAndUpdateConfigs(Integer hostId, List<KylinHostConfig> kylinHostConfigs) {
        List<KylinHostConfigChangeLog> kylinHostConfigChangeLogs = new ArrayList<>();
        for(KylinHostConfig item: kylinHostConfigs)
        {
            item.setHostId(hostId);
            KylinHostConfig config = hostConfigRepository.findByHostIdAndNameAndAttribute(hostId, item.getName(),
                    item.getAttribute());
            if(config == null)
            {
                kylinHostConfigChangeLogs.add(toHostConfigChangelog(item, ""));
                continue;
            }
            item.setId(config.getId());
            if(!item.getBaseline().equals(config.getBaseline()))
            {
                kylinHostConfigChangeLogs.add(toHostConfigChangelog(item, config.getBaseline()));
            }
        }
        hostConfigRepository.saveAll(kylinHostConfigs);
        hostConfigChangeLogRepository.saveAll(kylinHostConfigChangeLogs);
    }

    @Override
    public SearchResult<KylinHostConfig> findConfigByHostId(Integer hostId, SearchParam searchParam) {
        return AdvancedSearch.findBySearch(hasConfigHost(hostId), searchParam, hostConfigRepository);
    }

    @Override
    public SearchResult<KylinHostConfigChangeLog> findConfigChangeLogByHostId(Integer hostId, SearchParam searchParam) {
        return AdvancedSearch.findBySearch(hasConfigLogHost(hostId), searchParam, hostConfigChangeLogRepository);
    }

    @Override
    public int findCountByVersion(List<String> kylinIps) {
        return hostRepository.findCountByVersion(kylinIps);
    }

    static Specification<KylinHostConfigChangeLog> hasConfigLogHost(Integer hostId) {
        return (kylinHostConfigChangeLogRoot, cq, cb) -> cb.equal(kylinHostConfigChangeLogRoot.get("hostId"), hostId);
    }

    static Specification<KylinHostConfig> hasConfigHost(Integer hostId) {
        return (kylinHostConfigRoot, cq, cb) -> cb.equal(kylinHostConfigRoot.get("hostId"), hostId);
    }

    private KylinHostConfigChangeLog toHostConfigChangelog(KylinHostConfig kylinHostConfig, String oldBaseline)
    {
        KylinHostConfigChangeLog kylinHostConfigChangeLog = new KylinHostConfigChangeLog();
        kylinHostConfigChangeLog.setHostId(kylinHostConfig.getHostId());
        kylinHostConfigChangeLog.setName(kylinHostConfig.getName());
        kylinHostConfigChangeLog.setConfigType(kylinHostConfig.getConfigType());
        kylinHostConfigChangeLog.setAttribute(kylinHostConfig.getAttribute());
        kylinHostConfigChangeLog.setDescription(kylinHostConfig.getDescription());
        kylinHostConfigChangeLog.setFlag(kylinHostConfig.getFlag());
        kylinHostConfigChangeLog.setFilePath(kylinHostConfig.getFilePath());
        kylinHostConfigChangeLog.setBaseline(oldBaseline);
        kylinHostConfigChangeLog.setCurBaseline(kylinHostConfig.getBaseline());
        return kylinHostConfigChangeLog;
    }
}
