package cn.kylinos.kylinmanager.host.dao;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HostRepository extends JpaRepository<KylinHost, Integer>, JpaSpecificationExecutor<KylinHost>
{
    KylinHost findByIp(String ip);
    KylinHost findById(int id);
    List<KylinHost> findByIdIn(List<Integer> ids);

    @Query(value = "select count(version) from (select version, count(ip) from kylin_hosts h where h.ip in ?1 GROUP BY h.version) as a; ", nativeQuery = true)
    int findCountByVersion(List<String> kylinIps);
}
