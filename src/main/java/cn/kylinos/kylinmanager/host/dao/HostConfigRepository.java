package cn.kylinos.kylinmanager.host.dao;

import cn.kylinos.kylinmanager.host.model.KylinHostConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HostConfigRepository  extends JpaRepository<KylinHostConfig, Integer>, JpaSpecificationExecutor<KylinHostConfig> {
    KylinHostConfig findByHostIdAndNameAndAttribute(Integer hostId, String name, String attribute);
    List<KylinHostConfig> findByHostId(Integer hostId);
}
