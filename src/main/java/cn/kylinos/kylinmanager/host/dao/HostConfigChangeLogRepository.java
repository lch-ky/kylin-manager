package cn.kylinos.kylinmanager.host.dao;

import cn.kylinos.kylinmanager.host.model.KylinHostConfigChangeLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface HostConfigChangeLogRepository extends JpaRepository<KylinHostConfigChangeLog, Integer>,
        JpaSpecificationExecutor<KylinHostConfigChangeLog> {
}
