package cn.kylinos.kylinmanager.host.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "kylin_hosts")
public class KylinHost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "ip")
    private String ip;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "operating_system")
    private String operatingSystem;

    @Column(name = "version")
    private String version;

    @Column(name = "architecture")
    private String architecture;

    @Column(name = "agent_status")
    private String agentStatus;

    @Column(name = "agent_version")
    private String agentVersion;

    @Column(name = "agent_ca")
    @JsonIgnore
    private String agentCa;

    @Column(name = "agent_token")
    @JsonIgnore
    private String agentToken;

    @Column(name = "distribution")
    private String distribution;

    @Column(name = "install_env")
    private String installEnv;

    @Column(name = "install_groups")
    private String installGroups;

    @Column(name = "bandwidth_limit")
    private String bandwidthLimit;

    @Column(name = "migrate_lock")
    private String migrateLock;

    @Column(name = "install_time")
    private Instant installTime;

    @Column(name = "reserved1")
    private String reserved1;

    @Column(name = "reserved2")
    private String reserved2;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;
}
