package cn.kylinos.kylinmanager.host.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;


@Data
@Builder
@AllArgsConstructor
@Entity
@Table(name="kylin_host_config_changelogs")
public class KylinHostConfigChangeLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "config_type")
    private String configType;

    @Column(name = "attribute")
    private String attribute;

    @Column(name = "baseline")
    private String baseline;

    @Column(name = "description")
    private String description;

    @Column(name = "flag")
    private String flag;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "host_id")
    private Integer hostId;

    @Column(name = "cur_baseline")
    private String curBaseline;

    public KylinHostConfigChangeLog()
    {
        createdAt = Instant.now();
        updatedAt = Instant.now();
        baseline = "";
    }
}
