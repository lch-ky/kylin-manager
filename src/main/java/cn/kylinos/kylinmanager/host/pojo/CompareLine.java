package cn.kylinos.kylinmanager.host.pojo;

import lombok.Data;

@Data
public class CompareLine {
    String name;

    String leftValue;

    String attribute;

    String configType;

    String filePath;

    String rightValue;

    String leftRegister;

    String rightRegister;
}
