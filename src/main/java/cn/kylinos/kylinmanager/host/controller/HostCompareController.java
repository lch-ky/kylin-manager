package cn.kylinos.kylinmanager.host.controller;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.pojo.CompareLine;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.compare.CompareResult;
import cn.kylinos.kylinmanager.common.compare.CompareResultItem;
import cn.kylinos.kylinmanager.common.config.ConfigStruct;
import cn.kylinos.kylinmanager.util.ExportExcel;
import cn.kylinos.kylinmanager.util.clientrpc.ClientRpcFactory;
import com.alibaba.fastjson.JSONObject;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController()
@RequestMapping("/hosts")
public class HostCompareController {

    private static final transient Logger log = LoggerFactory.getLogger(HostManagerController.class);

    @Autowired
    private HostService hostService;

    @Autowired
    private ClientRpcFactory clientRpcFactory;

    @PostMapping("/compare")
    public RestResponse compare(@RequestParam("source") String sourceIp, @RequestParam("target") String targetIp,
                                @RequestParam(value="isComplete", defaultValue = "false") boolean isComplete)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            KylinHost sourceHost = hostService.findHostByIp(sourceIp);
            if(sourceHost == null)
            {
                throw new Exception(MessageFormat.format("主机{0}不存在", sourceIp));
            }
            KylinHost targetHost = hostService.findHostByIp(targetIp);
            if(targetHost == null)
            {
                throw new Exception(MessageFormat.format("主机{0}不存在", targetIp));
            }
            ConfigStruct targetConfig = clientRpcFactory.getClientInstance(
                    targetHost.getIp()).getAllConfig();
            ConfigStruct sourceConfig = clientRpcFactory.getClientInstance(
                    sourceHost.getIp()).getAllConfig();
            JSONObject compareResult = sourceConfig.compare(targetConfig, isComplete);
            restResponse.setData(compareResult);
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    @GetMapping("/compare")
    public RestResponse compareDownload(@RequestParam("source") String sourceIp, @RequestParam("target") String targetIp,
                                        @RequestParam(value="isComplete", defaultValue = "false") boolean isComplete,
                                        HttpServletResponse response)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            KylinHost sourceHost = hostService.findHostByIp(sourceIp);
            if(sourceHost == null)
            {
                throw new Exception(MessageFormat.format("主机{0}不存在", sourceIp));
            }
            KylinHost targetHost = hostService.findHostByIp(targetIp);
            if(targetHost == null)
            {
                throw new Exception(MessageFormat.format("主机{0}不存在", targetIp));
            }
            ConfigStruct targetConfig = clientRpcFactory.getClientInstance(
                    targetHost.getIp()).getAllConfig();
            ConfigStruct sourceConfig = clientRpcFactory.getClientInstance(
                    sourceHost.getIp()).getAllConfig();
            List<CompareResult> compareResults = sourceConfig.compareList(targetConfig, isComplete);
            ArrayList<CompareLine> lines = convertCompareResult(compareResults, sourceHost.getCreatedAt(),
                    targetHost.getCreatedAt());
            String headSource1 = MessageFormat.format("{0}注册时间", sourceIp);
            String headSource2 = MessageFormat.format("{0}注册时间", targetIp);
            String[] head = {"配置参数", sourceIp, "配置属性", "配置类型", "文件路径", targetIp, headSource1, headSource2};
            HSSFWorkbook hssfWorkbook = ExportExcel.exportExcel(head, lines, "比对结果");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition",
                    MessageFormat.format("attachment;filename={0}-{1}.xls", sourceIp, targetIp));
            response.flushBuffer();
            hssfWorkbook.write(response.getOutputStream());
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    private ArrayList<CompareLine> convertCompareResult(List<CompareResult> compareResults, Instant leftRegister,
                                                              Instant rightRegister)
    {
        DateTimeFormatter dataTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String leftTime = dataTimeFormatter.format(LocalDateTime.ofInstant(leftRegister, ZoneId.systemDefault()));
        String rightTime = dataTimeFormatter.format(LocalDateTime.ofInstant(rightRegister, ZoneId.systemDefault()));
        ArrayList<CompareLine> lines = new ArrayList<>();
        for(CompareResult compareResult: compareResults)
        {
            for (CompareResultItem compareResultItem: compareResult.getData())
            {
                CompareLine compareLine = new CompareLine();
                compareLine.setName(compareResultItem.getName());
                compareLine.setLeftValue(compareResultItem.getLeft());
                compareLine.setAttribute(compareResult.getAttribute());
                compareLine.setConfigType(compareResult.getConfigType());
                compareLine.setFilePath(compareResult.getFilePath());
                compareLine.setRightValue(compareResultItem.getLeft());
                compareLine.setLeftRegister(leftTime);
                compareLine.setRightRegister(rightTime);
                lines.add(compareLine);
            }
        }
        return lines;
    }
}
