package cn.kylinos.kylinmanager.host.controller;

import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.factory.SpringTaskManager;
import cn.kylinos.kylinmanager.springasynctask.task.configIssue.ConfigIssue;
import cn.kylinos.kylinmanager.springasynctask.task.scan.HostScan;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController()
@RequestMapping("/hosts")
public class HostScanController {

    @Autowired
    private HostService hostService;

    @Autowired
    private LogService logService;

    @Autowired
    private SpringTaskManager springTaskManager;

    @PostMapping("/scan/now")
    public RestResponse immediate(@RequestBody JSONObject jsonObject)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            List<Integer> hostIds = jsonObject.getJSONArray("hostIds").toJavaList(Integer.class);
            Integer parentId = logService.createParentLogs("扫描", "pending", "", "扫描模块", "false").getId();

            for(Integer hostId: hostIds)
            {
                HashMap<String, String> data = new HashMap<>();
                data.put("hostId", String.valueOf(hostId));
                Integer logId = logService.createSubLogs("扫描", "pending", "", "扫描模块", "false",
                        parentId, hostId, -1).getId();
                springTaskManager.addTask(logId, new HostScan(), data);
            }
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    @PostMapping("/scan/timed")
    public RestResponse timed(@RequestBody JSONObject jsonObject)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            String timeRule = jsonObject.getString("timeRule");
            Integer parentId = logService.createParentLogs("定时扫描", "pending", "", "扫描模块", "false").getId();
            List<KylinHost> kylinHosts = hostService.findAll();
            for(KylinHost kylinHost: kylinHosts)
            {
                Integer hostId = kylinHost.getId();
                HashMap<String, String> data = new HashMap<>();
                data.put("hostId", String.valueOf(hostId));
                Integer logId = logService.createSubLogs("定时扫描", "pending", "", "扫描模块", "false",
                        parentId, hostId, -1).getId();
                springTaskManager.addTimedTask(logId, new HostScan(), data, timeRule);
            }
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }
}
