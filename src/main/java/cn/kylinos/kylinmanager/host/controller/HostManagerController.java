package cn.kylinos.kylinmanager.host.controller;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.factory.SpringTaskManager;
import cn.kylinos.kylinmanager.springasynctask.task.agent.AgentCreate;
import cn.kylinos.kylinmanager.springasynctask.task.agent.AgentDelete;
import cn.kylinos.kylinmanager.springasynctask.task.agent.AgentUpdate;
import cn.kylinos.kylinmanager.util.ParserExcel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("/hosts")
public class HostManagerController {

    private static final transient Logger log = LoggerFactory.getLogger(HostManagerController.class);

    @Autowired
    private HostService hostService;

    @Autowired
    private SpringTaskManager springTaskManager;

    @Autowired
    private LogService logService;

    @GetMapping("/list")
    public RestResponse list(SearchParam searchParam)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
           SearchResult<KylinHost> hostList = hostService.findHostByPage(searchParam);
           restResponse.setData(hostList);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    @PostMapping("/create")
    public RestResponse create(@RequestParam("file") MultipartFile file)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            InputStream inputStream = file.getInputStream();
            ArrayList<ArrayList<String>> arrayList = ParserExcel.loadExcel(inputStream);
            int fileLines = arrayList.size();
            if (fileLines <= 1)
            {
                throw new Exception("文件缺少数据");
            }
            Integer parentId = logService.createParentLogs("客户端创建", "pending", "", "客户端管理", "false").getId();
            for(int lineNum = 0; lineNum < fileLines; lineNum++)
            {
                if (lineNum == 0)
                {
                    continue;
                }
                ArrayList<String> hostList = arrayList.get(lineNum);
                if (hostService.isHostExist(hostList.get(0)))
                {
                    continue;
                }
                HashMap<String, String> hostInfo = new HashMap<>();
                hostInfo.put("host", hostList.get(0));
                hostInfo.put("user", hostList.get(1));
                hostInfo.put("pwd", hostList.get(2));
                Integer logId = logService.createSubLogs("客户端创建", "pending", "", "客户端管理", "false",
                        parentId, -1, -1).getId();
                springTaskManager.addTask(logId, new AgentCreate(), hostInfo);
            }
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    @PutMapping("/update")
    public RestResponse update(@RequestBody List<Integer> hostIds) {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        List<KylinHost> kylinHosts = hostService.findByIdIn(hostIds);
        try {
            if(kylinHosts.size() != hostIds.size())
            {
                List<Integer> ids = kylinHosts.stream().map(KylinHost::getId).collect(Collectors.toList());
                hostIds.removeAll(ids);
                String errorMessage = MessageFormat.format("{0} 未找到, 停止更新",
                        StringUtils.join(hostIds.toArray(), ","));
                throw new Exception(errorMessage);
            }

            Integer parentId = logService.createParentLogs("客户端更新", "pending", "",
                    "客户端管理", "false").getId();



            for(KylinHost kylinHost: kylinHosts)
            {
                HashMap<String, String> data = new HashMap<>();
                data.put("hostId", String.valueOf(kylinHost.getId()));
                Integer logId = logService.createSubLogs("客户端更新", "pending", "", "客户端管理", "false",
                        parentId, kylinHost.getId(), -1).getId();
                springTaskManager.addTask(logId, new AgentUpdate(), data);
            }
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }

        return restResponse;
    }

    @DeleteMapping("/delete")
    public RestResponse delete(@RequestBody List<Integer> hostIds) {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        List<KylinHost> kylinHosts = hostService.findByIdIn(hostIds);
        try {
            if(kylinHosts.size() != hostIds.size())
            {
                List<Integer> ids = kylinHosts.stream().map(KylinHost::getId).collect(Collectors.toList());
                hostIds.removeAll(ids);
                String errorMessage = MessageFormat.format("{0} 未找到, 停止更新",
                        StringUtils.join(hostIds.toArray(), ","));
                throw new Exception(errorMessage);
            }

            Integer parentId = logService.createParentLogs("客户端删除", "pending", "",
                    "客户端管理", "false").getId();



            for(KylinHost kylinHost: kylinHosts)
            {
                HashMap<String, String> data = new HashMap<>();
                data.put("hostId", String.valueOf(kylinHost.getId()));
                Integer logId = logService.createSubLogs("客户端删除", "pending", "", "客户端管理", "false",
                        parentId, kylinHost.getId(), -1).getId();
                springTaskManager.addTask(logId, new AgentDelete(), data);
            }
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }

        return restResponse;
    }

}
