package cn.kylinos.kylinmanager.host.controller;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.model.KylinHostConfig;
import cn.kylinos.kylinmanager.host.model.KylinHostConfigChangeLog;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/hosts")
public class HostDetailController {

    @Autowired
    private HostService hostService;

    @GetMapping("/config/list")
    public RestResponse configList(@RequestParam Integer hostId, SearchParam searchParam)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            SearchResult<KylinHostConfig> hostList = hostService.findConfigByHostId(hostId, searchParam);
            restResponse.setData(hostList);
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    @GetMapping("/config/changelog")
    public RestResponse configRecord(Integer hostId, SearchParam searchParam)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            SearchResult<KylinHostConfigChangeLog> hostList = hostService.findConfigChangeLogByHostId(hostId, searchParam);
            restResponse.setData(hostList);
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    @GetMapping("/package/list")
    public RestResponse packageList(SearchParam searchParam)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            SearchResult<KylinHost> hostList = hostService.findHostByPage(searchParam);
            restResponse.setData(hostList);
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }

    @GetMapping("/package/record")
    public RestResponse packageRecord(SearchParam searchParam)
    {
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            SearchResult<KylinHost> hostList = hostService.findHostByPage(searchParam);
            restResponse.setData(hostList);
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }
}
