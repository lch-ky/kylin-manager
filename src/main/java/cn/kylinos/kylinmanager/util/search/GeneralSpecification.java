package cn.kylinos.kylinmanager.util.search;


import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class GeneralSpecification<T> implements Specification<T> {

    private SpecSearchCriteria criteria;

    public GeneralSpecification(final SpecSearchCriteria criteria) {
        super();
        this.criteria = criteria;
    }

    public SpecSearchCriteria getCriteria() {
        return criteria;
    }

    @Override
    public Predicate toPredicate(final Root<T> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {
        Class<?> clazz = root.getJavaType();
        // Apply alias
        String key = alias(clazz, criteria.getKey());

        Object value = convertValueByType(clazz, key, criteria.getValue());

        switch (criteria.getOperation()) {
            case EQUALITY:
                return builder.equal(root.get(key), value);
            case NEGATION:
                return builder.notEqual(root.get(key), value);
            case GREATER_THAN:
                return builder.greaterThan(root.get(key), value.toString());
            case LESS_THAN:
                return builder.lessThan(root.get(key), value.toString());
            case GREATER_THAN_EQ:
                return builder.greaterThanOrEqualTo(root.get(key), value.toString());
            case LESS_THAN_EQ:
                return builder.lessThanOrEqualTo(root.get(key), value.toString());
            case LIKE:
            case CONTAINS:
                return builder.like(builder.concat(root.get(key), ""), "%" + value.toString() + "%");
            case NOT_LIKE:
                return builder.notLike(root.get(key), "%" + value.toString() + "%");
            case STARTS_WITH:
                return builder.like(root.get(key), value.toString() + "%");
            case ENDS_WITH:
                return builder.like(root.get(key), "%" + value.toString());
            default:
                return null;
        }
    }

    protected Object convertValueByType(final Class<?> clazz, String key, Object value) {
        Class<?> typeClass;
        try {
            typeClass = clazz.getDeclaredField(key).getType();
        } catch (NoSuchFieldException e) {
            return value;
        }
        if (Boolean.class.equals(typeClass)) {
            return "true".equalsIgnoreCase(value.toString())
                    ? true
                    : ("false".equalsIgnoreCase(value.toString())
                        ? false
                        : value);
        } else if (Integer.class.equals(typeClass)) {
            return Integer.parseInt(value.toString());
        } else if (Double.class.equals(typeClass)) {
            return Double.parseDouble(value.toString());
        } else if (Float.class.equals(typeClass)) {
            return Float.parseFloat(value.toString());
        } else if (Long.class.equals(typeClass)) {
            return Long.parseLong(value.toString());
        } else if (Character.class.equals(typeClass)) {
            return value.toString().codePointAt(0);
        } else {
            return value;
        }
    }

    protected String alias(Class<?> clazz, String key) {
        Method aliasMethod;
        try {
            aliasMethod = clazz.getMethod("aliases");
        } catch (NoSuchMethodException e) {
            return key;
        }

        Object o;
        try {
            o = aliasMethod.invoke(null);
        } catch (IllegalAccessException | InvocationTargetException e) {
            return  key;
        }

        if (o == null) {
            return key;
        } else {
            Map<String, String> aliases = (Map<String, String>) o;
            return aliases.getOrDefault(key, key);
        }
    }

}
