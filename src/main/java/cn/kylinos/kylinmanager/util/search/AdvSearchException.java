package cn.kylinos.kylinmanager.util.search;

public class AdvSearchException extends RuntimeException {
    public AdvSearchException() {
        super();
    }

    public AdvSearchException(String message) {
        super(message);
    }

    public AdvSearchException(String message, Throwable cause) {
        super(message, cause);
    }

    public AdvSearchException(Throwable cause) {
        super(cause);
    }

    protected AdvSearchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
