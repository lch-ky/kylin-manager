package cn.kylinos.kylinmanager.util.search;

public enum SearchOperation {
    /** a = b, a != b, a > b, a < b, a >= b, a <= b, a like b, a not like b,*/
    EQUALITY, NEGATION, GREATER_THAN, LESS_THAN, GREATER_THAN_EQ, LESS_THAN_EQ, LIKE, NOT_LIKE, STARTS_WITH, ENDS_WITH, CONTAINS;

    public static final String[] SIMPLE_OPERATION_SET = { "=", "!=", ">", "<", ">=", "<=", "~", "!~" };

    public static final String OR_PREDICATE_FLAG = "'";

    public static final String ZERO_OR_MORE_REGEX = "*";

    public static final String OR_OPERATOR = "OR";

    public static final String AND_OPERATOR = "AND";

    public static final String LEFT_PARANTHESIS = "(";

    public static final String RIGHT_PARANTHESIS = ")";

    public static SearchOperation getSimpleOperation(final String input) {
        switch (input) {
            case "=":
                return EQUALITY;
            case "!=":
                return NEGATION;
            case ">":
                return GREATER_THAN;
            case "<":
                return LESS_THAN;
            case ">=":
                return GREATER_THAN_EQ;
            case "<=":
                return LESS_THAN_EQ;
            case "~":
                return LIKE;
            case "!~":
                return NOT_LIKE;
            default:
                return null;
        }
    }
}

