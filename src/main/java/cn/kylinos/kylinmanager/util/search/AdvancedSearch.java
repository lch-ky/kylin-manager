package cn.kylinos.kylinmanager.util.search;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public class AdvancedSearch {
    public static<T> Specification<T> createQuerySpec(String search) {
        if (search == null || search.isEmpty()) {
            return Specification.where(emptyAndSpec());
        }
        CriteriaParser parser = new CriteriaParser();
        GenericSpecificationsBuilder<T> specBuilder = new GenericSpecificationsBuilder<>();
        Specification<T> spec = specBuilder.build(parser.parse(search), GeneralSpecification::new);
        return spec;
    }

    public static<T> List<T> findBySearch(String search, JpaSpecificationExecutor<T> repository) {
        Specification<T> spec = createQuerySpec(search);
        return repository.findAll(spec);
    }

    public static<T> SearchResult<T> findBySearch(SearchParam param, JpaSpecificationExecutor<T> repository) {
        Specification<T> spec = createQuerySpec(param.getSearch());
        Pageable page = param.toPageable();
        if (page == null) {
            long count = repository.count(spec);
            Sort sort = param.toSort();
            page = sort == null ? PageRequest.of(0, (int) count) : PageRequest.of(0, (int) count, sort);
        }
        Page<T> resultPage = repository.findAll(spec, page);
        return pageMapSearchResult(resultPage, param.getSearch());
    }

    public static<T> SearchResult<T> findBySearch(String search, JpaSpecificationExecutor<T> repository, Pageable page) {
        Specification<T> spec = createQuerySpec(search);
        Page<T> resultPage = repository.findAll(spec, page);
        return pageMapSearchResult(resultPage, search);
    }

    public static<T> List<T> findBySearch(String search, JpaSpecificationExecutor<T> repository, Sort sort) {
        Specification<T> spec = createQuerySpec(search);
        return repository.findAll(spec, sort);
    }

    public static<T> List<T> findBySearch(Specification<T> query, String search, JpaSpecificationExecutor<T> repository) {
        Specification<T> spec = createQuerySpec(search);
        return repository.findAll(Specification.where(query).and(spec));
    }

    public static<T> SearchResult<T> findBySearch(Specification<T> query, SearchParam param, JpaSpecificationExecutor<T> repository) {
        Specification<T> spec = createQuerySpec(param.getSearch());
        Pageable page = param.toPageable();
        if (page == null) {
            long count = repository.count(Specification.where(query).and(spec));
            Sort sort = param.toSort();
            page = sort == null ? PageRequest.of(0, (int) count) : PageRequest.of(0, (int) count, sort);
        }

        Page<T> resultPage = repository.findAll(Specification.where(query).and(spec), page);
        return pageMapSearchResult(resultPage, param.getSearch());
    }

    public static<T> SearchResult<T> findBySearch(Specification<T> query, String search, JpaSpecificationExecutor<T> repository, Pageable page) {
        Specification<T> spec = createQuerySpec(search);
        Page<T> resultPage = repository.findAll(Specification.where(query).and(spec), page);
        return pageMapSearchResult(resultPage, search);
    }

    public static<T> List<T> findBySearch(Specification<T> query, String search, JpaSpecificationExecutor<T> repository, Sort sort) {
        Specification<T> spec = createQuerySpec(search);
        return repository.findAll(Specification.where(query).and(spec), sort);
    }

    public static<T> SearchResult<T> pageMapSearchResult(Page<T> page, String search) {
        SearchResult<T> result = new SearchResult<>();
        result.setResults(page.getContent());
        result.setPage(page.getPageable().getPageNumber()+1);
        result.setPerPage(page.getPageable().getPageSize());
        result.setPaged(page.getPageable().isPaged());
        result.setTotalPages(page.getTotalPages());
        result.setTotal(page.getTotalElements());
        result.setSearch(search);
        return result;
    }

    public static<Integer> Specification<Integer> emptyAndSpec() {
        return (t, cq, cb) -> cb.equal(cb.literal(1), 1);
    }

    public static<Integer> Specification<Integer> emptyOrSpec() {
        return (t, cq, cb) -> cb.notEqual(cb.literal(1), 1);
    }
}
