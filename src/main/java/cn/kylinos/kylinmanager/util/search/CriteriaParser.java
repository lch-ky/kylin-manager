package cn.kylinos.kylinmanager.util.search;

import org.springframework.data.jpa.domain.Specification;

import java.util.*;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CriteriaParser {

    private static Map<String, Operator> ops;

    private static final String OPERATION_LIST = String.join("|", SearchOperation.SIMPLE_OPERATION_SET);

    private static final Pattern SpecCriteriaRegex = Pattern.compile("^(\\w+?)\\s*(" + OPERATION_LIST + ")\\s*(\\p{Punct}?)(\\w+?)(\\p{Punct}?)$");

    private static final Pattern StringDelimiter = Pattern.compile("\"");
    private static final Pattern StringEscapeDelimiter = Pattern.compile("\\\\\"");
    private static final Pattern TokenDelimiter = Pattern.compile(" and | or |\\(|\\)");

    private static final String LEFT_PARENTHESIS_TYPE = "delimiter_left_par";
    private static final String RIGHT_PARENTHESIS_TYPE = "delimiter_right_par";
    private static final String AND_KEYWORD_TYPE = "keyword_and";
    private static final String OR_KEYWORD_TYPE = "keyword_or";

    private enum Operator {
        OR(1), AND(2);
        final int precedence;

        Operator(int p) {
            precedence = p;
        }
    }

    static {
        Map<String, Operator> tempMap = new HashMap<>();
        tempMap.put(" and ", Operator.AND);
        tempMap.put(" or ", Operator.OR);
        tempMap.put("or", Operator.OR);
        tempMap.put("and", Operator.AND);

        ops = Collections.unmodifiableMap(tempMap);
    }

    private static boolean isHigerPrecedenceOperator(String currOp, String prevOp) {
        return (ops.containsKey(prevOp) && ops.get(prevOp).precedence >= ops.get(currOp).precedence);
    }

    public Deque<?> parse(String searchParam) {

        List<Token> tokenList = new ArrayList<>();

        List<Integer[]> strDelimiters = locateStringDelimiter(searchParam);
        Matcher tokenMatcher = TokenDelimiter.matcher(searchParam);
        int lastIdx = 0;

        for (MatchResult rs : tokenMatcher.results().collect(Collectors.toList())) {
            int pos = rs.start();
            String content = rs.group();

            if (! isTokenInString(pos, strDelimiters)) {
                // Create expr token
                String expr = searchParam.substring(lastIdx, pos).strip();
                if (! expr.isEmpty()) {
                    Token exprToken = new Token("expr", expr);
                    tokenList.add(exprToken);
                }

                // Create delimiter token
                String type = "expr";
                switch (content) {
                    case " and ": type = AND_KEYWORD_TYPE; break;
                    case " or ": type = OR_KEYWORD_TYPE; break;
                    case "(": type = LEFT_PARENTHESIS_TYPE; break;
                    case ")": type = RIGHT_PARENTHESIS_TYPE; break;
                    default:
                        throw new IllegalStateException("Unexpected token: " + content);
                }
                Token delimiterToken = new Token(type, content);
                tokenList.add(delimiterToken);

                lastIdx = rs.end();
            }
        }
        // Create last expr token
        String expr =searchParam.substring(lastIdx).strip();
        if (!expr.isEmpty()) {
            Token exprToken = new Token("expr", expr);
            tokenList.add(exprToken);
        }

        System.out.println(tokenList);


        Deque<Object> output = new LinkedList<>();
        Deque<String> stack = new LinkedList<>();
        tokenList.forEach(token -> {
            String tokenValue = token.getValue();
            String type = token.getType();

            if ( AND_KEYWORD_TYPE.equals(type) || OR_KEYWORD_TYPE.equals(type) ) {
                tokenValue = tokenValue.strip();
                while (!stack.isEmpty() && isHigerPrecedenceOperator(tokenValue, stack.peek())) {
                    output.push(stack.pop()
                            .equalsIgnoreCase(SearchOperation.OR_OPERATOR) ? SearchOperation.OR_OPERATOR : SearchOperation.AND_OPERATOR);
                }
                stack.push(tokenValue.equalsIgnoreCase(SearchOperation.OR_OPERATOR) ? SearchOperation.OR_OPERATOR : SearchOperation.AND_OPERATOR);
            } else if ( LEFT_PARENTHESIS_TYPE.equals(type) ) {
                stack.push(SearchOperation.LEFT_PARANTHESIS);
            } else if ( RIGHT_PARENTHESIS_TYPE.equals(type) ) {
                while (!stack.peek()
                        .equals(SearchOperation.LEFT_PARANTHESIS)) {
                    output.push(stack.pop());
                }
                stack.pop();
            } else {

                Matcher matcher = SpecCriteriaRegex.matcher(tokenValue);
                while (matcher.find()) {
                    output.push(new SpecSearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5)));
                }
            }
        });

        while (!stack.isEmpty()) {
            output.push(stack.pop());
        }
        return output;
    }

    private List<Integer[]> locateStringDelimiter(String searchParam) {
        Matcher delimiterMatcher = StringDelimiter.matcher(searchParam);
        List<Integer> delimiterList = delimiterMatcher.results().map(MatchResult::start).sorted().collect(Collectors.toList());

        Matcher delimiterMatcherEscape = StringEscapeDelimiter.matcher(searchParam);
        List<Integer> delimiterEscapeList = delimiterMatcherEscape.results().map(src -> src.start()+1).sorted().collect(Collectors.toList());

        delimiterList.removeAll(delimiterEscapeList);
        if (delimiterList.size() % 2 != 0) {
            throw new AdvSearchException("Syntax error: \" expected");
        }
        List<Integer[]> delimiterPositions = new ArrayList<>(delimiterList.size() / 2);
        Integer[] delimiters = new Integer[delimiterList.size()];
        delimiters = delimiterList.toArray(delimiters);
        for (int i=0; i<delimiters.length; i+=2) {
            delimiterPositions.add(new Integer[]{delimiters[i], delimiters[i+1]} );
        }
        return delimiterPositions;
    }

    private Boolean isTokenInString(int pos, List<Integer[]> stringDelimiters) {
        for (Integer[] delimiters : stringDelimiters) {
            if (delimiters[0] <= pos && pos < delimiters[1]) {
                return true;
            } else if (delimiters[0] > pos) {
                return false;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        String search = "name = \"123\" and active = true";

        CriteriaParser parser = new CriteriaParser();
        GenericSpecificationsBuilder<Object> specBuilder = new GenericSpecificationsBuilder<>();
        Specification spec = specBuilder.build(parser.parse(search), GeneralSpecification::new);
        System.out.println(spec);
    }


}

