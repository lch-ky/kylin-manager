package cn.kylinos.kylinmanager.util;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

public class ParserExcel {

    public static ArrayList<ArrayList<String>> loadExcel(String filePath) throws Exception {
        ArrayList<ArrayList<String>> excelList = new ArrayList<ArrayList<String>>();
        try
        {
            InputStream is = new FileInputStream(filePath);
            Workbook wb = new HSSFWorkbook(is);
            Sheet sheet = wb.getSheetAt(0);
            int maxRows = sheet.getPhysicalNumberOfRows();
            for (int nRow = 0; nRow < maxRows; nRow++)
            {
                Row curRow = sheet.getRow(nRow);
                if (curRow == null)
                {
                    continue;
                }
                ArrayList<String> rowList = new ArrayList<String>();
                int curColumns = curRow.getPhysicalNumberOfCells();
                for (int nColumns = 0; nColumns < curColumns; nColumns++)
                {
                    rowList.add(getCellFormatValue(curRow.getCell(nColumns)));
                }
                excelList.add(rowList);
            }
        }
        catch (Exception e)
        {
            throw new Exception("不支持的文件格式");
        }
        return excelList;
    }

    public static ArrayList<ArrayList<String>> loadExcel(InputStream is) throws Exception {
        ArrayList<ArrayList<String>> excelList = new ArrayList<ArrayList<String>>();
        try
        {
            Workbook wb = new HSSFWorkbook(is);
            Sheet sheet = wb.getSheetAt(0);
            int maxRows = sheet.getPhysicalNumberOfRows();
            for (int nRow = 0; nRow < maxRows; nRow++)
            {
                Row curRow = sheet.getRow(nRow);
                if (curRow == null)
                {
                    continue;
                }
                ArrayList<String> rowList = new ArrayList<String>();
                int curColumns = curRow.getPhysicalNumberOfCells();
                for (int nColumns = 0; nColumns < curColumns; nColumns++)
                {
                    rowList.add(getCellFormatValue(curRow.getCell(nColumns)));
                }
                excelList.add(rowList);
            }
        }
        catch (Exception e)
        {
            throw new Exception("不支持的文件格式");
        }
        return excelList;
    }

    private static String getCellFormatValue(Cell cell)
    {
        String cellValue = null;
        if(cell!=null)
        {
            //判断cell类型
            switch(cell.getCellType())
            {
                case NUMERIC:
                {
                    cellValue = String.valueOf(cell.getNumericCellValue());
                    break;
                }
                case FORMULA:
                {
                    //判断cell是否为日期格式
                    if(DateUtil.isCellDateFormatted(cell)){
                        //转换为日期格式YYYY-mm-dd
                        cellValue = cell.getDateCellValue().toString();
                    }else{
                        //数字
                        cellValue = String.valueOf(cell.getNumericCellValue());
                    }
                    break;
                }
                case STRING:
                {
                    cellValue = cell.getRichStringCellValue().getString();
                    break;
                }
                default:
                {
                    cellValue = "";
                }

            }
        }else{
            cellValue = "";
        }
        return cellValue;
    }
}
