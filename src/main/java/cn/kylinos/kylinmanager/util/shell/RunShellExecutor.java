package cn.kylinos.kylinmanager.util.shell;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

@Slf4j
public class RunShellExecutor {

    private static final Integer PROCESSOR_COUNT = Runtime.getRuntime().availableProcessors();
    static Semaphore semaphore = new Semaphore(PROCESSOR_COUNT >= 3 ? (int) (PROCESSOR_COUNT / 8.0 * 5) : 1 );


    public static String runTask(String command){
        try {
            semaphore.acquire();

            ProcessBuilder builder = new ProcessBuilder();
            builder.redirectErrorStream(true);
            builder.command(command.split(" "));

            Process process = builder.start();
            StringBuilder result = new StringBuilder();
            final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line).append('\n');
                    log.info(builder.command().toString() + " --->: " + line);
                }
            } catch (IOException e) {
                log.warn("failed to read output from process", e);
            } finally {
                reader.close();
            }
            boolean rs = process.waitFor(120, TimeUnit.SECONDS);
            int exitCode = process.exitValue();
            semaphore.release();
            return result.toString();
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}