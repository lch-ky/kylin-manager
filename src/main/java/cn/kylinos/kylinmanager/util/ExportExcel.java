package cn.kylinos.kylinmanager.util;

import cn.kylinos.kylinmanager.common.KylinException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

public class ExportExcel {

    public static HSSFWorkbook exportExcel(String[] head, List<?> list, String table) throws Exception{
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        HSSFSheet sheet = workbook.createSheet(table);
        sheet.setDefaultColumnWidth(10);
        HSSFRow headRow = sheet.createRow(0);
        for (int i = 0; i < head.length; i++) {
            HSSFCell cell = headRow.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(head[i]);
            cell.setCellValue(text);
            cell.setCellStyle(cellStyle);
        }
        for (int i = 0; i < list.size(); i++) {
            HSSFRow row = sheet.createRow(i + 1);
            Object o = list.get(i);
            Field[] fields = o.getClass().getDeclaredFields();
            for (int j = 0; j < fields.length; j++) {
                HSSFCell cell = row.createCell(j);
                fields[j].setAccessible(true);
                String name = fields[j].getName();
                String value = getFieldValueByName(name, o) == null ? "" : getFieldValueByName(name, o).toString();
                HSSFRichTextString text = new HSSFRichTextString(value);
                cell.setCellValue(text);
            }
        }
        return workbook;
    }
    private static Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter);
            return method.invoke(o);
        } catch (Exception e) {
            throw new KylinException();
        }
    }
}
