package cn.kylinos.kylinmanager.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Objects;

@Component
public class MsgUtil {
    private static MessageSource messageSource;

    @Autowired
    public MsgUtil(MessageSource messageSource) {
        MsgUtil.messageSource = messageSource;
    }

    /**
     * Get translated message from resource bundle
     * @param locale Language used
     * @param code Message code(unique)
     * @return translated message, null if locale not found or code not found in resources
     */
    public static String get(Locale locale, String code) {
        return get(locale, null, code, null);
    }

    /**
     * Get translated message from resource bundle
     * @param locale Language used
     * @param code Message code(unique)
     * @param args Arguments passed to message placeholder like {0}, {1} etc.
     * @return translated message, null if locale not found or code not found in resources
     */
    public static String get(Locale locale, String code, Object... args) {
        return get(locale, null, code, args);
    }

    /**
     * Get translated message from resource bundle
     * @param locale locale Language used
     * @param defaultMessage default message to be returned when code is not found in resources
     * @param code Message code(unique)
     * @param args Arguments passed to message placeholder like {0}, {1} etc.
     * @return translated message, null if locale not found or code not found in resources
     */
    public static String get(Locale locale, @Nullable String defaultMessage, String code, Object[] args) {
        if (Objects.isNull(locale) || Objects.isNull(code)) {
            return null;
        }
        return messageSource.getMessage(code, args, defaultMessage, locale);
    }

    /**
     * Get translated message from resource bundle in context locale
     * <b>The default locale is from LocaleContextHolder.getLocale(). You MUST know WHAT HAPPENED when context not available.</b>
     * @param code Message code(unique)
     * @return translated message, null if locale not found or code not found in resources
     */
    public static String get(String code) {
        return get(LocaleContextHolder.getLocale(), null, code, null);
    }

    /**
     * Get translated message from resource bundle in context locale
     * <b>The default locale is from LocaleContextHolder.getLocale(). You MUST know WHAT HAPPENED when context not available.</b>
     * @param code Message code(unique)
     * @param args Arguments passed to message placeholder like {0}, {1} etc.
     * @return translated message, null if locale not found or code not found in resources
     */
    public static String get(String code, Object... args) {
        return get(LocaleContextHolder.getLocale(), null, code, args);
    }

}
