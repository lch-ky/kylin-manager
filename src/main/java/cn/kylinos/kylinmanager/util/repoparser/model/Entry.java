package cn.kylinos.kylinmanager.util.repoparser.model;

public class Entry {
    private String name;

    private Short flags;

    private String epoch;

    private String version;

    private String release;

    public Entry(String name) {
        this.name = name;
        this.flags = 0;
    }

    public Entry(String name, Short flags, String epoch, String version, String release) {
        this.name = name;
        this.flags = flags;
        this.epoch = epoch;
        this.version = version;
        this.release = release;
    }

    public Entry(String name, String flags, String epoch, String version, String release) {
        this.name = name;
        this.flags = flagsToBit(flags);
        this.epoch = epoch;
        this.version = version;
        this.release = release;
    }

    public static Short flagsToBit(String flags) {
        if (flags == null) {
            return 0;
        }
        switch (flags) {
            case "EQ": return 8;
            case "LT": return 2;
            case "GT": return 4;
            case "LE": return 10;
            case "GE": return 12;
            default: return 0;
        }
    }

    public String getName() {
        return name;
    }

    public Short getFlags() {
        return flags;
    }

    public String getEpoch() {
        return epoch;
    }

    public String getVersion() {
        return version;
    }

    public String getRelease() {
        return release;
    }
}
