package cn.kylinos.kylinmanager.util.repoparser;

import cn.kylinos.kylinmanager.util.repoparser.model.Evr;
import cn.kylinos.kylinmanager.util.repoparser.model.File;
import cn.kylinos.kylinmanager.util.repoparser.model.FileType;
import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Filelists.xml parser
 *
 */

class FilelistsHandler extends RepoHandler {
    private final static List<String> supportedTag = new ArrayList<>();
    static {
        supportedTag.add("package");
        supportedTag.add("version");
        supportedTag.add("file");
    }

    public FilelistsHandler(Map<String, Package> packageMap) {
        super(packageMap);
    }

    @Override
    public void startDocument() throws SAXException {
        print("start document");
    }

    @Override
    public void endDocument() throws SAXException {
        print("end document");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//		 print("start element:", localName, qName);
        if ("package".equals(qName)) {
            String pkgid = attributes.getValue("pkgid");
            Package pack;
            if (! packageMap.containsKey(pkgid)) {
                pack = new Package(pkgid);
            } else {
                pack = packageMap.get(pkgid);
                packageMap.put(pkgid, pack);
            }
            pack.setName(attributes.getValue("name"));
            pack.setArch(attributes.getValue("arch"));
            parseStack.push(pack);

        } else if ("version".equals(qName) && checkStackTopType(Package.class) ) {
            Package pack = (Package) parseStack.peek();
            Evr evr = new Evr(attributes.getValue("epoch"),
                    attributes.getValue("ver"), attributes.getValue("rel"));
            pack.setEvr(evr);
            parseStack.push(evr);
        } else if ("file".equals(qName) && checkStackTopType(Package.class)) {
            Package pack = (Package) parseStack.peek();
            File file = new File();
            if ("dir".equals(attributes.getValue("type"))) {
                file.setType(FileType.DIRECTORY);
            } else {
                file.setType(FileType.FILE);
            }
            pack.addFile(file);
            parseStack.push(file);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (supportedTag.contains(qName)) {
            parseStack.pop();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (checkStackTopType(File.class)) {
            File file = (File) parseStack.peek();
            file.setPath(new String(ch, start, length));
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        print("error:", e);
    }

}
