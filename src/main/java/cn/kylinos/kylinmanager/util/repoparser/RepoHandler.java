package cn.kylinos.kylinmanager.util.repoparser;

import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import org.xml.sax.helpers.DefaultHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public abstract class RepoHandler extends DefaultHandler {

    protected Stack<Object> parseStack;

    protected Map<String, Package> packageMap;

    public RepoHandler() {
        this.parseStack = new Stack<>();
        this.packageMap = new HashMap<>();
    }

    public RepoHandler(Map<String, Package> packageMap) {
        this.parseStack = new Stack<>();
        this.packageMap = packageMap;
    }

    public Map<String, Package> getPackageMap() {
        return this.packageMap;
    }

    protected Boolean checkStackTopType(Class<?> clazz) {
        return !parseStack.isEmpty() && clazz.isInstance(parseStack.peek());
    }

    protected void print(Object... objs) {
        for (Object obj : objs) {
            System.out.print(obj);
            System.out.print(" ");
        }
        System.out.println();
    }
}
