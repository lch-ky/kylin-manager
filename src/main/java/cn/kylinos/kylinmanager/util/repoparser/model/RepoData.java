package cn.kylinos.kylinmanager.util.repoparser.model;

public class RepoData {
    private String type;

    private Checksum checksum;

    private Checksum openChecksum;

    private String href;

    private Long timestamp;

    private Long size;

    private Long openSize;

    public RepoData(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Checksum getChecksum() {
        return checksum;
    }

    public void setChecksum(Checksum checksum) {
        this.checksum = checksum;
    }

    public Checksum getOpenChecksum() {
        return openChecksum;
    }

    public void setOpenChecksum(Checksum openChecksum) {
        this.openChecksum = openChecksum;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getOpenSize() {
        return openSize;
    }

    public void setOpenSize(Long openSize) {
        this.openSize = openSize;
    }
}
