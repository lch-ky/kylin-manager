package cn.kylinos.kylinmanager.util.repoparser.model;

public class ChangeLog {
    private String author;

    private Long date;

    private String content;

    public ChangeLog(String author, String date) {
        this.author = author;
        this.date = Long.parseLong(date);
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public Long getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }
}
