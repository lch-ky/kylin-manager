package cn.kylinos.kylinmanager.util.repoparser.model;

public class Evr {
    private String epoch;

    private String version;

    private String release;

    public Evr(String epoch, String version, String release) {
        this.epoch = epoch;
        this.version = version;
        this.release = release;
    }

    public String getEpoch() {
        return epoch;
    }

    public String getVersion() {
        return version;
    }

    public String getRelease() {
        return release;
    }
}
