package cn.kylinos.kylinmanager.util.repoparser;

import cn.kylinos.kylinmanager.util.repoparser.model.Checksum;
import cn.kylinos.kylinmanager.util.repoparser.model.RepoData;
import cn.kylinos.kylinmanager.util.repoparser.model.Repomd;
import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class RepomdParser {
    private static final long MEGABYTE = 1024L * 1024L;
    private static final String REPOMD_RELATIVE_PATH = "repodata/repomd.xml";

    private static final Map<String, String> DIGEST_TYPE_MAP = new HashMap<String, String>();
    static {
        DIGEST_TYPE_MAP.put("sha256", "SHA-256");
    }

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

    public static Repomd parseRepomd(URL baseUrl) throws IOException, ParserConfigurationException, SAXException {
        URL repomdUrl = new URL(baseUrl, REPOMD_RELATIVE_PATH);
        URLConnection conn = repomdUrl.openConnection();
        InputStream input = conn.getInputStream();
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser saxParser = spf.newSAXParser();
        RepomdHandler repomdHandler = new RepomdHandler();
        saxParser.parse(input, repomdHandler);
        return repomdHandler.getRepomd();
    }

    public static Map<String, Package> parseComponent(String type, URL baseUrl, Repomd repomd, Map<String, Package> packageMap) throws IOException, ParserConfigurationException, SAXException {
        String relativePath = null;
        RepoHandler repomdHandler;
        Checksum checksum = null;
        if ("primary".equals(type)) {
            RepoData primary = repomd.getPrimary();
            if (primary == null) {
                throw new RepoException("Undefined primary.xml info");
            }
            relativePath = primary.getHref();
            if (StringUtils.isEmpty(relativePath)) {
                throw new RepoException("Empty primary.xml location href");
            }
            System.out.println("baseUrl when parse ="+baseUrl.toString());
            repomdHandler = new PrimaryHandler(baseUrl);
            checksum = primary.getChecksum();
        } else if ("filelists".equals(type)) {
            RepoData filelists = repomd.getFilelists();
            if (filelists == null) {
                throw new RepoException("Undefined filelists.xml info");
            }
            relativePath = filelists.getHref();
            if (StringUtils.isEmpty(relativePath)) {
                throw new RepoException("Empty filelists.xml location href");
            }
            repomdHandler = new FilelistsHandler(packageMap);
            checksum = filelists.getChecksum();
        } else if ("other".equals(type)) {
            RepoData other = repomd.getOther();
            if (other == null) {
                throw new RepoException("Undefined other.xml info");
            }
            relativePath = other.getHref();
            if (StringUtils.isEmpty(relativePath)) {
                throw new RepoException("Empty other.xml location href");
            }
            repomdHandler = new OtherHandler(packageMap);
            checksum = other.getChecksum();
        } else {
            throw new RepoException("Unknown type of repo component");
        }

        URL repomdUrl = null;
        try {
            repomdUrl = new URL(baseUrl, relativePath);
        } catch (MalformedURLException e) {
            throw new RepoException(e);
        }
        URLConnection conn = repomdUrl.openConnection();
        byte[] content = downloadToBytes(conn.getInputStream());
        InputStream checksumInput = new ByteArrayInputStream(content);
        try {
            if (! fileChecksum(checksumInput, checksum )) {
                throw new RepoException("Type: "+type+" checksum not matched");
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RepoException("Unsupported checksum type: "+checksum.getType());
        }
        InputStream input = new GZIPInputStream(new ByteArrayInputStream(content));
        SAXParserFactory spf = SAXParserFactory.newInstance();
        SAXParser saxParser = spf.newSAXParser();
        saxParser.parse(input, repomdHandler);
        return repomdHandler.getPackageMap();
    }

    private static byte[] downloadToBytes(InputStream input) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while ((n = input.read(buf)) >= 0) {
            baos.write(buf, 0, n);
        }
        return baos.toByteArray();
    }

    public static Boolean fileChecksum(InputStream input, Checksum checksum) throws NoSuchAlgorithmException, IOException {
        if (checksum == null) {
            return Boolean.TRUE;
        }
        MessageDigest md = MessageDigest.getInstance(DIGEST_TYPE_MAP.get(checksum.getType()));
        try (DigestInputStream dis = new DigestInputStream(input, md)) {
            while (dis.read() != -1) {;}
            md = dis.getMessageDigest();
        }

        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }
        System.out.println("checksum:"+result.toString()+", "+checksum.getChecksum());
        return result.toString().equals(checksum.getChecksum());
    }

    public static Map<String, Package> parseRepo(String repoBaseUrl) throws IOException, ParserConfigurationException, SAXException {
        URL baseUrl = new URL(repoBaseUrl);
        Repomd repomd = parseRepomd(baseUrl);

        Map<String, Package> packageMap = parseComponent("primary", baseUrl, repomd, null);
        parseComponent("filelists", baseUrl, repomd, packageMap);
        parseComponent("other", baseUrl, repomd, packageMap);
        return packageMap;
    }

    public static void main(String[] args) throws Exception {
        long st = System.currentTimeMillis();
        String repoBaseUrl = "http://10.1.163.73/test/v10/";

        Map<String, Package> packageMap = parseRepo(repoBaseUrl);

        long ed = System.currentTimeMillis();
        System.out.println("Parse done time = "+(ed - st) + " ms");

        Runtime runtime = Runtime.getRuntime();
        long memory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Used memory is bytes: " + memory);
        System.out.println("Used memory is megabytes: "
                + bytesToMegabytes(memory));

        System.out.println(packageMap);
        for (Package pack : packageMap.values()) {
            if (pack.getFilelist().isEmpty()) {
                System.out.println("Empty filelist "+pack.getNvrea());
            }
        }
    }
}
