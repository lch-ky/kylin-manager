package cn.kylinos.kylinmanager.util.repoparser.model;

public class Repomd {
    private RepoData primary;

    private RepoData filelists;

    private RepoData other;

    public void setPrimary(RepoData primary) {
        this.primary = primary;
    }

    public void setFilelists(RepoData filelists) {
        this.filelists = filelists;
    }

    public void setOther(RepoData other) {
        this.other = other;
    }

    public RepoData getPrimary() {
        return primary;
    }

    public RepoData getFilelists() {
        return filelists;
    }

    public RepoData getOther() {
        return other;
    }
}
