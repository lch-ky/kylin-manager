package cn.kylinos.kylinmanager.util.repoparser.model;

public class Node {
    private String tag;

    private String value;

    public Node(String tag) {
        this.tag = tag;
    }

    public Node(String tag, String value) {
        this.tag = tag;
        this.value = value;
    }

    public String getTag() {
        return tag;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
