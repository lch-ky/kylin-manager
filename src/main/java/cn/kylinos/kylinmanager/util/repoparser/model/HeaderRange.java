package cn.kylinos.kylinmanager.util.repoparser.model;

public class HeaderRange {
    private Long start;

    private Long end;

    public HeaderRange(String start, String end) {
        this.start = Long.parseLong(start);
        this.end = Long.parseLong(end);
    }

    public Long getStart() {
        return start;
    }

    public Long getEnd() {
        return end;
    }
}
