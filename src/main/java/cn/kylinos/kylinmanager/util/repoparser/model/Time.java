package cn.kylinos.kylinmanager.util.repoparser.model;

public class Time {
    private Long fileTime;

    private Long buildTime;

    public Time(String fileTime, String buildTime) {
        this.fileTime = Long.parseLong(fileTime);
        this.buildTime = Long.parseLong(buildTime);
    }

    public Long getFileTime() {
        return fileTime;
    }

    public Long getBuildTime() {
        return buildTime;
    }
}
