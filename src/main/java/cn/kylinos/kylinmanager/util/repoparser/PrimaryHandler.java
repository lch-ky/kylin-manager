package cn.kylinos.kylinmanager.util.repoparser;

import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import cn.kylinos.kylinmanager.util.repoparser.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class PrimaryHandler extends RepoHandler {

    public final static String TAG_NAME = "name";
    public final static String TAG_ARCH = "arch";
    public final static String TAG_VERSION = "version";
    public final static String TAG_CHECKSUM = "checksum";
    public final static String TAG_SUMMARY = "summary";
    public final static String TAG_DESCRIPTION = "description";
    public final static String TAG_PACKAGER = "packager";
    public final static String TAG_URL = "url";
    public final static String TAG_TIME = "time";
    public final static String TAG_SIZE = "size";
    public final static String TAG_LOCATION = "location";
    public final static String TAG_LICENSE = "rpm:license";
    public final static String TAG_VENDOR = "rpm:vendor";
    public final static String TAG_GROUP = "rpm:group";
    public final static String TAG_BUILDHOST = "rpm:buildhost";
    public final static String TAG_SRPM = "rpm:sourcerpm";
    public final static String TAG_HEADER_RANGE = "rpm:header-range";
    public final static String TAG_PROVIDES = "rpm:provides";
    public final static String TAG_ENTRY = "rpm:entry";
    public final static String TAG_REQUIRES = "rpm:requires";
    public final static String TAG_OBSOLETES = "rpm:obsoletes";
    public final static String TAG_CONFLICTS = "rpm:conflicts";
    public final static String TAG_RECOMMENDS = "rpm:recommends";
    public final static String TAG_SUGGESTS = "rpm:suggests";
    public final static String TAG_SUPPLEMENTS = "rpm:supplements";
    public final static String TAG_ENHANCES = "rpm:enhances";

    public final static String ATTR_PKGID = "pkgid";

    public final static String YES = "YES";

    private final static Set<String> nodeTags = new HashSet<>();
    static {
        nodeTags.add(TAG_NAME);
        nodeTags.add(TAG_ARCH);
        nodeTags.add(TAG_SUMMARY);
        nodeTags.add(TAG_DESCRIPTION);
        nodeTags.add(TAG_PACKAGER);
        nodeTags.add(TAG_URL);
        nodeTags.add(TAG_LICENSE);
        nodeTags.add(TAG_VENDOR);
        nodeTags.add(TAG_GROUP);
        nodeTags.add(TAG_BUILDHOST);
        nodeTags.add(TAG_SRPM);
    }

    private final static Set<String> SUPPORTED_TAG = new HashSet<>();
    static {
        SUPPORTED_TAG.add("package");
        SUPPORTED_TAG.add(TAG_VERSION);
        SUPPORTED_TAG.add(TAG_CHECKSUM);
        SUPPORTED_TAG.add(TAG_TIME);
        SUPPORTED_TAG.add(TAG_SIZE);
        SUPPORTED_TAG.add(TAG_LOCATION);
        SUPPORTED_TAG.add(TAG_HEADER_RANGE);
        SUPPORTED_TAG.add(TAG_PROVIDES);
        SUPPORTED_TAG.add(TAG_ENTRY);
        SUPPORTED_TAG.add(TAG_REQUIRES);
        SUPPORTED_TAG.add(TAG_OBSOLETES);
        SUPPORTED_TAG.add(TAG_CONFLICTS);
        SUPPORTED_TAG.add(TAG_RECOMMENDS);
        SUPPORTED_TAG.add(TAG_SUGGESTS);
        SUPPORTED_TAG.add(TAG_SUPPLEMENTS);
        SUPPORTED_TAG.add(TAG_ENHANCES);
        SUPPORTED_TAG.addAll(nodeTags);
    }

    private static final Set<String> IGNORE_TAGS = new HashSet<>();
    static {
        IGNORE_TAGS.add("file");
        IGNORE_TAGS.add("format");
        IGNORE_TAGS.add("metadata");

    }

    private URL baseUrl;

    public PrimaryHandler(URL baseUrl) {
        super();
        this.baseUrl = baseUrl;
    }

    public PrimaryHandler(Map<String, Package> packageMap) {
        super(packageMap);
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ("package".equals(qName)) {
            String type = attributes.getValue("type");
            if ("rpm".equals(type)) {
                Package pack = new Package();
                parseStack.push(pack);
            }
        } else if (nodeTags.contains(qName) && checkStackTopType(Package.class)) {
            parseStack.push(new Node(qName));
        } else if (TAG_VERSION.equals(qName) && checkStackTopType(Package.class)) {
            Evr evr = new Evr(attributes.getValue("epoch"),
                    attributes.getValue("ver"), attributes.getValue("rel"));
            parseStack.push(evr);
        } else if (TAG_CHECKSUM.equals(qName) && checkStackTopType(Package.class)) {
            Boolean pkgid = YES.equals(attributes.getValue(ATTR_PKGID));
            Checksum checksum = new Checksum(attributes.getValue("type"), pkgid);
            parseStack.push(checksum);
        } else if (TAG_TIME.equals(qName) && checkStackTopType(Package.class)) {
            Time time = new Time(attributes.getValue("file"), attributes.getValue("build"));
            parseStack.push(time);
        } else if (TAG_SIZE.equals(qName) && checkStackTopType(Package.class)) {
            Size size = new Size(attributes.getValue("package"), attributes.getValue("installed"),
                    attributes.getValue("archive"));
            parseStack.push(size);
        } else if (TAG_LOCATION.equals(qName) && checkStackTopType(Package.class)) {
            parseStack.push(new Node(qName, attributes.getValue("href")));
        } else if (TAG_HEADER_RANGE.equals(qName) && checkStackTopType(Package.class)) {
            HeaderRange headerRange = new HeaderRange(attributes.getValue("start"), attributes.getValue("end"));
            parseStack.push(headerRange);
        } else if (TAG_PROVIDES.equals(qName) && checkStackTopType(Package.class)) {
            PkgProvides provides = new PkgProvides();
            parseStack.push(provides);
        } else if (TAG_REQUIRES.equals(qName) && checkStackTopType(Package.class)) {
            PkgRequires requires = new PkgRequires();
            parseStack.push(requires);
        } else if (TAG_OBSOLETES.equals(qName) && checkStackTopType(Package.class)) {
            PkgObsoletes obsoletes = new PkgObsoletes();
            parseStack.push(obsoletes);
        } else if (TAG_CONFLICTS.equals(qName) && checkStackTopType(Package.class)) {
            PkgConflicts conflicts = new PkgConflicts();
            parseStack.push(conflicts);
        } else if (TAG_RECOMMENDS.equals(qName) && checkStackTopType(Package.class)) {
            PkgRecommends recommends = new PkgRecommends();
            parseStack.push(recommends);
        } else if (TAG_SUGGESTS.equals(qName) && checkStackTopType(Package.class)) {
            PkgSuggests suggests = new PkgSuggests();
            parseStack.push(suggests);
        } else if (TAG_SUPPLEMENTS.equals(qName) && checkStackTopType(Package.class)) {
            PkgSupplements supplements = new PkgSupplements();
            parseStack.push(supplements);
        } else if (TAG_ENHANCES.equals(qName) && checkStackTopType(Package.class)) {
            PkgEnhances enhances = new PkgEnhances();
            parseStack.push(enhances);
        } else if (TAG_ENTRY.equals(qName) && checkStackTopType(PkgRelation.class)) {
            String flags = attributes.getValue("flags");
            List<String> knownFlags = new ArrayList<>();
            knownFlags.add("EQ");
            knownFlags.add("LT");
            knownFlags.add("GT");
            knownFlags.add("LE");
            knownFlags.add("GE");
            Entry entry;
            if (flags == null) {
                entry = new Entry(attributes.getValue("name"));
            } else {
                if (! knownFlags.contains(flags)) {
                    System.out.println("Unknown flags "+flags);
                }
                entry = new Entry(attributes.getValue("name"), flags, attributes.getValue("epoch"),
                        attributes.getValue("ver"), attributes.getValue("rel"));
            }
            parseStack.push(entry);
        } else if (!IGNORE_TAGS.contains(qName)) {
            System.out.println("Parse primary.xml, Unknown Tag : " + qName);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (!SUPPORTED_TAG.contains(qName)) {
            return;
        }

        if (checkStackTopType(Package.class)) {
            Package pack = (Package) parseStack.pop();
            if (pack.getPkgid() != null) {
                this.packageMap.put(pack.getPkgid(), pack);
            } else {
                this.packageMap.put(pack.getNvrea(), pack);
            }
        } else if (checkStackTopType(Node.class)) {
            Node node = (Node) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            if (TAG_NAME.equals(node.getTag())) {
                pack.setName(node.getValue());
            } else if (TAG_ARCH.equals(node.getTag())) {
                pack.setArch(node.getValue());
            } else if (TAG_SUMMARY.equals(node.getTag())) {
                pack.setSummary(node.getValue());
            } else if (TAG_DESCRIPTION.equals(node.getTag())) {
                pack.setDescription(node.getValue());
            } else if (TAG_PACKAGER.equals(node.getTag())) {
                pack.setPackager(node.getValue());
            } else if (TAG_URL.equals(node.getTag())) {
                pack.setUrl(node.getValue());
            } else if (TAG_LOCATION.equals(node.getTag())) {
                URL href = null;
                try {
                    href = new URL(this.baseUrl, node.getValue());
                    pack.setHref(href.toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            } else if (TAG_LICENSE.equals(node.getTag())) {
                pack.setLicense(node.getValue());
            } else if (TAG_VENDOR.equals(node.getTag())) {
                pack.setVendor(node.getValue());
            } else if (TAG_GROUP.equals(node.getTag())) {
                pack.setGroup(node.getValue());
            } else if (TAG_BUILDHOST.equals(node.getTag())) {
                pack.setBuildHost(node.getValue());
            } else if (TAG_SRPM.equals(node.getTag())) {
                pack.setSrpm(node.getValue());
            }
        } else if (checkStackTopType(Evr.class)) {
            Evr evr = (Evr) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setEvr(evr);
        } else if (checkStackTopType(Checksum.class)) {
            Checksum checksum = (Checksum) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setChecksum(checksum);
        } else if (checkStackTopType(Time.class)) {
            Time time = (Time) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setTime(time);
        } else if (checkStackTopType(Size.class)) {
            Size size = (Size) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setSize(size);
        } else if (checkStackTopType(HeaderRange.class)) {
            HeaderRange headerRange = (HeaderRange) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setHeaderRange(headerRange);
        } else if (checkStackTopType(PkgProvides.class)) {
            PkgProvides provides = (PkgProvides) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setProvides(provides.getEntries());
        } else if (checkStackTopType(PkgRequires.class)) {
            PkgRequires requires = (PkgRequires) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setRequires(requires.getEntries());
        } else if (checkStackTopType(PkgObsoletes.class)) {
            PkgObsoletes obsoletes = (PkgObsoletes) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setObsoletes(obsoletes.getEntries());
        } else if (checkStackTopType(PkgConflicts.class)) {
            PkgConflicts conflicts = (PkgConflicts) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setConflicts(conflicts.getEntries());
        } else if (checkStackTopType(PkgRecommends.class)) {
            PkgRecommends recommends = (PkgRecommends) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setRecommends(recommends.getEntries());
        } else if (checkStackTopType(PkgSuggests.class)) {
            PkgSuggests suggests = (PkgSuggests) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setSuggests(suggests.getEntries());
        } else if (checkStackTopType(PkgSupplements.class)) {
            PkgSupplements supplements = (PkgSupplements) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setSupplements(supplements.getEntries());
        } else if (checkStackTopType(PkgEnhances.class)) {
            PkgEnhances enhances = (PkgEnhances) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.setEnhances(enhances.getEntries());
        } else if (checkStackTopType(Entry.class)) {
            Entry entry = (Entry) parseStack.pop();
            PkgRelation relation = (PkgRelation) parseStack.peek();
            relation.addEntry(entry);
        } else {
            parseStack.pop();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String content = new String(ch, start, length);
        if (checkStackTopType(Node.class)) {
            Node node = (Node) parseStack.peek();
            node.setValue(node.getValue() == null ? content : node.getValue()+content);
        } else if (checkStackTopType(Checksum.class)) {
            Checksum checksum = (Checksum) parseStack.peek();
            checksum.setChecksum(content);
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        super.error(e);
    }
}
