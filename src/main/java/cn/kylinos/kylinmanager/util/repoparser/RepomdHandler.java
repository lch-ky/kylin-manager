package cn.kylinos.kylinmanager.util.repoparser;


import cn.kylinos.kylinmanager.util.repoparser.model.Checksum;
import cn.kylinos.kylinmanager.util.repoparser.model.Node;
import cn.kylinos.kylinmanager.util.repoparser.model.RepoData;
import cn.kylinos.kylinmanager.util.repoparser.model.Repomd;
import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RepomdHandler extends RepoHandler {

    private Repomd repomd;

    public static final String TAG_DATA = "data";
    public static final String TAG_CHECKSUM = "checksum";
    public static final String TAG_OPEN_CHECKSUM = "open-checksum";
    public static final String TAG_LOCATION = "location";
    public static final String TAG_TIMESTAMP = "timestamp";
    public static final String TAG_SIZE = "size";
    public static final String TAG_OPEN_SIZE = "open-size";

    public final static String TYPE_PRIMARY = "primary";
    public final static String TYPE_FILELISTS = "filelists";
    public final static String TYPE_OTHER = "other";

    private final static Set<String> nodeTags = new HashSet<>();
    static {
        nodeTags.add(TAG_TIMESTAMP);
        nodeTags.add(TAG_SIZE);
        nodeTags.add(TAG_OPEN_SIZE);
    }

    private final static Set<String> supportedTag = new HashSet<>();
    static {
        supportedTag.add(TAG_DATA);
        supportedTag.add(TAG_CHECKSUM);
        supportedTag.add(TAG_OPEN_CHECKSUM);
        supportedTag.add(TAG_LOCATION);
        supportedTag.addAll(nodeTags);
    }

    private final static Set<String> dataTypes = new HashSet<>();
    static {
        dataTypes.add(TYPE_PRIMARY);
        dataTypes.add(TYPE_FILELISTS);
        dataTypes.add(TYPE_OTHER);
    }

    public RepomdHandler() {
        this.repomd = new Repomd();
    }

    public RepomdHandler(Map<String, Package> packageMap) {
        super(packageMap);
        this.repomd = new Repomd();
    }

    public Repomd getRepomd() {
        return repomd;
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (TAG_DATA.equals(qName)) {
            String type = attributes.getValue("type");
            if (dataTypes.contains(type)) {
                RepoData primary = new RepoData(type);
                parseStack.push(primary);
            }
        } else if ((TAG_CHECKSUM.equals(qName) || TAG_OPEN_CHECKSUM.equals(qName)) && checkStackTopType(RepoData.class)) {
            Checksum checksum = new Checksum(attributes.getValue("type"), Boolean.TRUE);
            parseStack.push(checksum);
        } else if (TAG_LOCATION.equals(qName) && checkStackTopType(RepoData.class)) {
            Node location = new Node("location");
            location.setValue(attributes.getValue("href"));
            parseStack.push(location);
        } else if (nodeTags.contains(qName) && checkStackTopType(RepoData.class)) {
            parseStack.push(new Node(qName));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (! supportedTag.contains(qName)) {
            return;
        }
        if (TAG_DATA.equals(qName) && checkStackTopType(RepoData.class)) {
            RepoData repoData = (RepoData) parseStack.pop();
            if (TYPE_PRIMARY.equals(repoData.getType() )) {
                this.repomd.setPrimary(repoData);
            } else if (TYPE_FILELISTS.equals(repoData.getType() )) {
                this.repomd.setFilelists(repoData);
            } else if (TYPE_OTHER.equals(repoData.getType() )) {
                this.repomd.setOther(repoData);
            }
        } else if (TAG_CHECKSUM.equals(qName) && checkStackTopType(Checksum.class)) {
            Checksum checksum = (Checksum) parseStack.pop();
            RepoData repoData = (RepoData) parseStack.peek();
            repoData.setChecksum(checksum);
        } else if (TAG_OPEN_CHECKSUM.equals(qName) && checkStackTopType(Checksum.class)) {
            Checksum checksum = (Checksum) parseStack.pop();
            RepoData repoData = (RepoData) parseStack.peek();
            repoData.setOpenChecksum(checksum);
        } else if (TAG_LOCATION.equals(qName) && checkStackTopType(Node.class)) {
            Node location = (Node) parseStack.pop();
            RepoData repoData = (RepoData) parseStack.peek();
            repoData.setHref(location.getValue());
        } else if (nodeTags.contains(qName) && checkStackTopType(Node.class)) {
            Node node = (Node) parseStack.pop();
            RepoData repoData = (RepoData) parseStack.peek();
            if (TAG_TIMESTAMP.equals(node.getTag())) {
                repoData.setTimestamp(Long.parseLong(node.getValue()));
            } else if (TAG_SIZE.equals(node.getTag())) {
                repoData.setSize(Long.parseLong(node.getValue()));
            } else if (TAG_OPEN_SIZE.equals(node.getTag())) {
                repoData.setOpenSize(Long.parseLong(node.getValue()));
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String content = new String(ch, start, length);
        if (checkStackTopType(Checksum.class)) {
            Checksum checksum = (Checksum) parseStack.peek();
            checksum.setChecksum(checksum.getChecksum() == null ? content : checksum.getChecksum()+content);
        }else if (checkStackTopType(Node.class)) {
            Node node = (Node) parseStack.peek();
            node.setValue(node.getValue() == null ? content : node.getValue()+content);
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        super.error(e);
    }
}
