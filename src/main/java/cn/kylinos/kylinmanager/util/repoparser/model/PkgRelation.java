package cn.kylinos.kylinmanager.util.repoparser.model;

import java.util.ArrayList;
import java.util.List;

public abstract class PkgRelation {
    List<Entry> entries;

    public PkgRelation() {
        this.entries = new ArrayList<Entry>();
    }

    public PkgRelation(List<Entry> entries) {
        this.entries = entries;
    }

    public void addEntry(Entry entry) {
        this.entries.add(entry);
    }

    public List<Entry> getEntries() {
        return entries;
    }
}
