package cn.kylinos.kylinmanager.util.repoparser;

import cn.kylinos.kylinmanager.util.repoparser.model.ChangeLog;
import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.*;

public class OtherHandler extends RepoHandler {

    public final static String TAG_PACKAGE = "package";
    public final static String TAG_CHANGELOG = "changelog";

    private final static List<String> supportedTag = new ArrayList<>();
    static {
        supportedTag.add(TAG_PACKAGE);
        supportedTag.add(TAG_CHANGELOG);
    }

    private static final Set<String> IGNORE_TAGS = new HashSet<>();
    static {
        IGNORE_TAGS.add("otherdata");
        IGNORE_TAGS.add("version");
    }

    public OtherHandler() {
        super();
    }

    public OtherHandler(Map<String, Package> packageMap) {
        super(packageMap);
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (TAG_PACKAGE.equals(qName)) {
            String pkgid = attributes.getValue("pkgid");
            Package pack;
            if (packageMap.containsKey(pkgid)) {
                pack = packageMap.get(pkgid);
            } else {
                pack = new Package(pkgid);
                pack.setIgnored(true);
            }
            parseStack.push(pack);
        } else if (TAG_CHANGELOG.equals(qName) && checkStackTopType(Package.class)) {
            ChangeLog changelog = new ChangeLog(attributes.getValue("author"), attributes.getValue("date"));
            parseStack.push(changelog);
        } else if (! IGNORE_TAGS.contains(qName))  {
            System.out.println("Parse other.xml, Unsupported tag: "+qName);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (! supportedTag.contains(qName)) {
            return;
        }
        if (TAG_CHANGELOG.equals(qName) && checkStackTopType(ChangeLog.class)) {
            ChangeLog changelog = (ChangeLog) parseStack.pop();
            Package pack = (Package) parseStack.peek();
            pack.addChangeLog(changelog);
        } else if (TAG_PACKAGE.equals(qName) && checkStackTopType(Package.class)) {
            Package pack = (Package) parseStack.pop();
            if (! pack.getIgnored()) {
                if (pack.getPkgid() != null) {
                    this.packageMap.put(pack.getPkgid(), pack);
                } else {
                    this.packageMap.put(pack.getNvrea(), pack);
                }
            }
        } else {
            parseStack.pop();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String content = new String(ch, start, length);
        if (checkStackTopType(ChangeLog.class)) {
            ChangeLog changelog = (ChangeLog) parseStack.peek();
            changelog.setContent(changelog.getContent() == null ? content : changelog.getContent()+content);
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        super.error(e);
    }
}
