package cn.kylinos.kylinmanager.util.repoparser.model;

public class Size {
    private Long pack;

    private Long installed;

    private Long archive;

    public Size(String pack, String installed, String archive) {
        this.pack = Long.parseLong(pack);
        this.installed = Long.parseLong(installed);
        this.archive = Long.parseLong(archive);
    }

    public Long getPack() {
        return pack;
    }

    public Long getInstalled() {
        return installed;
    }

    public Long getArchived() {
        return archive;
    }

    @Override
    public String toString() {
        return "Size{" +
                "pack=" + pack +
                ", installed=" + installed +
                ", archive=" + archive +
                '}';
    }
}
