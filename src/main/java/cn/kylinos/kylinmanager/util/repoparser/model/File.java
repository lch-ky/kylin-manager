package cn.kylinos.kylinmanager.util.repoparser.model;

public class File {
    private FileType type;
    private String path;

    public File() {
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
