package cn.kylinos.kylinmanager.util.repoparser.model;

public enum FileType {
    FILE,
    DIRECTORY
}
