package cn.kylinos.kylinmanager.util.repoparser.model;

import java.util.ArrayList;
import java.util.List;

public class Package {
    private String pkgid;

    private String name;

    private String arch;

    private String epoch;

    private String version;

    private String release;

    private String checksum;

    private String checksumType;

    private String summary;

    private String description;

    private String packager;

    private String url;

    private Long fileTime;

    private Long buildTime;

    private Long packSize;

    private Long installedSize;

    private Long archiveSize;

    private String href;

    private String license;

    private String vendor;

    private String group;

    private String buildHost;

    private String srpm;

    private Long headerRangeStart;

    private Long headerRangeEnd;

    private Boolean ignored;

    private List<Entry> provides;

    private List<Entry> requires;

    private List<Entry> obsoletes;

    private List<Entry> conflicts;

    private List<Entry> recommends;

    private List<Entry> suggests;

    private List<Entry> supplements;

    private List<Entry> enhances;

    private List<File> filelist;

    private List<ChangeLog> changelogs;

    public Package() {
        this.pkgid = null;
        this.filelist = new ArrayList<>();
        this.changelogs = new ArrayList<>();
        this.ignored = Boolean.FALSE;
    }

    public Package(String pkgid) {
        this.pkgid = pkgid;
        this.filelist = new ArrayList<>();
        this.changelogs = new ArrayList<>();
        this.ignored = Boolean.FALSE;
    }

    public void setPkgid(String pkgid) {
        this.pkgid = pkgid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArch(String arch) {
        this.arch = arch;
    }

    public void setEvr(Evr evr) {
        this.epoch = evr.getEpoch();
        this.version = evr.getVersion();
        this.release = evr.getRelease();
    }

    public void setChecksum(Checksum checksum) {
        this.checksum = checksum.getChecksum();
        this.checksumType = checksum.getType();
        if (checksum.getPkgid()) {
            this.pkgid = checksum.getChecksum();
        }
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPackager(String packager) {
        this.packager = packager;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTime(Time time) {
        this.fileTime = time.getFileTime();
        this.buildTime = time.getBuildTime();
    }

    public void setSize(Size size) {
        this.packSize = size.getPack();
        this.installedSize = size.getInstalled();
        this.archiveSize = size.getArchived();
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setBuildHost(String buildHost) {
        this.buildHost = buildHost;
    }

    public void setSrpm(String srpm) {
        this.srpm = srpm;
    }

    public void setHeaderRange(HeaderRange headerRange) {
        this.headerRangeStart = headerRange.getStart();
        this.headerRangeEnd = headerRange.getEnd();
    }

    public void setProvides(List<Entry> provides) {
        this.provides = provides;
    }

    public void setRequires(List<Entry> requires) {
        this.requires = requires;
    }

    public void setObsoletes(List<Entry> obsoletes) {
        this.obsoletes = obsoletes;
    }

    public void setConflicts(List<Entry> conflicts) {
        this.conflicts = conflicts;
    }

    public void setRecommends(List<Entry> recommends) {
        this.recommends = recommends;
    }

    public void setSuggests(List<Entry> suggests) {
        this.suggests = suggests;
    }

    public void setSupplements(List<Entry> supplements) {
        this.supplements = supplements;
    }

    public void setEnhances(List<Entry> enhances) {
        this.enhances = enhances;
    }

    public void setIgnored(Boolean ignored) {
        this.ignored = ignored;
    }

    public void addFile(File file) {
        this.filelist.add(file);
    }

    public void addChangeLog(ChangeLog changelog) {
        this.changelogs.add(changelog);
    }

    public String getPkgid() {
        return pkgid;
    }

    public String getNvrea() {
        return (this.epoch+":"+this.name+"-"+this.version+"-"+this.release+"."+this.arch);
    }

    public String getName() {
        return name;
    }

    public String getArch() {
        return arch;
    }

    public String getEpoch() {
        return epoch;
    }

    public String getVersion() {
        return version;
    }

    public String getRelease() {
        return release;
    }

    public String getChecksum() {
        return checksum;
    }

    public String getChecksumType() {
        return checksumType;
    }

    public String getSummary() {
        return summary;
    }

    public String getDescription() {
        return description;
    }

    public String getPackager() {
        return packager;
    }

    public String getUrl() {
        return url;
    }

    public Long getFileTime() {
        return fileTime;
    }

    public Long getBuildTime() {
        return buildTime;
    }

    public Long getPackSize() {
        return packSize;
    }

    public Long getInstalledSize() {
        return installedSize;
    }

    public Long getArchiveSize() {
        return archiveSize;
    }

    public String getHref() {
        return href;
    }

    public String getLicense() {
        return license;
    }

    public String getVendor() {
        return vendor;
    }

    public String getGroup() {
        return group;
    }

    public String getBuildHost() {
        return buildHost;
    }

    public String getSrpm() {
        return srpm;
    }

    public Long getHeaderRangeStart() {
        return headerRangeStart;
    }

    public Long getHeaderRangeEnd() {
        return headerRangeEnd;
    }

    public List<Entry> getProvides() {
        return provides;
    }

    public List<Entry> getRequires() {
        return requires;
    }

    public List<Entry> getObsoletes() {
        return obsoletes;
    }

    public List<Entry> getConflicts() {
        return conflicts;
    }

    public List<Entry> getRecommends() {
        return recommends;
    }

    public List<Entry> getSuggests() {
        return suggests;
    }

    public List<Entry> getSupplements() {
        return supplements;
    }

    public List<Entry> getEnhances() {
        return enhances;
    }

    public List<File> getFilelist() {
        return filelist;
    }

    public List<ChangeLog> getChangelogs() {
        return changelogs;
    }

    public Boolean getIgnored() {
        return ignored;
    }

    @Override
    public String toString() {
        return "Package(name="+(this.epoch+":"+this.name+"-"+this.version+"-"+this.release+"."+this.arch)+
                ", checksum="+(this.checksumType+":"+this.checksum)+
                ", pkg size="+this.packSize+
                ", filelist_count="+ (this.filelist == null ? "null" : this.filelist.size() ) +
                ", changelog_count="+(this.changelogs == null ? "null" : this.changelogs.size() ) +
                ")";
    }
}
