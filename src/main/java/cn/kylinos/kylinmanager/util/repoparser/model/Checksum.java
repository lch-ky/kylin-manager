package cn.kylinos.kylinmanager.util.repoparser.model;

public class Checksum {
    private String type;

    private String checksum;

    private Boolean pkgid;

    public Checksum(String type, Boolean pkgid) {
        this.type = type;
        this.pkgid = pkgid;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getType() {
        return type;
    }

    public String getChecksum() {
        return checksum;
    }

    public Boolean getPkgid() {
        return pkgid;
    }
}
