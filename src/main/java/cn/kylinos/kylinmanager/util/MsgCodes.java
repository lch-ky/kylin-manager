package cn.kylinos.kylinmanager.util;

public class MsgCodes {
    public final static String RESPONSE_CODE_PREFIX= "response.code.";
    public final static String RESPCODE_FAILED_LOGIN= "1001";
    public final static String RESPCODE_TOO_MANY_LOGIN_ATTEMPTS= "1002";
    public final static String RESPCODE_CREATE_TASK_FAILURE= "1003";

    public final static String RESPCODE_PRODUCT_NOT_FOUND= "4001";
    public final static String RESPCODE_LOCKED_PRODUCT_UPDATED_NOT_ALLOWED= "4002";
    public final static String RESPCODE_LOCKED_PRODUCT_DELETED_NOT_ALLOWED= "4004";
    public final static String RESPCODE_UNSUPPPORT_CONTENT_TYPE= "4011";
    public final static String RESPCODE_REPO_NOT_FOUND= "4012";
    public final static String RESPCODE_REPO_DUPLICATED= "4013";
    public final static String RESPCODE_LOCKED_REPO_UPDATED_NOT_ALLOWED= "4014";
    public final static String RESPCODE_LOCKED_REPO_DELETED_NOT_ALLOWED= "4015";
    public final static String RESPCODE_CREATE_REPO_CONTENT_ERROR= "4016";
    public final static String RESPCODE_REPO_CONTENT_UNREADABLE= "4017";
    public final static String RESPCODE_REPO_CONTENT_NOT_EXIST= "4018";
    public final static String RESPCODE_INVALID_REPO_URL= "4019";
    public final static String RESPCODE_NO_ENOUGH_SPACE= "4020";

    public final static String RESPONSE_TYPE_MISMATCH= "response.type.mismatch";
    public final static String PARAM_MISSING= "param.missing";

    public final static String PATCH_REPO_INVALID_ID_FORMAT = "patch.repo.invalid_id_format";
    public final static String PATCH_REPO_NOT_FOUND = "patch.repo.repo_not_found";
    public final static String PATCH_REPO_PARSE_ERROR = "patch.repo.repo_parse_error";
    public final static String PATCH_REPOMD_PARSE_ERROR = "patch.repo.repomd_parse_error";
    public final static String PATCH_EMPTY_PACKAGE_HREF = "patch.repo.empty_package_href";
    public final static String PATCH_INVALID_PACKAGE_HREF = "patch.repo.invalid_package_href";
    public final static String PATCH_FETCH_PACKAGE_ERROR = "patch.repo.fetch_package_error";

}
