package cn.kylinos.kylinmanager.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Objects;

@Component
@Slf4j
public class RedisUtil {

    private static JedisPool jedisPool;

    // Default expired second = 1day
    public static Integer DEFAULT_EXPIRED_SECONDS = 24 * 60 * 60;

    public static Integer MAX_TRY_TIME = 3;

    @Autowired
    public void setJedisPool(JedisPool jedisPool) {
        RedisUtil.jedisPool = jedisPool;
    }

    public static void set(String key, String value) {
        set(key, value, DEFAULT_EXPIRED_SECONDS);
    }

    public static void set(String key, String value, Integer expiredSeconds) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(key, value);
            jedis.expire(key, expiredSeconds);
        }
    }

    public static String get(String key) {
        return get(key, null);
    }

    public static String get(String key, String defaultValue) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Objects.toString(jedis.get(key), defaultValue);
        }
    }

    public static Long incr(String key) {
        return incr(key, DEFAULT_EXPIRED_SECONDS);
    }

    public static Long incr(String key, Integer expiredSeconds) {
        try (Jedis jedis = jedisPool.getResource()) {
            Long rs = jedis.incr(key);
            jedis.expire(key, expiredSeconds);
            return rs;
        }
    }

    public static Long delete(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.del(key);
        }
    }

}
