package cn.kylinos.kylinmanager.util.repocontent;

import cn.kylinos.kylinmanager.common.KylinException;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.repoparser.RepomdParser;
import cn.kylinos.kylinmanager.util.repoparser.model.Checksum;
import cn.kylinos.kylinmanager.util.repoparser.model.Package;
import cn.kylinos.kylinmanager.util.shell.RunShellExecutor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Slf4j
public class RepoContentUtils {
    public static void createRepoPath(Path repoPath) {
        checkRepoPath(repoPath);
        updateRepoData(repoPath);
    }

    private static Boolean checkExistedRpm(Path repoPath, Package pack) {
        File rpmFile = new File(repoPath.toString(), pack.getNvrea()+".rpm");
        if (rpmFile.canRead()) {
            // Check checksum
            InputStream rpmInput = null;
            try {
                rpmInput = new FileInputStream(rpmFile.getAbsolutePath());
            } catch (FileNotFoundException e) {
                log.warn("RPM file not found. However, this is not expected.");
                return false;
            }
            Checksum checksum = new Checksum(pack.getChecksumType(), false);
            checksum.setChecksum(pack.getChecksum());
            try {
                if (RepomdParser.fileChecksum(rpmInput, checksum)) {
                    return true;
                }
            } catch (NoSuchAlgorithmException | IOException e) {
                return false;
            }
        }
        return false;
    }

    public static void syncRepoRpm(Path repoPath, Package pack) {
        if (checkExistedRpm(repoPath, pack)) {
            // Ignore download progress
            log.info("Existed rpm {}/{} has the same checksum with remote", repoPath, pack.getNvrea());
            return;
        }

        if (StringUtils.isBlank(pack.getHref()) ) {
            log.warn("SyncRepoRpm failed, href of package {} is empty", pack.getNvrea());
            throw new KylinException(MsgCodes.PATCH_EMPTY_PACKAGE_HREF);
        }
        URL repomdUrl = null;
        try {
            repomdUrl = new URL(pack.getHref());
        } catch (MalformedURLException e) {
            throw new KylinException(MsgCodes.PATCH_INVALID_PACKAGE_HREF);
        }
        FileOutputStream fileWriter = null;
        InputStream input = null;
        try {
            URLConnection conn = repomdUrl.openConnection();
            //开启客户端与Url所指向的资源的网络连接
            conn.connect();
            Path packagePath = Path.of(repoPath.toString(), pack.getNvrea()+".rpm");
            fileWriter = new FileOutputStream(packagePath.toString());
            input = conn.getInputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while ((n = input.read(buf)) >= 0) {
                fileWriter.write(buf, 0, n);
            }
            log.info("Fetch package content done: pkg {} -> dir {}", pack.getNvrea(), packagePath);
        } catch (IOException e) {
            log.error("Failed to fetch package content: " + e.getMessage(),e);
            throw new KylinException(MsgCodes.PATCH_FETCH_PACKAGE_ERROR);
        } finally {
            Optional.ofNullable(input).ifPresent(inputStream -> {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.warn("Rpm download inputstream cannot close properly", e);
                }
            });
            Optional.ofNullable(fileWriter).ifPresent(outputStream -> {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    log.warn("Rpm download outputstream cannot close properly", e);
                }
            });
        }

    }

    public static void checkRepoPath(Path repoPath) {
        checkRepoPath(repoPath, Boolean.TRUE);
    }

    public static void checkRepoPath(Path repoPath, Boolean createFlag) {
        if (! Files.exists(repoPath)) {
            if (createFlag) {
                try {
                    // Create repoPath and its nonexist parent directory
                    Files.createDirectories(repoPath);
                } catch (IOException e) {
                    log.error("Create repoPath error: {}", e.getMessage(), e);
                    throw new KylinException(MsgCodes.RESPCODE_CREATE_REPO_CONTENT_ERROR, e.getMessage());
                }
            } else {
                throw new KylinException(MsgCodes.RESPCODE_REPO_CONTENT_NOT_EXIST);
            }
        }
        if (! Files.isReadable(repoPath)) {
            throw new KylinException(MsgCodes.RESPCODE_REPO_CONTENT_UNREADABLE);
        }
    }

    public static String updateRepoData(Path repoPath) {
        log.info("Createrepo path: {}", repoPath.toAbsolutePath().toString());
        String result = RunShellExecutor.runTask( String.format("createrepo --update %s", repoPath.toAbsolutePath().toString()) );
        log.info("Createrepo result: {}", result);
        return result;
    }
}
