package cn.kylinos.kylinmanager.util;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class UncompressFileTAR {
    public static final String EXT = ".tar.gz";

    public  HashMap<String, String> decompress(String pathname, String uncompressPath) {
        HashMap<String, String> results = new HashMap<String, String>();
        //check parameters
        File filed = new File(pathname);
        System.out.println("解压前的文件名称："+pathname);
        //String destDir = "";
        if(!filed.exists() || !filed.getName().endsWith(EXT)) {
            results.put("code", "4004");
            results.put("result", "not found compress file or not is a '.tar.gz' file.");
        }

        //开始解压
        String fileName =null;
        FileInputStream fis ;
        ArchiveInputStream in = null;
        BufferedInputStream bufferedInputStream = null ;
        BufferedOutputStream bufferedOutputStream = null ;
        List<String> pathList = new ArrayList<>();
        try {
            fis = new FileInputStream(filed);
            GZIPInputStream is = new GZIPInputStream(new BufferedInputStream(fis));
            in = new ArchiveStreamFactory().createArchiveInputStream("tar", is);

//          设置解压路径
//            String decompressPath = utilConfig.getDecompressPath();
//            String outFileName = decompressPath;
            mkFolder(uncompressPath);
            String outFileName = uncompressPath;

            bufferedInputStream = new BufferedInputStream(in);
            TarArchiveEntry entry = (TarArchiveEntry)in.getNextEntry();

            while(entry != null) {
                String name = entry.getName();
                String[] names = name.split("/");
                //System.out.println(names);
                fileName = outFileName;
                //System.out.println(fileName);
                for(int i = 0; i < names.length; i++) {
                    String str = names[i];
                    fileName = fileName + File.separator + str;

                }
                if(name.endsWith("/")) {
                    mkFolder(fileName);
                } else {
                    File file = mkFile(fileName);
                    pathList.add(fileName);
                    System.out.println(fileName);
                    bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                    int b = 0;
                    while((b = bufferedInputStream.read()) != -1) {
                        bufferedOutputStream.write(b);
                    }
                    bufferedOutputStream.flush();
                    bufferedOutputStream.close();
                }
                entry = (TarArchiveEntry)in.getNextEntry();
            }

            System.out.println("解压后的文件路径："+ fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArchiveException e) {
            e.printStackTrace();
        } finally {
            if(bufferedInputStream != null) {
                try {
                    bufferedInputStream.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        String decompressPath = pathList.get(0) ;
        results.put("code", "200");

        results.put("result", decompressPath );
        return results;
    }

    //创建文件夹
    private static void mkFolder(String fileName) {
        File f = new File(fileName);
        if(!f.exists()) {
            f.setWritable(true, false);
            f.mkdirs();
        }
    }

    private static File mkFile(String fileName) {
        File f = new File(fileName);
        try {
            f.getParentFile().mkdirs();
            f.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }

    //    获取文件名
    public static String getFileName(String f) {
        String fname = "";
        int i = f.lastIndexOf('.');

        if (i > 0 &&  i < f.length() - 1) {
            fname = f.substring(0,i-4);
        }
        return fname;
    }

}