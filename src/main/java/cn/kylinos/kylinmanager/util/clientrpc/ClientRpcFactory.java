package cn.kylinos.kylinmanager.util.clientrpc;

import cn.kylinos.kylinmanager.host.model.KylinHost;
import cn.kylinos.kylinmanager.host.service.HostService;
import cn.kylinos.kylinmanager.configitem.service.ConfigService;
import cn.kylinos.kylinmanager.configitem.service.StandardLibService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class ClientRpcFactory {

    @Autowired
    private HostService hostService;

    @Autowired
    private StandardLibService standardLibService;

    @Autowired
    private ConfigService configService;

    private final ConcurrentHashMap<String, ClientInstance> clientMap = new ConcurrentHashMap<>();

    public ClientInstance getClientInstance(String ip)
    {
        ClientInstance clientInstance = clientMap.getOrDefault(ip, null);
        if(clientInstance != null)
        {
            return clientInstance;
        }
        KylinHost kylinHost = hostService.findHostByIp(ip);
        Integer standardId = standardLibService.findIdByHostInfo(kylinHost.getOperatingSystem(),
                kylinHost.getVersion(), kylinHost.getArchitecture());
        clientInstance = new ClientInstance();
        clientInstance.setInfo(ip, kylinHost.getAgentToken(), kylinHost.getAgentCa());
        clientInstance.setConfigService(configService);
        clientInstance.setStandardId(standardId);
        clientMap.put(ip, clientInstance);
        return clientInstance;
    }
}
