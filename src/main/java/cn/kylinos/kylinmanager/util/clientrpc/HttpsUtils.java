package cn.kylinos.kylinmanager.util.clientrpc;

import com.alibaba.fastjson.JSONObject;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class HttpsUtils {

    @Setter
    private String crt;

    @Setter
    private String token;

    @Setter
    private String address;

    private SSLContext getSslContext() throws Exception
    {
        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        InputStream is = IOUtils.toInputStream(crt, StandardCharsets.UTF_8.name());
        X509Certificate certificate = (X509Certificate) fact.generateCertificate(is);
        KeyStore ks = KeyStore.getInstance("pkcs12");
        ks.load(null, null);
        ks.setCertificateEntry("ca-certificate", certificate);
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(ks);
        SSLContext context = SSLContext.getInstance("TLSv1.2");
        context.init(null, tmf.getTrustManagers(), null);
        return context;
    }

    public ClientRpcResult get(String relativeUrl, HashMap<String, String> datas) throws Exception
    {
        if (datas == null)
        {
            datas = new HashMap<>(0);
        }
        String url = MessageFormat.format("https://{0}{1}", address, relativeUrl);
        URIBuilder uriBuilder = new URIBuilder(url);

        for(Map.Entry<String, String> data : datas.entrySet())
        {
            uriBuilder.setParameter(data.getKey(), data.getValue());
        }

        HttpGet httpMethod = new HttpGet(uriBuilder.build());
        httpMethod.setHeader("token", token);

        HttpClientBuilder builder = HttpClients.custom();
        builder.setSSLContext(getSslContext());

        CloseableHttpClient httpclient = builder.build();
        CloseableHttpResponse response = httpclient.execute(httpMethod);
        HttpEntity entity = response.getEntity();
        String body = EntityUtils.toString(entity);
        response.close();
        httpclient.close();

        return JSONObject.toJavaObject(JSONObject.parseObject(body), ClientRpcResult.class);
    }

    public ClientRpcResult post(String relativeUrl, HashMap<String, String> datas) throws Exception
    {
        if (datas == null)
        {
            datas = new HashMap<>(0);
        }
        String url = MessageFormat.format("https://{0}{1}", address, relativeUrl);
        URIBuilder uriBuilder = new URIBuilder(url);

        for(Map.Entry<String, String> data : datas.entrySet())
        {
            uriBuilder.setParameter(data.getKey(), data.getValue());
        }

        HttpPost httpMethod = new HttpPost(uriBuilder.build());
        httpMethod.setHeader("token", token);

        HttpClientBuilder builder = HttpClients.custom();
        builder.setSSLContext(getSslContext());

        CloseableHttpClient httpclient = builder.build();
        CloseableHttpResponse response = httpclient.execute(httpMethod);
        HttpEntity entity = response.getEntity();
        String body = EntityUtils.toString(entity);
        response.close();
        httpclient.close();

        return JSONObject.toJavaObject(JSONObject.parseObject(body), ClientRpcResult.class);
    }

    public ClientRpcResult postJson(String relativeUrl, String json) throws Exception
    {
        String url = MessageFormat.format("https://{0}{1}", address, relativeUrl);
        URIBuilder uriBuilder = new URIBuilder(url);

        StringEntity stringEntity = new StringEntity(json, "utf-8");
        stringEntity.setContentEncoding("UTF-8");
        stringEntity.setContentType("application/json");
        HttpPost httpMethod = new HttpPost(uriBuilder.build());
        httpMethod.setHeader("token", token);
        httpMethod.setEntity(stringEntity);

        HttpClientBuilder builder = HttpClients.custom();
        builder.setSSLContext(getSslContext());

        CloseableHttpClient httpclient = builder.build();
        CloseableHttpResponse response = httpclient.execute(httpMethod);
        HttpEntity entity = response.getEntity();
        String body = EntityUtils.toString(entity);
        response.close();
        httpclient.close();

        return JSONObject.toJavaObject(JSONObject.parseObject(body), ClientRpcResult.class);
    }

    public ClientRpcResult put(String relativeUrl, HashMap<String, String> datas) throws Exception
    {
        if (datas == null)
        {
            datas = new HashMap<>(0);
        }
        String url = MessageFormat.format("https://{0}{1}", address, relativeUrl);
        URIBuilder uriBuilder = new URIBuilder(url);

        for(Map.Entry<String, String> data : datas.entrySet())
        {
            uriBuilder.setParameter(data.getKey(), data.getValue());
        }

        HttpPut httpMethod = new HttpPut(uriBuilder.build());
        httpMethod.setHeader("token", token);

        HttpClientBuilder builder = HttpClients.custom();
        builder.setSSLContext(getSslContext());

        CloseableHttpClient httpclient = builder.build();
        CloseableHttpResponse response = httpclient.execute(httpMethod);
        HttpEntity entity = response.getEntity();
        String body = EntityUtils.toString(entity);
        response.close();
        httpclient.close();

        return JSONObject.toJavaObject(JSONObject.parseObject(body), ClientRpcResult.class);
    }

    public ClientRpcResult delete(String relativeUrl, HashMap<String, String> datas) throws Exception
    {
        if (datas == null)
        {
            datas = new HashMap<>(0);
        }
        String url = MessageFormat.format("https://{0}{1}", address, relativeUrl);
        URIBuilder uriBuilder = new URIBuilder(url);

        for(Map.Entry<String, String> data : datas.entrySet())
        {
            uriBuilder.setParameter(data.getKey(), data.getValue());
        }

        HttpDelete httpMethod = new HttpDelete(uriBuilder.build());
        httpMethod.setHeader("token", token);

        HttpClientBuilder builder = HttpClients.custom();
        builder.setSSLContext(getSslContext());

        CloseableHttpClient httpclient = builder.build();
        CloseableHttpResponse response = httpclient.execute(httpMethod);
        HttpEntity entity = response.getEntity();
        String body = EntityUtils.toString(entity);
        response.close();
        httpclient.close();

        return JSONObject.toJavaObject(JSONObject.parseObject(body), ClientRpcResult.class);
    }
}
