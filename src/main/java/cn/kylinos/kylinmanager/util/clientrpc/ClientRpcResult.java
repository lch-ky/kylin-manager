package cn.kylinos.kylinmanager.util.clientrpc;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

@Data
public class ClientRpcResult {
    private int code;
    private JSONObject data;
    private String content;
}
