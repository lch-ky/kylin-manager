package cn.kylinos.kylinmanager.util.clientrpc;

public enum ConfigEnum {
    //应用配置
    APP("app"),
    //内核参数
    KERNEL("kernel"),
    //安全配置
    SECURITY("security");

    private final String configType;

    private ConfigEnum(String configType) {
        this.configType = configType;
    }

    public String getConfigType()
    {
        return configType;
    }
}
