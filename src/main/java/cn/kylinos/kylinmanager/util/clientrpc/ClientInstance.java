package cn.kylinos.kylinmanager.util.clientrpc;

import cn.kylinos.kylinmanager.common.config.ConfigStruct;
import cn.kylinos.kylinmanager.common.config.ConfigStructItem;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.configitem.service.ConfigService;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientInstance {

    private final HttpsUtils httpsUtils;

    private Integer clientStandardId;

    private ConfigService clientConfigService = null;

    public ClientInstance() {
        httpsUtils = new HttpsUtils();
    }


    public void setInfo(String clientIp, String clientToken, String clientCa) {
        httpsUtils.setAddress(clientIp + ":13000");
        httpsUtils.setToken(clientToken);
        httpsUtils.setCrt(clientCa);
    }

    public void setConfigService(ConfigService configService)
    {
        if (clientConfigService == null)
        {
            clientConfigService = configService;
        }
    }

    public void setStandardId(Integer standardId)
    {
        clientStandardId = standardId;
    }

    public ClientRpcResult getVersion() throws Exception {
        return httpsUtils.get("/info/version", null);
    }

    public ClientRpcResult getHeartBeat() throws Exception {
        return httpsUtils.get("/info/ping", null);
    }

    public ClientRpcResult remind() throws Exception {
        return httpsUtils.post("/info/remind", null);
    }

    public ClientRpcResult execScript(String script, List<String> blacklist, Integer timeout) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("script", script);
        jsonObject.put("blacklist", blacklist);
        jsonObject.put("timeout", timeout);
        return httpsUtils.postJson("/script", jsonObject.toJSONString());
    }

    public ClientRpcResult uninstall() throws Exception {
        ClientRpcResult clientRpcResult =  httpsUtils.delete("/agent/uninstall", null);
        if(clientRpcResult.getCode() != 0)
        {
            throw new Exception(clientRpcResult.getContent());
        }
        return clientRpcResult;
    }

    public ClientRpcResult update(String updateFile, Integer forceSwitch) throws Exception {
        HashMap<String, String> hashMap = new HashMap<>(2);
        hashMap.put("update_file", updateFile);
        hashMap.put("force", forceSwitch.toString());
        ClientRpcResult clientRpcResult = httpsUtils.put("/agent/update", hashMap);
        if(clientRpcResult.getCode() != 0)
        {
            throw new Exception(clientRpcResult.getContent());
        }
        return clientRpcResult;
    }

    public ConfigStruct getAllConfig() throws Exception {
        ClientRpcResult clientRpcResult = httpsUtils.get("/config/all", null);
        if (clientRpcResult.getCode() != 0)
        {
            throw new Exception(clientRpcResult.getContent());
        }

        ConfigStruct configStruct = new ConfigStruct();

        JSONObject configData = clientRpcResult.getData();
        JSONObject kernelData = configData.getJSONObject("kernel");
        Map<String, KylinConfig> kernels = clientConfigService.findByNameConfigTypeList(clientStandardId,
                "内核参数", new ArrayList<>(kernelData.keySet()));

        ConfigStructItem kernelItem = createConfigItem(kernels, kernelData);
        configStruct.addConfigStructItem("kernel", "", kernelItem);

        JSONObject appData = configData.getJSONObject("app");
        for (Map.Entry<String, Object> app: appData.entrySet())
        {
            JSONObject jsonObject = (JSONObject) app.getValue();
            Map<String, KylinConfig> curApp = clientConfigService.findByNameAttributeList(clientStandardId,
                    app.getKey(), new ArrayList<>(jsonObject.keySet()));

            ConfigStructItem appItem = createConfigItem(curApp, jsonObject);
            configStruct.addConfigStructItem("app", app.getKey(), appItem);
        }

        JSONObject securityData = configData.getJSONObject("security");
        for (Map.Entry<String, Object> security: securityData.entrySet())
        {
            JSONObject jsonObject = (JSONObject) security.getValue();
            Map<String, KylinConfig> curSecurity = clientConfigService.findByNameAttributeList(clientStandardId,
                    security.getKey(), new ArrayList<>(jsonObject.keySet()));
            ConfigStructItem securityItem = createConfigItem(curSecurity, jsonObject);
            configStruct.addConfigStructItem("security", security.getKey(), securityItem);
        }
        return configStruct;
    }

    public void writeAll(JSONObject configJson) throws Exception{
        ClientRpcResult clientRpcResult = httpsUtils.postJson("/config/all", configJson.toJSONString());;
        if (clientRpcResult.getCode() != 0)
        {
            throw new Exception(clientRpcResult.getContent());
        }
    }

    private ConfigStructItem createConfigItem(Map<String, KylinConfig> curMap, JSONObject curData)
    {
        ConfigStructItem curItem = new ConfigStructItem();
        for(Map.Entry<String, KylinConfig> item: curMap.entrySet())
        {
            String configName = item.getKey();
            KylinConfig kylinConfig = item.getValue();
            kylinConfig.setBaseline(curData.get(configName).toString());
            curItem.addConfig(configName, kylinConfig);
            curItem.setFilePath(kylinConfig.getFilePath());
            curItem.setConfigType(kylinConfig.getConfigType());
            curItem.setAttribute(kylinConfig.getAttribute());
        }
        return curItem;
    }
}
