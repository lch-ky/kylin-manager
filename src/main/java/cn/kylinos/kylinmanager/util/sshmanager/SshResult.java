package cn.kylinos.kylinmanager.util.sshmanager;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SshResult {

    private String message = "";

    private int exitStatus = 0;
}
