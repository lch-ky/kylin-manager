package cn.kylinos.kylinmanager.util.sshmanager;

import com.jcraft.jsch.*;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class SshManager {

    private Session session;

    public void initSession(String user, String host, String password) throws Exception {
        JSch jsch=new JSch();
        session = jsch.getSession(user,host,22);
        session.setPassword(password);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect(20000);
    }

    public SshResult getCommandResult(String command) throws Exception
    {
        ChannelExec channel= (ChannelExec) session.openChannel("exec");
        String result = "";
        SshResult sshResult = new SshResult();
        try
        {
            InputStream inputStream = channel.getInputStream();
            channel.setCommand(command);
            channel.connect(19000);
            result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            int returnCode = channel.getExitStatus();
            sshResult.setMessage(result);
            sshResult.setExitStatus(returnCode);
        }
        catch (Exception e)
        {
            sshResult.setMessage(e.getMessage());
            sshResult.setExitStatus(-1);
        }
        finally
        {
            channel.disconnect();
        }

        return sshResult;
    }

    public void sendFile(String localPath, String remoteRoot) throws JSchException, SftpException, IOException
    {
        ChannelSftp channel= (ChannelSftp) session.openChannel("sftp");
        try
        {
            channel.connect(19000);
            channel.setFilenameEncoding("UTF-8");
            File file = new File(localPath);
            String remoteFile = remoteRoot + "/" + file.getName();
            long fileLength = file.length();
            OutputStream out = channel.put(remoteFile, ChannelSftp.OVERWRITE);
            byte[] buff = new byte[(int) fileLength];
            int read;
            if (out != null)
            {
                System.out.println("Start to read input stream");
                InputStream is = new FileInputStream(file);
                do
                {
                    read = is.read(buff, 0, buff.length);
                    if (read > 0)
                    {
                        out.write(buff, 0, read);
                    }
                    out.flush();
                } while (read >= 0);
                System.out.println("input stream read done.");
            }
        }
        finally
        {
            channel.quit();
            channel.disconnect();
        }
    }

    public String getClientIp() throws JSchException, SftpException, IOException
    {
        return session.getHost();
    }

    public void disConnect()
    {
        session.disconnect();
    }
}
