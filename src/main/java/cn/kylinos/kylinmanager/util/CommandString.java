package cn.kylinos.kylinmanager.util;

public class CommandString {
    public static String INSTALLTIME = "last -RF reboot | awk '{printf $8 \" \" $5 \" \" $6 \" \" $7 \"\\n\"}' | tail -n 3 | head -n 1|date  +\"%Y-%m-%d %H:%M:%S\"";
    public static String OSSTREAM = "cat /etc/.productinfo";
    public static String INSTALLENV = "grep -rn \"Environment selected for installation\" /var/log/anaconda/anaconda.log |tail -1";
    public static String INSTALLGROUP = "grep -rn \"Groups selected for installation\" /var/log/anaconda/anaconda.log |tail -1";
    public static String ARCH = "arch";
    public static String DMIDECODE = "dmidecode |grep UUID";
    public static String AGENTCA = "cat /opt/kylin-manager/etc/pki/kylin-agent/server/server.crt";
    public static String AGENTTOKEN = "cat /opt/kylin-manager/etc/pki/kylin-agent/server/.token";
}
