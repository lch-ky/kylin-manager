package cn.kylinos.kylinmanager.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class LocaleInterceptor implements HandlerInterceptor {
    @Autowired
    private LocaleResolver localeResolver;

    private static final List<Locale> LOCALES = Arrays.asList(new Locale("en"),
            new Locale("zh", "CN"), new Locale("zh", "TW"));

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Locale locale = localeResolver.resolveLocale(request);
        // Set default locale if Accept-Language header not parsed
        if (locale.getDisplayName().isEmpty()) {
            LocaleContextHolder.setLocale(Locale.SIMPLIFIED_CHINESE);
        }
        List<Locale.LanguageRange> list = Locale.LanguageRange.parse(locale.toString().replace('_', '-'));
        locale = Locale.lookup(list, LOCALES);
        // Set default locale if locale is not in available list
        if (!LOCALES.contains(locale)) {
            LocaleContextHolder.setLocale(Locale.SIMPLIFIED_CHINESE);
        }
        return true;
    }
}
