package cn.kylinos.kylinmanager.common;

import lombok.Data;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
public class SearchResult<T> {
    private Long total;

    private Boolean paged;

    private Integer page;

    private Integer perPage;

    private Integer totalPages;

    private String search;

    private List<T> results;

    public <R> SearchResult<R> mapResults(Function<T, R> converter) {
        SearchResult<R> mappedSearchResult = new SearchResult<>();
        this.copyMetadata(mappedSearchResult);
        mappedSearchResult.setResults(this.results.stream().map(converter).collect(Collectors.toList()));
        return mappedSearchResult;
    }

    public void copyMetadata(SearchResult<?> to) {
        to.setTotal(this.total);
        to.setPaged(this.paged);
        to.setPage(this.page);
        to.setPerPage(this.perPage);
        to.setTotalPages(this.totalPages);
        to.setSearch(this.search);
    }
}
