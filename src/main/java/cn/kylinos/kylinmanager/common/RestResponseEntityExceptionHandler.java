package cn.kylinos.kylinmanager.common;

import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.MsgUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Exception handler
 * Capture exception thrown from controller
 */

@ControllerAdvice
@Slf4j
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, HttpServletRequest request, WebRequest webRequest){
        RestResponse response = RestResponse.createErrorResponse(
                "409");
        log.warn("handleConflict() called. Response={}, URL={}", JSON.toJSONString(response), request.getRequestURI(), ex);
        return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.CONFLICT, webRequest);
    }

    /**
     * When parameters transferred to controller cannot be parsed properly
     * @param ex
     * @param request
     * @param webRequest
     * @return
     */
    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      HttpServletRequest request, WebRequest webRequest){
        RestResponse response = RestResponse.createResponse(
                "400", MsgUtil.get(MsgCodes.RESPONSE_TYPE_MISMATCH, ex.getName(), Objects.requireNonNull(ex.getRequiredType()).getSimpleName()),
                null);
        log.warn("handleMethodArgumentTypeMismatch() called. Response={}, URL={}", JSON.toJSONString(response), request.getRequestURI());
        return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.BAD_REQUEST, webRequest);
    }

    /**
     * Handle max upload size exceeded exception
     * @param ex
     * @param request
     * @param webRequest
     * @return
     */
    @ExceptionHandler(value = {MaxUploadSizeExceededException.class})
    protected ResponseEntity<Object> handleUploadSizeExceededException(RuntimeException ex, HttpServletRequest request, WebRequest webRequest){
        RestResponse response = RestResponse.createErrorResponse("413");
        log.warn("handleUploadSizeExceededException() called. Response={}, URL={}", JSON.toJSONString(response), request.getRequestURI(), ex);
        return handleExceptionInternal(ex,response, new HttpHeaders(), HttpStatus.PAYLOAD_TOO_LARGE, webRequest);
    }

    /**
     * Handle Shiro authorization exception
     * @param ex
     * @param request
     * @param webRequest
     * @return
     */
    @ExceptionHandler(value = {AuthorizationException.class, IncorrectCredentialsException.class})
    protected ResponseEntity<Object> handleAuthorizationExceptionException(RuntimeException ex, HttpServletRequest request, WebRequest webRequest){
        RestResponse response = RestResponse.createErrorResponse(
                "401");
        log.warn("handleAuthorizationExceptionException() called. Response={}, URL={}", JSON.toJSONString(response), request.getRequestURI(), ex);
        return handleExceptionInternal(ex,response, new HttpHeaders(), HttpStatus.UNAUTHORIZED, webRequest);
    }

    /**
     * Handle Kylin exception
     * @param ex
     * @param request
     * @param webRequest
     * @return
     */
    @ExceptionHandler(value = {KylinException.class})
    protected ResponseEntity<Object> handleKylinExceptionException(KylinException ex, HttpServletRequest request, WebRequest webRequest){
        RestResponse response = RestResponse.createErrorResponse(
                ex.getErrorCode(), ex.getMessage());
        log.warn("handleKylinExceptionException() called. Response={}, URL={}", JSON.toJSONString(response), request.getRequestURI(), ex);
        return handleExceptionInternal(ex,response, new HttpHeaders(), HttpStatus.OK, webRequest);
    }

    /**
     * Last exception handler, handle all uncatched exception
     * @param ex
     * @param request
     * @param webRequest
     * @return
     */
    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleAllException(RuntimeException ex, HttpServletRequest request, WebRequest webRequest){
        RestResponse response = RestResponse.createErrorResponse("500");
        log.warn("handleAllException() called. Response={}, URL={}", JSON.toJSONString(response), request.getRequestURI(), ex);
        return handleExceptionInternal(ex,response, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, webRequest);
    }

    /**
     * Custom http 405 handler
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
                                                                         HttpHeaders headers, HttpStatus status, WebRequest request) {
        StringBuilder supportedHttpMethods = new StringBuilder("Http method should be one of ");
        if (ex != null && ex.getSupportedMethods() != null) {
            for (String m : ex.getSupportedMethods()) {
                supportedHttpMethods.append(m).append(", ");
            }
            supportedHttpMethods.delete(supportedHttpMethods.length() - 2, supportedHttpMethods.length());
        } else {
            supportedHttpMethods = new StringBuilder(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());
        }
        RestResponse response = RestResponse.createResponse("405",
                MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+"405"), supportedHttpMethods.toString());
        log.warn("handleAllException() called. Response="+ JSON.toJSONString(response), ex);
        return handleExceptionInternal(ex,response, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        List<String> errors = new ArrayList<String>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        RestResponse response = RestResponse.createResponse(
                "400", MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+"400"),
                JSON.toJSONString(errors));

        logger.warn("handleAllException() called. Response="+ JSON.toJSONString(response), ex);
        return handleExceptionInternal(
                ex, response, headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Handle parameter missing in controller methods
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        RestResponse response = RestResponse.createResponse(
                "400",
                MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+"400")+":"+
                MsgUtil.get(MsgCodes.PARAM_MISSING, ex.getParameterName()),
                null);

        log.warn("handleMissingServletRequestParameter() called. Response="+ JSON.toJSONString(response));
        return handleExceptionInternal(
                ex, response, headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Handler Http 404
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "No found for " + ex.getHttpMethod() + "-" + ex.getRequestURL();
        RestResponse response = RestResponse.createErrorResponse("404", ex.getHttpMethod(), ex.getRequestURL());
        log.warn("handleNoHandlerFoundException() called. Response="+ JSON.toJSONString(response));
        return handleExceptionInternal(
                ex, response, headers, HttpStatus.NOT_FOUND, request);
    }
}