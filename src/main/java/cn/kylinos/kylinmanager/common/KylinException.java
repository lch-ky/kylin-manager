package cn.kylinos.kylinmanager.common;

import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.MsgUtil;

public class KylinException extends RuntimeException {
    private String errorCode;

    public KylinException() {
        super(MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+"500"));
        errorCode = "500";
    }

    public KylinException(String errorCode) {
        super(MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+errorCode));
        this.errorCode = errorCode;
    }

    public KylinException(String errorCode, Object... args) {
        super(MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+errorCode, args));
        this.errorCode = errorCode;
    }

    public KylinException(Boolean customMessage, String errorCode, String message) {
        super(customMessage ? message : MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+errorCode, message));
        this.errorCode = errorCode;
    }

    public KylinException(Boolean customMessage, String errorCode, String message, Throwable cause) {
        super(customMessage ? message : MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+errorCode, message), cause);
        this.errorCode = errorCode;
    }

    public KylinException(String errorCode, Throwable cause) {
        super(MsgUtil.get(MsgCodes.RESPONSE_CODE_PREFIX+errorCode), cause);
        this.errorCode = errorCode;
    }

    protected KylinException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        errorCode = "500";
    }

    public String getErrorCode() {
        return errorCode;
    }
}
