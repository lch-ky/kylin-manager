package cn.kylinos.kylinmanager.common.compare;

import lombok.Data;

import java.util.ArrayList;

@Data
public class CompareResultItem implements Comparable<CompareResultItem> {

    private String name;

    private String left;

    private String right;

    public CompareResultItem(String strName, String strLeft, String strRight)
    {
        name = strName;
        left = strLeft;
        right = strRight;
    }

    public ArrayList<String> toList()
    {
        ArrayList<String> list = new ArrayList<>();
        list.add(name);
        list.add(left);
        list.add(right);
        return list;
    }



    @Override
    public int compareTo(CompareResultItem o) {
        return this.getName().compareTo(o.getName());
    }
}
