package cn.kylinos.kylinmanager.common.compare;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;

@Data
public class CompareResult {

    private String filePath = "";

    private String configType = "";

    private String attribute = "";

    private ArrayList<CompareResultItem> data = new ArrayList<>();

    public void addItem(String name, String left, String right)
    {
        data.add(new CompareResultItem(name, left, right));
    }

    public CompareResult sort()
    {
        Collections.sort(data);
        return this;
    }
}
