package cn.kylinos.kylinmanager.common;

import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.MsgUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Locale;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestResponse {
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ", timezone = "Asia/Shanghai")
    private OffsetDateTime timestamp;

    private String code;

    private String message;

    private Object data;

    public static RestResponse createResponse(String code, String message, Object data) {
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode(code);
        response.setMessage(message);
        response.setData(data);
        return response;
    }

    public static RestResponse createSuccessResponse(Object data) {
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode("0");
        response.setMessage(MsgUtil.get(LocaleContextHolder.getLocale(), MsgCodes.RESPONSE_CODE_PREFIX+"0"));
        response.setData(data);
        return response;
    }

    public static RestResponse createSuccessResponse(Object data, Locale locale) {
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode("0");
        response.setMessage(MsgUtil.get(locale, MsgCodes.RESPONSE_CODE_PREFIX+"0"));
        response.setData(data);
        return response;
    }

    // Response code mapped message should be like "response.code.CODE_VALUE"
    public static RestResponse createErrorResponse(String code) {
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode(code);
        response.setMessage(MsgUtil.get(LocaleContextHolder.getLocale(), MsgCodes.RESPONSE_CODE_PREFIX+code));
        response.setData(null);
        return response;
    }

    public static RestResponse createErrorResponse(String code, Locale locale) {
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode(code);
        response.setMessage(MsgUtil.get(locale, MsgCodes.RESPONSE_CODE_PREFIX+code));
        response.setData(null);
        return response;
    }

    public static RestResponse createErrorResponse(String code, Object... args) {
        if (args.length > 1 && args[1] instanceof Locale) {
            if (args.length > 2) {
                Object[] newargs = Arrays.copyOfRange(args, 1, args.length);
                createErrorResponse(code, (Locale) args[1], newargs);
            } else {
                createErrorResponse(code, (Locale) args[1]);
            }
        }
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode(code);
        response.setMessage(MsgUtil.get(LocaleContextHolder.getLocale(), MsgCodes.RESPONSE_CODE_PREFIX+code, args));
        response.setData(null);
        return response;
    }

    public static RestResponse createErrorResponse(String code, Locale locale, Object... args) {
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode(code);
        response.setMessage(MsgUtil.get(locale, MsgCodes.RESPONSE_CODE_PREFIX+code, args));
        response.setData(null);
        return response;
    }

    public static RestResponse createErrorResponse(String code, String errorMsg) {
        RestResponse response = new RestResponse();
        response.setTimestamp(OffsetDateTime.now());
        response.setCode(code);
        response.setMessage(errorMsg);
        response.setData(null);
        return response;
    }

}
