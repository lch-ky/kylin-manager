package cn.kylinos.kylinmanager.common;

import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/error")
public class BasicErrorController extends AbstractErrorController {


    public BasicErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping
    public ResponseEntity<RestResponse> error(HttpServletRequest request) {
        HttpStatus status = getStatus(request);
        RestResponse response = RestResponse.createErrorResponse("500");
        if (status != null) {
            if (HttpStatus.NOT_FOUND.equals(status)) {
                response = RestResponse.
                        createErrorResponse("404", request.getMethod(), request.getRequestURI());
            } else if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
                response = RestResponse.createErrorResponse("500");

            } else if (HttpStatus.BAD_REQUEST.equals(status)) {
                response = RestResponse.createErrorResponse("400");

            }
        }

        return new ResponseEntity<>(response, status == null ? HttpStatus.INTERNAL_SERVER_ERROR : status);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}