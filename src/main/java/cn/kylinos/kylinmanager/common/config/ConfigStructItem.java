package cn.kylinos.kylinmanager.common.config;

import cn.kylinos.kylinmanager.common.compare.CompareResult;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

public class ConfigStructItem {

    @Getter
    private final HashMap<String, KylinConfig> config = new HashMap<>();

    @Getter
    @Setter
    private String filePath;

    @Getter
    @Setter
    private String configType;

    @Getter
    @Setter
    private String attribute;


    public void addConfig(String name, KylinConfig kylinConfig)
    {
            config.put(name, kylinConfig);
    }


    public JSONObject toJson()
    {
        JSONObject jsonObject = new JSONObject();
        for(Map.Entry<String, KylinConfig> item: config.entrySet())
        {
            jsonObject.put(item.getKey(), item.getValue());
        }
        return jsonObject;
    }

    public CompareResult compare(ConfigStructItem target, boolean isComplete)
    {
        CompareResult compareResult = new CompareResult();
        compareResult.setFilePath(target.getFilePath());
        compareResult.setConfigType(target.getConfigType());
        compareResult.setAttribute(target.getAttribute());
        MapDifference<String, KylinConfig> difference = Maps.difference(config, target.getConfig());
        Map<String, KylinConfig> entriesInCommon = difference.entriesInCommon();

        Map<String, MapDifference.ValueDifference<KylinConfig>> entriesDiffering = difference.entriesDiffering();
        for (Map.Entry<String, MapDifference.ValueDifference<KylinConfig>> item: entriesDiffering.entrySet())
        {
            if(item.getValue().leftValue().getBaseline().equals(item.getValue().rightValue().getBaseline()))
            {
                continue;
            }
            compareResult.addItem(item.getKey(), item.getValue().leftValue().getBaseline(),
                    item.getValue().rightValue().getBaseline());
        }

        if(!isComplete)
        {
            return compareResult;
        }

        for (Map.Entry<String, KylinConfig> item: entriesInCommon.entrySet())
        {
            compareResult.addItem(item.getKey(), item.getValue().getBaseline(), item.getValue().getBaseline());
        }

        Map<String, KylinConfig> onlyOnLeft = difference.entriesOnlyOnLeft();
        for (Map.Entry<String, KylinConfig> item: onlyOnLeft.entrySet())
        {
            compareResult.addItem(item.getKey(), item.getValue().getBaseline(), "");
        }

        Map<String, KylinConfig> entriesOnlyOnRight = difference.entriesOnlyOnRight();
        for (Map.Entry<String, KylinConfig> item: entriesOnlyOnRight.entrySet())
        {
            compareResult.addItem(item.getKey(), "", item.getValue().getBaseline());
        }
        return compareResult;
    }

}
