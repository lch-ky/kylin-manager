package cn.kylinos.kylinmanager.common.config;

import cn.kylinos.kylinmanager.common.compare.CompareResult;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import com.alibaba.fastjson.JSONObject;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigStruct {

    @Getter
    ConfigStructItem kernelConfig = new ConfigStructItem();

    @Getter
    HashMap<String, ConfigStructItem> appConfig = new HashMap<>();

    @Getter
    HashMap<String, ConfigStructItem> securityConfig = new HashMap<>();

    public JSONObject compare(ConfigStruct target, boolean bComplete)
    {
        JSONObject jsonObject = new JSONObject();
        ConfigStructItem kernelItem = target.getKernelConfig();
        CompareResult kernelResult = kernelItem.compare(kernelConfig, bComplete);
        kernelResult.setAttribute(target.getKernelConfig().getAttribute());
        kernelResult.setConfigType(target.getKernelConfig().getConfigType());
        kernelResult.setFilePath(target.getKernelConfig().getFilePath());
        jsonObject.put("kernel", kernelResult.sort());

        HashMap<String, CompareResult> appData = new HashMap<>();
        for(Map.Entry<String, ConfigStructItem> item : appConfig.entrySet())
        {
            ConfigStructItem targetApp = target.getAppConfig().get(item.getKey());
            CompareResult appResult = item.getValue().compare(targetApp, bComplete);
            if(appResult.getData().size() == 0)
            {
                continue;
            }
            appResult.setAttribute(targetApp.getAttribute());
            appResult.setConfigType(targetApp.getConfigType());
            appResult.setFilePath(targetApp.getFilePath());
            appData.put(item.getKey(), appResult.sort());
        }
        jsonObject.put("app", appData);

        HashMap<String, CompareResult> securityData = new HashMap<>();
        for(Map.Entry<String, ConfigStructItem> item : securityConfig.entrySet())
        {
            ConfigStructItem targetSecurity = target.getSecurityConfig().get(item.getKey());
            CompareResult securityResult = item.getValue().compare(targetSecurity, bComplete);
            if(securityResult.getData().size() == 0)
            {
                continue;
            }
            securityResult.setAttribute(targetSecurity.getAttribute());
            securityResult.setConfigType(targetSecurity.getConfigType());
            securityResult.setFilePath(targetSecurity.getFilePath());
            securityData.put(item.getKey(), securityResult.sort());
        }
        jsonObject.put("security", securityData);
        return jsonObject;
    }

    public List<CompareResult> compareList(ConfigStruct target, boolean bComplete)
    {
        List<CompareResult> compareResults = new ArrayList<>();
        ConfigStructItem kernelItem = target.getKernelConfig();
        CompareResult kernelResult = kernelItem.compare(kernelConfig, bComplete);
        kernelResult.setAttribute(target.getKernelConfig().getAttribute());
        kernelResult.setConfigType(target.getKernelConfig().getConfigType());
        kernelResult.setFilePath(target.getKernelConfig().getFilePath());
        compareResults.add(kernelResult.sort());

        for(Map.Entry<String, ConfigStructItem> item : appConfig.entrySet())
        {
            ConfigStructItem targetApp = target.getAppConfig().get(item.getKey());
            CompareResult appResult = item.getValue().compare(targetApp, bComplete);
            if(appResult.getData().size() == 0)
            {
                continue;
            }
            appResult.setAttribute(targetApp.getAttribute());
            appResult.setConfigType(targetApp.getConfigType());
            appResult.setFilePath(targetApp.getFilePath());
            compareResults.add(appResult.sort());
        }

        for(Map.Entry<String, ConfigStructItem> item : securityConfig.entrySet())
        {
            ConfigStructItem targetSecurity = target.getSecurityConfig().get(item.getKey());
            CompareResult securityResult = item.getValue().compare(targetSecurity, bComplete);
            if(securityResult.getData().size() == 0)
            {
                continue;
            }
            securityResult.setAttribute(targetSecurity.getAttribute());
            securityResult.setConfigType(targetSecurity.getConfigType());
            securityResult.setFilePath(targetSecurity.getFilePath());
            compareResults.add(securityResult.sort());
        }
        return compareResults;
    }

    public List<KylinConfig> toList()
    {
        List<KylinConfig> kylinConfigs = new ArrayList<>(kernelConfig.getConfig().values());
        for(Map.Entry<String, ConfigStructItem> item: appConfig.entrySet())
        {
            kylinConfigs.addAll(item.getValue().getConfig().values());
        }
        for(Map.Entry<String, ConfigStructItem> item: securityConfig.entrySet())
        {
            kylinConfigs.addAll(item.getValue().getConfig().values());
        }
        return kylinConfigs;
    }

    public void addConfigStructItem(String configType, String key, ConfigStructItem item)
    {
        if("kernel".equals(configType))
        {
            kernelConfig = item;
        }
        else if("app".equals(configType))
        {
            appConfig.put(key, item);
        }
        else if("security".equals(configType))
        {
            securityConfig.put(key, item);
        }
    }

}
