package cn.kylinos.kylinmanager.common;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Data
public class SearchParam {
    private String search;

    private Boolean paged;

    private Integer page;

    private Integer perPage;

    private String sortBy;

    private String sortOrder;

    public Sort toSort() {
        if (sortBy == null) {
            return null;
        }
        return "DESC".equalsIgnoreCase(sortOrder) ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
    }

    public Pageable toPageable() {
        if (paged != null && !paged) {
            return null;
        }
        if (page == null || page == 0) {
            page = 0;
        } else {
            page--;
        }
        if (perPage == null || perPage == 0) {
            perPage = 20;
        }
        Sort sort = toSort();
        return sort == null ? PageRequest.of(page, perPage) : PageRequest.of(page, perPage, sort);
    }
}
