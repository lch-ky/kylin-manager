package cn.kylinos.kylinmanager.configitem.controller;

import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.configitem.service.ConfigService;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.springasynctask.factory.SpringTaskManager;
import cn.kylinos.kylinmanager.springasynctask.task.configIssue.ConfigIssue;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/config")
public class IssueController {
    @Autowired
    private ConfigService configService;

    @Autowired
    private LogService logService;

    @Autowired
    private SpringTaskManager springTaskManager;

    @PostMapping("/issue")
    public RestResponse issue(@RequestBody JSONObject jsonObject){
        RestResponse restResponse = RestResponse.createSuccessResponse(null);
        try
        {
            JSONArray configIds = jsonObject.getJSONArray("configIds");
            List<Integer> hostIds = null;
            //Integer batchId = jsonObject.getInteger("batchId");
            hostIds = jsonObject.getJSONArray("hostIds").toJavaList(Integer.class);
            Integer parentId = logService.createParentLogs("配置下发", "pending", "", "配置模块", "false").getId();

            for(Integer hostId: hostIds)
            {
                HashMap<String, String> data = new HashMap<>();
                data.put("hostId", String.valueOf(hostId));
                data.put("configIds", configIds.toString());
                Integer logId = logService.createSubLogs("配置下发", "pending", "", "配置模块", "false",
                        parentId, hostId, -1).getId();
                springTaskManager.addTask(logId, new ConfigIssue(), data);
            }
        }
        catch (Exception e)
        {
            restResponse = RestResponse.createErrorResponse("10001", e.getMessage());
        }
        return restResponse;
    }
}
