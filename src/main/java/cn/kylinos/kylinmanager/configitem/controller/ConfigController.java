package cn.kylinos.kylinmanager.configitem.controller;

import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.configitem.model.KylinConfigTemplate;
import cn.kylinos.kylinmanager.configitem.service.ConfigService;
import cn.kylinos.kylinmanager.configitem.service.ConfigTemplateService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Pattern;


@RestController
@RequestMapping("/configs")
@Slf4j
public class ConfigController {
    @Autowired
    private ConfigService configService;

    @Autowired
    private ConfigTemplateService configTemplateService;

    @Operation(summary = "获取配置项列表",
            responses = {@ApiResponse(description = "获取配置项列表", content = @Content(mediaType = "application/json"))})
    @GetMapping()
    public RestResponse list(SearchParam searchParam){
        SearchResult<KylinConfig> result = configService.findConfig(searchParam);
        return RestResponse.createSuccessResponse(result);
    }

    /*传入参数格式
    {
    "name":"",
    "configType":"",
    "attribute":"",
    "baseline":"",
    "description":"",
    "flag":"",
    "filePath":"",
    "standardId":"",
    "standardName":""
    }
     */
    @Operation(summary = "创建配置项",
            responses = {@ApiResponse(description = "创建配置项", content = @Content(mediaType = "application/json"))})
    @PostMapping()
    public RestResponse create(@Parameter(description = "创建配置项信息") @RequestBody KylinConfig config) {
        RestResponse restResponse = dataVerification(config);
        if (restResponse != null)
        {
            return restResponse;
        }
        if(configService.isExist(config))
        {
            return RestResponse.createErrorResponse("3006", "The config item is exit");
        }
        configService.createConfig(config);
        return RestResponse.createSuccessResponse(null);
    }
    /*传入参数格式
    {
    "id":"",
    "name":"",
    "configType":"",
    "attribute":"",
    "baseline":"",
    "description":"",
    "flag":"",
    "filePath":"",
    "standardId":"",
    "standardName":""
    }
     */
    @Operation(summary = "更新配置项",
            responses = {@ApiResponse(description = "更新配置项", content = @Content(mediaType = "application/json"))})
    @PutMapping()
    public RestResponse update(@Parameter(description = "更新配置项信息") @RequestBody KylinConfig config) {
        if (configService.findById(config.getId()).isEmpty()) {
            return RestResponse.createErrorResponse("3001", "The config item does not exist");
        }
        RestResponse restResponse = dataVerification(config);
        if (restResponse != null)
        {
            return restResponse;
        }
        configService.updateConfig(config);
        return RestResponse.createSuccessResponse(null);
    }

    /*传入参数格式
   {
   "ids":[1,2,3]
   }
    */
    @Operation(summary = "删除配置项",
            responses = {@ApiResponse(description = "删除配置项", content = @Content(mediaType = "application/json"))})
    @DeleteMapping()
    public RestResponse delete(@Parameter(description = "需要删除的删除配置项Id") @RequestBody JSONObject configArray) {
        List<Integer> configIds = JSONObject.parseArray(configArray.getJSONArray("ids").toJSONString(), Integer.class);
        configService.deleteConfig(configIds);
        return RestResponse.createSuccessResponse(null);
    }

    @Operation(summary = "config_type、attribute、file_path对应关系",
            responses = {@ApiResponse(description = "config_type、attribute、file_path对应关系", content = @Content(mediaType = "application/json"))})
    @GetMapping("/classinfo")
    public RestResponse classinfo(){
        return RestResponse.createSuccessResponse(configService.classinfo());
    }


    private RestResponse dataVerification(KylinConfig config){
        config.setName(config.getName().replace(" ", ""));
        config.setBaseline(config.getBaseline().replace("\t", " "));
        String baseline;
        if (config.getBaseline().contains(" ")){
            baseline = "array";
        }else{
            baseline = "string";
        }
        KylinConfigTemplate resutlt = configTemplateService.findByName(config.getName());
        if (resutlt != null ){
            return check(resutlt,config,baseline);
        }
        List<String> templateName = configTemplateService.findNameByMatch();
        String temName = "";
        for (String value:templateName){
            if (Pattern.matches(value, config.getName())) {
                temName = value;
                break;
            }
        }
        if ("".equals(temName)){
            return RestResponse.createErrorResponse("3002", "The config name error");
        }
        KylinConfigTemplate matchResutlt = configTemplateService.findByName(temName);
        return check(matchResutlt,config,baseline);
    }

    private RestResponse check(KylinConfigTemplate resutlt, KylinConfig config, String baseline){
        if (! resutlt.getConfigType().equals(config.getConfigType())) {
            return RestResponse.createErrorResponse("3005", "The config type error");
        }
        if (! resutlt.getAttribute().equals(config.getAttribute())) {
            return RestResponse.createErrorResponse("3004", "The config attribute type error");
        }
        if (! resutlt.getValueType().equals(baseline)) {
            return RestResponse.createErrorResponse("3003", "The config baseline type error");
        }
        return null;
    }

}
