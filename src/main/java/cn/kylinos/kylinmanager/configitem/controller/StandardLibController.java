package cn.kylinos.kylinmanager.configitem.controller;


import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.configitem.service.StandardLibService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/standardlibs")
@Slf4j
public class StandardLibController {
    @Autowired
    private StandardLibService standardLibService;

    @Operation(summary = "获取标准库列表",
            responses = {@ApiResponse(description = "获取标准库列表", content = @Content(mediaType = "application/json"))})
    @GetMapping()
    public RestResponse list(@Parameter(description = "列表参数") SearchParam searchParam) {
        return RestResponse.createSuccessResponse(standardLibService.findStandardLib(searchParam));
    }

}
