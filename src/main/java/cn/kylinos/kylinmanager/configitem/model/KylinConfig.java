package cn.kylinos.kylinmanager.configitem.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@Entity
@Table(name="kylin_configs")
public class KylinConfig implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "config_type")
    private String configType;

    @Column(name = "attribute")
    private String attribute;

    @Column(name = "baseline")
    private String baseline;

    @Column(name = "description")
    private String description;

    @Column(name = "flag")
    private String flag;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "reserved1")
    private String reserved1;

    @Column(name = "reserved2")
    private String reserved2;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "standard_id")
    private Integer standardId;

    @Column(name = "standard_name")
    private String standardName;

    @Column(name = "os_arch")
    private String standardArch;

    @Column(name = "os_vesion")
    private String standardVersion;

    public KylinConfig()
    {
        createdAt = Instant.now();
        updatedAt = Instant.now();
    }

}



