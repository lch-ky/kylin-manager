package cn.kylinos.kylinmanager.configitem.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="kylin_standardlibs")
public class KylinStandardlib implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "version")
    private String version;

    @Column(name = "cpu_arch")
    private String cpuArch;

    @Column(name = "reserved1")
    private String reserved1;

    @Column(name = "reserved2")
    private String reserved2;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updatedAt")
    private Instant updatedAt;
}
