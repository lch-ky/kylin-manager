package cn.kylinos.kylinmanager.configitem.dao;

import cn.kylinos.kylinmanager.configitem.model.KylinStandardlib;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface StandardLibRepository extends JpaRepository<KylinStandardlib, Integer>, JpaSpecificationExecutor<KylinStandardlib> {
    KylinStandardlib findByName(String name);

    KylinStandardlib findByNameAndVersionAndCpuArch(String name, String version, String CpuArch);

}
