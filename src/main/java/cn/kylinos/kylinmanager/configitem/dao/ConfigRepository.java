package cn.kylinos.kylinmanager.configitem.dao;

import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface ConfigRepository extends JpaRepository<KylinConfig, Integer>, JpaSpecificationExecutor<KylinConfig> {
   KylinConfig findByName(String name);

   KylinConfig findByNameAndConfigTypeAndAttribute(String name, String configType, String attribute);

   List<KylinConfig> findByStandardId(Integer standardId);

   List<KylinConfig> findByIdIn(List<Integer> ids);

   List<KylinConfig> findByStandardIdAndConfigTypeAndNameIn(Integer standardId, String configType, List<String> names);
   List<KylinConfig> findByStandardIdAndAttributeAndNameIn(Integer standardId, String attribute, List<String> names);

   @Modifying
   @Transactional
   @Query("delete from KylinConfig config where config.id in (?1)")
   void delete(List<Integer> configIds);

   @Transactional
   @Query("select DISTINCT attribute from KylinConfig where configType = :#{#configType}")
   List<String> findConfigTypeDistinctByAttribute(@Param("configType") String configType);

   @Transactional
   @Query("select DISTINCT filePath from KylinConfig  where attribute = :#{#attribute}")
   List<String> findAttributeDistinctByFilePath(@Param("attribute") String attribute);

}
