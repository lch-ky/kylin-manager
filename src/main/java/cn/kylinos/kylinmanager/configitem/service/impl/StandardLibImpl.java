package cn.kylinos.kylinmanager.configitem.service.impl;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.configitem.dao.ConfigRepository;
import cn.kylinos.kylinmanager.configitem.dao.StandardLibRepository;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.configitem.model.KylinStandardlib;
import cn.kylinos.kylinmanager.configitem.service.StandardLibService;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class StandardLibImpl implements StandardLibService {
    @Autowired
    private StandardLibRepository standardlibRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Override
    @Transactional
    public SearchResult<KylinStandardlib> findStandardLib(SearchParam searchParam){
        return AdvancedSearch.findBySearch(searchParam, standardlibRepository);
    }

    @Override
    public KylinStandardlib findByName(String name) {
        return standardlibRepository.findByName(name);
    }

    @Override
    public List<KylinConfig> findConfigsByHostInfo(String osName, String version, String arch) {
        KylinStandardlib kylinStandardlib = standardlibRepository.findByNameAndVersionAndCpuArch(osName, version, arch);
        return configRepository.findByStandardId(kylinStandardlib.getId());
    }

    @Override
    public boolean isStandardExist(String osName, String version, String arch) {
        return standardlibRepository.findByNameAndVersionAndCpuArch(osName, version, arch) != null;
    }

    @Override
    public Integer createStandard(String osName, String version, String arch) {
        KylinStandardlib kylinStandardlib = new KylinStandardlib();
        kylinStandardlib.setName(osName);
        kylinStandardlib.setCpuArch(arch);
        kylinStandardlib.setVersion(version);
        kylinStandardlib.setCreatedAt(Instant.now());
        kylinStandardlib.setUpdatedAt(Instant.now());
        standardlibRepository.save(kylinStandardlib);
        return kylinStandardlib.getId();
    }

    @Override
    public Integer findIdByHostInfo(String osName, String version, String arch) {
        KylinStandardlib kylinStandardlib = standardlibRepository.findByNameAndVersionAndCpuArch(osName, version, arch);
        if (kylinStandardlib == null)
        {
            return -1;
        }
        return kylinStandardlib.getId();
    }
}
