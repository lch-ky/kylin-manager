package cn.kylinos.kylinmanager.configitem.service;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ConfigService {


    SearchResult<KylinConfig> findConfig(SearchParam searchParam);

    KylinConfig findByName(String name);

    Optional<KylinConfig> findById(Integer id);

    KylinConfig createConfig(KylinConfig configParam);

    KylinConfig updateConfig(KylinConfig configParam);

    void deleteConfig(List<Integer> configIdList);

    Boolean isExist(KylinConfig kylinConfig);

    void addConfigs(List<KylinConfig> kylinConfigList);

    List<KylinConfig> findByStandardId(Integer standardId);

    List<KylinConfig> findByIds(List<Integer> configIds);

    Map<String, KylinConfig> findByNameConfigTypeList(Integer standardId, String configType, List<String> nameList);

    Map<String, KylinConfig> findByNameAttributeList(Integer standardId, String attribute, List<String> nameList);

    JSONObject classinfo();

}
