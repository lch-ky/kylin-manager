package cn.kylinos.kylinmanager.configitem.service.impl;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.configitem.dao.ConfigRepository;
import cn.kylinos.kylinmanager.configitem.dao.StandardLibRepository;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.configitem.model.KylinStandardlib;
import cn.kylinos.kylinmanager.configitem.service.ConfigService;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ConfigImpl implements ConfigService {
    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private StandardLibRepository standardLibRepository;

    @Override
    public SearchResult<KylinConfig> findConfig(SearchParam searchParam) {
        SearchResult<KylinConfig> searchResult = AdvancedSearch.findBySearch(searchParam, configRepository);
        List<KylinConfig> results = searchResult.getResults();
        for (KylinConfig kylinconfig : results){
            Integer standardId = kylinconfig.getStandardId();
            Optional<KylinStandardlib> standardlib = standardLibRepository.findById(standardId);
            if (standardlib.isPresent()){
                KylinStandardlib kylinStandardlib = standardlib.get();
                String standardArch = kylinStandardlib.getCpuArch();
                String standardVersion = kylinStandardlib.getVersion();
                kylinconfig.setStandardArch(standardArch);
                kylinconfig.setStandardVersion(standardVersion);
            }

        }
        return searchResult;
    }

    @Override
    public KylinConfig findByName(String name) {
        return configRepository.findByName(name);
    }

    @Override
    public Optional<KylinConfig> findById(Integer id) {
        return configRepository.findById(id);
    }

    @Override
    public KylinConfig createConfig(KylinConfig configParam) {
        configParam.setCreatedAt(Instant.now());
        configParam.setUpdatedAt(Instant.now());
        return configRepository.save(configParam);
    }

    @Override
    public KylinConfig updateConfig(KylinConfig configParam) {
        configParam.setCreatedAt(Instant.now());
        configParam.setUpdatedAt(Instant.now());
        configRepository.saveAndFlush(configParam);
        return configParam;
    }

    @Override
    public void deleteConfig(List<Integer> configIdList) {
        configRepository.delete(configIdList);
    }


    @Override
    public Boolean isExist(KylinConfig kylinConfig) {
        KylinConfig tmp = configRepository.findByNameAndConfigTypeAndAttribute(kylinConfig.getName(),
                kylinConfig.getConfigType(), kylinConfig.getAttribute());
        return tmp != null;
    }

    @Override
    public void addConfigs(List<KylinConfig> kylinConfigList) {
        configRepository.saveAll(kylinConfigList);
    }

    @Override
    public List<KylinConfig> findByStandardId(Integer standardId)
    {
        return configRepository.findByStandardId(standardId);
    }

    @Override
    public List<KylinConfig> findByIds(List<Integer> configIds) {
        return configRepository.findByIdIn(configIds);
    }

    @Override
    public Map<String, KylinConfig> findByNameConfigTypeList(Integer standardId, String configType, List<String> nameList) {
        return configRepository.findByStandardIdAndConfigTypeAndNameIn(standardId, configType, nameList).stream().
                collect(Collectors.toMap(KylinConfig::getName, a -> a,(k1, k2)->k1));
    }

    @Override
    public Map<String, KylinConfig> findByNameAttributeList(Integer standardId, String attribute, List<String> nameList) {
        return configRepository.findByStandardIdAndAttributeAndNameIn(standardId, attribute, nameList).stream().
                collect(Collectors.toMap(KylinConfig::getName, a -> a,(k1, k2)->k1));
    }

    @Override
    public JSONObject classinfo(){
        String[] configType = {"内核参数","应用配置","安全配置"};
        JSONObject configAttrFilePath =  new JSONObject();
        for (String str : configType){
            List<String> attribute = configRepository.findConfigTypeDistinctByAttribute(str);
            JSONArray attrFilePaths = new JSONArray();
            for (String att: attribute){
                JSONObject attFilePath = new JSONObject();
                List<String> filePath = configRepository.findAttributeDistinctByFilePath(att);
                for (String fp : filePath){
                    attFilePath.put(att, fp);
                    attrFilePaths.add(attFilePath);
                }
            }
            configAttrFilePath.put(str,attrFilePaths);
        }

        return configAttrFilePath;

    }
}
