package cn.kylinos.kylinmanager.configitem.service;

import cn.kylinos.kylinmanager.configitem.model.KylinConfigTemplate;

import java.util.List;

public interface ConfigTemplateService {
    KylinConfigTemplate findByName(String name);

    List<String> findNameByMatch();
}
