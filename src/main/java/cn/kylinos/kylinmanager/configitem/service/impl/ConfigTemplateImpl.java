package cn.kylinos.kylinmanager.configitem.service.impl;

import cn.kylinos.kylinmanager.configitem.dao.ConfigTemplateRepossitory;
import cn.kylinos.kylinmanager.configitem.model.KylinConfigTemplate;
import cn.kylinos.kylinmanager.configitem.service.ConfigTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ConfigTemplateImpl  implements ConfigTemplateService {

    @Autowired
    private ConfigTemplateRepossitory configTemplateRepossitory;


    @Override
    public KylinConfigTemplate findByName(String name) {
        return configTemplateRepossitory.findByName(name);
    }

    @Override
    public List<String> findNameByMatch() {
        return configTemplateRepossitory.findNameByMatch();
    }
}
