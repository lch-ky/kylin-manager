package cn.kylinos.kylinmanager.configitem.service;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.configitem.model.KylinConfig;
import cn.kylinos.kylinmanager.configitem.model.KylinStandardlib;

import java.util.List;

public interface StandardLibService {
    SearchResult<KylinStandardlib> findStandardLib(SearchParam searchParam);

    KylinStandardlib findByName(String username);

    List<KylinConfig> findConfigsByHostInfo(String osName, String version, String arch);

    boolean isStandardExist(String osName, String version, String arch);

    Integer createStandard(String osName, String version, String arch);

    Integer findIdByHostInfo(String osName, String version, String arch);
}
