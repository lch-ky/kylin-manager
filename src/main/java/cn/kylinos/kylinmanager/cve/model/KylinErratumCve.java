package cn.kylinos.kylinmanager.cve.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="kylin_erratum_cves")
public class KylinErratumCve {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "erratum_id")
    private Integer erratumId;

    @Column(name = "cve_id")
    private String cveId;

    @Column(name = "href")
    private String href;
}
