package cn.kylinos.kylinmanager.cve.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="kylin_erratum_packages")
public class KylinErratumPackage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "cve_id")
    private Integer cveId;

    @Column(name = "nvrea")
    private String nvrea;

    @Column(name = "name")
    private String name;

    @Column(name = "filename")
    private String filename;


}
