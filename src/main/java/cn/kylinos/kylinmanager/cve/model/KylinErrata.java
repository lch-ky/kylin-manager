package cn.kylinos.kylinmanager.cve.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="kylin_errata")
public class KylinErrata implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "errata_id")
    private String errataId;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "issued_date")
    private Date issuedDate;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Column(name = "errata_type")
    private String errataType;

    @Column(name = "severity")
    private String severity;

    @Column(name = "title")
    private String title;

    @Column(name = "solution")
    private String solution;

    @Column(name = "description")
    private String description;

    @Column(name = "summary")
    private String summary;

    @Column(name = "reboot_suggested")
    private Boolean rebootSuggested;


}
