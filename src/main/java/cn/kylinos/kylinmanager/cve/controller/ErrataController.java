package cn.kylinos.kylinmanager.cve.controller;


import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.cve.service.ErrataService;
import cn.kylinos.kylinmanager.util.UncompressFileTAR;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@RestController
@RequestMapping("/errata")
@Slf4j
public class ErrataController {
    @Autowired
    private ErrataService errataService;

    @Value("${cve.decompressPath}")
    private String cveDecompressPath;

    @Operation(summary = "获取errara列表",
            responses = {@ApiResponse(description = "获取errata列表", content = @Content(mediaType = "application/json"))})
    @GetMapping()
    public RestResponse list(@Parameter(description = "列表参数") SearchParam searchParam){
        return RestResponse.createSuccessResponse(errataService.findErrata(searchParam));
    }


    @Operation(summary = "cve解析",
            responses = {@ApiResponse(description = "cve解析", content = @Content(mediaType = "multipart/form-data"))})
    @PostMapping()
    public RestResponse create(@Parameter(description = "安全公告") @RequestParam("file") MultipartFile file,
                               @Parameter(description = "软件源id") @RequestParam Integer repoId) throws Exception {
        //接收上传文件
        HashMap<String, String> uploadResult = errataService.upload(file);
        String uploadCode = uploadResult.get("code");
        if (!"200".equals(uploadCode)) {
            return RestResponse.createErrorResponse(uploadCode, uploadResult.get("result"));
        }
        String tarPath = uploadResult.get("result");

        //解压tar.gz
        UncompressFileTAR uncompressFileTAR = new UncompressFileTAR();
        HashMap<String, String> decompressResult = uncompressFileTAR.decompress(tarPath, cveDecompressPath);
        String decompressCode = decompressResult.get("code");
        if(!"200".equals(decompressCode)){
            return RestResponse.createErrorResponse(decompressCode, decompressResult.get("result"));
        }
        String decompressPath = decompressResult.get("result");

        //解析xml
        errataService.parseXML(decompressPath,repoId);
        return  RestResponse.createSuccessResponse(null);
    }

}



