package cn.kylinos.kylinmanager.cve.dao;

import cn.kylinos.kylinmanager.cve.model.KylinErratumCve;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ErratumCveRepository extends JpaRepository<KylinErratumCve, Integer> {

}
