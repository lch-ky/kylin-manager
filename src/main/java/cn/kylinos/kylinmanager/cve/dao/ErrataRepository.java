package cn.kylinos.kylinmanager.cve.dao;

import cn.kylinos.kylinmanager.cve.model.KylinErrata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ErrataRepository extends JpaRepository<KylinErrata, Integer>, JpaSpecificationExecutor<KylinErrata> {
    KylinErrata findByErrataId (Integer erratraId);
}
