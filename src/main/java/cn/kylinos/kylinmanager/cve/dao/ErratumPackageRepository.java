package cn.kylinos.kylinmanager.cve.dao;

import cn.kylinos.kylinmanager.cve.model.KylinErratumPackage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ErratumPackageRepository extends JpaRepository<KylinErratumPackage, Integer> {

}
