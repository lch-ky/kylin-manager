package cn.kylinos.kylinmanager.cve.service;


import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.cve.model.KylinErrata;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

public interface ErrataService {

    SearchResult<KylinErrata> findErrata(SearchParam searchParam);

    HashMap<String, String> upload(MultipartFile file);

    JSONObject parseXML(String decompressPath, Integer repoId) throws Exception;



}
