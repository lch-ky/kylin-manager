package cn.kylinos.kylinmanager.cve.service.impl;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.config.CveConfig;
import cn.kylinos.kylinmanager.cve.dao.ErrataRepository;
import cn.kylinos.kylinmanager.cve.dao.ErratumCveRepository;
import cn.kylinos.kylinmanager.cve.dao.ErratumPackageRepository;
import cn.kylinos.kylinmanager.cve.model.KylinErrata;
import cn.kylinos.kylinmanager.cve.model.KylinErratumCve;
import cn.kylinos.kylinmanager.cve.model.KylinErratumPackage;
import cn.kylinos.kylinmanager.cve.service.ErrataService;
import cn.kylinos.kylinmanager.patch.dao.ProductRepository;
import cn.kylinos.kylinmanager.patch.dao.RepoRepository;
import cn.kylinos.kylinmanager.patch.dao.RepoRpmRepository;
import cn.kylinos.kylinmanager.patch.dao.RpmRepository;
import cn.kylinos.kylinmanager.patch.model.*;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import com.alibaba.fastjson.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Service
@Transactional
public class ErrataImpl implements ErrataService {
    @Autowired
    private ErrataRepository errataRepository;

    @Autowired
    private ErratumCveRepository erratumCveRepository;

    @Autowired
    private ErratumPackageRepository erratumPackageRepository;

    @Autowired
    private CveConfig cveConfig;

    @Autowired
    private RpmRepository rpmRepository;

    @Autowired
    private RepoRpmRepository repoRpmRepository;

    @Autowired
    private RepoRepository repoRepository;

    @Autowired
    private ProductRepository productRepository;

    @Value("${cve.uploadPath}")
    private String cveUploadPath;

    @Override
    public SearchResult<KylinErrata> findErrata(SearchParam searchParam) {
        return AdvancedSearch.findBySearch( searchParam, errataRepository);
    }

    //接收tar包
    @Override
    public HashMap<String, String> upload(MultipartFile file) {
        HashMap<String, String> results = new HashMap<String, String>();
        if (file.isEmpty()) {
            results.put("code", "4001");
            results.put("result", "Please select the file to upload");
            return results;
        }
        String filename = file.getOriginalFilename();
        if (!(filename.contains(".tar.gz"))) {
            results.put("code", "4002");
            results.put("result", "The uploaded file format is wrong, please upload the file in tar or tar.gz format");
            return results;
        }

        //将文件存储在指定目录下
        mkFolder(cveUploadPath);
        String filePath = cveConfig.getUploadPath();//在xxx.application中修改
        //String filePath = String uploadPath;
        File dest = new File(filePath + filename);
        try {
            file.transferTo(dest);
            results.put("code", "200");
            results.put("result", filePath + filename);
            return results;
        } catch (IOException e) {
            e.printStackTrace();
            results.put("code", "4003");
            results.put("result", "upload failed " + e);
            return results;
        }
    }


    @Override
    public JSONObject parseXML(String decompressPath, Integer repoId) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(new File(decompressPath));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        //获取根节点
        Element root = document.getRootElement();

        //获取cvrfID
        String errataId = root.selectSingleNode("/cvrfdoc/DocumentTracking/Identification/ID").getText();

        //获取更新日期
        Date updatedDate = dateFormat.parse(root.selectSingleNode("/cvrfdoc/DocumentTracking/RevisionHistory/Revision/Date").getText());

        //获取description
        String description = root.selectSingleNode("/cvrfdoc/DocumentTitle").getText();

        //获取issuedDate
        Date issuedDate = dateFormat.parse(root.selectSingleNode("/cvrfdoc/DocumentTracking/InitialReleaseDate").getText());

        //获取fromstr //安全公告目前没有提供这个字段，后续cve平台更新后获取该字段
//        String formstr = root.selectSingleNode().getText();

        //获取DocumentTitle
        String title = root.selectSingleNode("/cvrfdoc/DocumentTitle").getText();

        //获取summary
//        Node note = root.selectSingleNode("cvrfdoc/DocumentNotes/Note");
//        List<Element> summary = ((Element) note).elements("Note");

        //获取errata_type
        String errata_type = root.selectSingleNode("/cvrfdoc/DocumentType").getText();

        //获取version
//        String version = root.selectSingleNode("cvrfdoc/DocumentTracking/Version").getText();

        //获取severity
        String severity = root.selectSingleNode("/cvrfdoc/AggregateSeverity").getText();

        //获取solution
        String solution = root.selectSingleNode("/cvrfdoc/DocumentTracking/RevisionHistory/Revision/Description").getText();

        //获取rights    //目前不支持该字段,后续更新
//        String rights = root.selectSingleNode("cvrfdoc/DocumentDistribution").getText();

        //获取reboot_suggested    //目前不支持该字段,,后续更新
        Boolean reboot_flag = false;

        //获取href
        String href = root.selectSingleNode("/cvrfdoc/DocumentReferences/Reference/URL").getText();

//        String reboot_suggested = root.selectSingleNode("/cvrfdoc/Vulnerability/Remediations/Remediation/Description").getText();
//        Boolean reboot_flag;
//        if (reboot_suggested.contains("The system must be rebooted for this update to take effect")){
//            reboot_flag = true;
//        }else{
//            reboot_flag = false;
//        }

        //获取pkglist     //目前不支持该字段,后续等待平台更新
//        List<String> pkglist = new ArrayList<>();


        System.out.println(errataId);
        System.out.println(updatedDate);

        //存储kylin_errata表中的信息
        KylinErrata errata = new KylinErrata();
        errata.setErrataId(errataId);
        errata.setIssuedDate(issuedDate);
        errata.setUpdatedDate(updatedDate);
        errata.setDescription(description);
        errata.setSeverity(severity);
        errata.setTitle(title);
        errata.setSolution(solution);
        errata.setErrataType(errata_type);
//        errata.setSummary(summary);
        errata.setRebootSuggested(reboot_flag);
        errata.setCreatedAt(Instant.now());
        errata.setUpdatedAt(Instant.now());
        errataRepository.save(errata);

        Integer errata_id = errata.getId();


        List<String> architectureBranchList = new ArrayList<>();
        List<String> RelationshipList = new ArrayList<>();

        //获取ProductTree节点
        Node productTree = root.selectSingleNode("/cvrfdoc/ProductTree");

        //得到Branch Type
        List<Element> branchList = ((Element) productTree).elements("Branch");

        //得到Relationship
        List<Element> relationshipList = ((Element) productTree).elements("Relationship");

        //循环branchList集合
        for (Element branch : branchList) {
            //获取第三层branch
            List<Element> Branch =branch.elements("Branch");
            if (Branch != null){
                //获取FullProductName的值
                for (Element ProductName : Branch){
                    Element FullProductNameElement =ProductName.element("FullProductName");
                    String FullProductName = FullProductNameElement.getText();
                    architectureBranchList.add(FullProductName);
                    System.out.println(FullProductName);
                }
            }
        }

        System.out.println("\n elationshipList");
        for (Element relationship : relationshipList){
            Element RelationshipElement =relationship.element("FullProductName");
            String Relationshipname = RelationshipElement.attributeValue("ProductID");
            RelationshipList.add(Relationshipname);
            System.out.println(Relationshipname);
        }

        //获取productID
        List<String> ProductIDList = new ArrayList<>();
        List<String> cveList = new ArrayList<>();
        HashMap<String, List> CVEProduct = new HashMap<>();
        //获取Vulnerability节点
        List<Element> Vulnerability = root.elements("Vulnerability");

        System.out.println("\nVulnerability");
        for (Element Vulner:Vulnerability){
            String cve = Vulner.element("CVE").getText();
            KylinErratumCve erratumCve = new KylinErratumCve();
            System.out.println(cve);
            erratumCve.setErratumId(errata_id);
            erratumCve.setCveId(cve);
            erratumCve.setHref(href);
            erratumCveRepository.save(erratumCve);
            cveList.add(cve);
            //得到ProductStatuses
            Element ProductStatuses = Vulner.element("ProductStatuses");
            Element Status = ProductStatuses.element("Status");
            List<Element> ProductIDs = Status.elements("ProductID");
            for (Element ProductID: ProductIDs){
                String Product = ProductID.getText();
                String[] nvrea = getNvrea(Product);
                String system = nvrea[0];
                String nvra = nvrea[1];
                String arch = nvrea[2];
                String name = nvra.substring(0,nvra.lastIndexOf("-",nvra.lastIndexOf("-")-1));
                String vrs = nvra.substring(nvra.lastIndexOf("-",nvra.lastIndexOf("-")-1)+1);
                String[] vr = vrs.split("-");
                String version = vr[0];
                String release = vr[1];
                Integer cveId = erratumCve.getId();
                KylinErratumPackage erratumPackage = new KylinErratumPackage();
                erratumPackage.setCveId(cveId);
                erratumPackage.setName(name);
                erratumPackage.setNvrea(nvra);
                erratumPackageRepository.save(erratumPackage);
                Rpm rpm = new Rpm();
                rpm.setCreatedAt(Instant.now());
                rpm.setUpdatedAt(Instant.now());
                rpm.setName(name);
                rpm.setVersion(version);
                rpm.setRelease(release);
                rpm.setArch(arch);
                rpm.setSourcerpm(nvra+".src.rpm");
                rpm.setNvrea(nvra);
                rpmRepository.save(rpm);
                ProductIDList.add(Product);
                System.out.println(Product);

                Integer rpmId = rpm.getId();

                RepositoryRpmKey repoRpmKey = new RepositoryRpmKey();
                repoRpmKey.setRpmId(rpmId);

                //获取osArch && osVersion并将rpm包进行分类存储
                Optional<Repository> repo = repoRepository.findById(repoId);
                Product product = repo.get().getProduct();
                String osArch = product.getOsArch();
                String osVersion  = product.getOsVersion();

                switch (osVersion){
                    case "V7.0":
                        if("NeoKylin_V7".equals(system)){
                            if (osArch.equals(arch) || "noarch".equals(arch)){
                                saveRepo(rpm,repoId,repoRepository,repoRpmRepository);
                            }
                        }
                        break;
                    case "V10(SP1)":
                        if("Kylin_V10_SP1".equals(system) || "Kylin_V10".equals(system)){
                            if (osArch.equals(arch) || "noarch".equals(arch)){
                                saveRepo(rpm,repoId,repoRepository,repoRpmRepository);
                            }
                        }
                        break;
                    case "V10(SP2)":
                        if("Kylin_V10_SP2".equals(system)){
                            if (osArch.equals(arch) || "noarch".equals(arch)){
                                saveRepo(rpm,repoId,repoRepository,repoRpmRepository);
                            }
                        }
                        break;
                }
            }
            CVEProduct.put(cve, ProductIDList);
        }
        System.out.println(CVEProduct);
        return null;
    }

    public static String[] getNvrea(String Product) {
        return Product.split(":");
    }

    public static void saveRepo(Rpm rpm, Integer repoId, RepoRepository repoRepository, RepoRpmRepository repoRpmRepository){
        int repositoryId;
        int productId;
        Integer rpmId = rpm.getId();
        RepositoryRpmKey repoRpmKey = new RepositoryRpmKey();
        repoRpmKey.setRpmId(rpmId);
        RepositoryRpm repoRpm = new RepositoryRpm();
        repoRpm.setCreatedAt(Instant.now());
        repoRpm.setUpdatedAt(Instant.now());
        repoRpmKey.setRepositoryId(repoId);
        repositoryId = repoId;
        productId = repoId;
        Repository repository = repoRepository.findRepositoryByIdAndProductId(repositoryId, productId);
        repoRpm.setRepository(repository);
        repoRpm.setId(repoRpmKey);
        repoRpm.setRpm(rpm);
        repoRpmRepository.save(repoRpm);
    }

    //创建文件夹
    private static void mkFolder(String fileName) {
        File f = new File(fileName);
        if(!f.exists()) {
            f.setWritable(true, false);
            f.mkdirs();
        }
    }

}







