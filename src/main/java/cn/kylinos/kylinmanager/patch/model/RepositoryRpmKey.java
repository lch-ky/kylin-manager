package cn.kylinos.kylinmanager.patch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class RepositoryRpmKey implements Serializable {
    @Column(name = "repository_id")
    private Integer repositoryId;
    @Column(name = "rpm_id")
    private Integer rpmId;

    public Integer getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(Integer repositoryId) {
        this.repositoryId = repositoryId;
    }

    public Integer getRpmId() {
        return rpmId;
    }

    public void setRpmId(Integer rpmId) {
        this.rpmId = rpmId;
    }
}
