package cn.kylinos.kylinmanager.patch.model;

import cn.kylinos.kylinmanager.auth.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;

@Data
@Entity
@Table(name = "kylin_repositories")
public class Repository {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "url")
    private String url;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "mirror_on_sync")
    private Boolean mirrorOnSync;

    @Column(name = "upstream_username")
    private String upstreamUsername;

    @Column(name = "upstream_password")
    private String upstreamPassword;

    @Column(name = "last_sync_time")
    private Instant lastSyncTime;

    @Column(name = "last_sync_status")
    private String lastSyncStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "locked")
    private Boolean locked;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "repository")
    private Set<RepositoryRpm> repositoryRpmSet;
}
