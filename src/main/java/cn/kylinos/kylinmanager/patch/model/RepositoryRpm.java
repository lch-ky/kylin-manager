package cn.kylinos.kylinmanager.patch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "kylin_repository_rpms")
public class RepositoryRpm {
    @EmbeddedId
    private RepositoryRpmKey id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @ManyToOne
    @MapsId("repositoryId")
    @JoinColumn(name = "repository_id")
    private Repository repository;

    @ManyToOne
    @MapsId("rpmId")
    @JoinColumn(name = "rpm_id")
    private Rpm rpm;

    public RepositoryRpm(Repository repository, Rpm rpm) {
        this.id = new RepositoryRpmKey(repository.getId(), rpm.getId());
        this.repository = repository;
        this.rpm = rpm;
        this.createdAt = Instant.now();
        this.updatedAt = Instant.now();
    }
}
