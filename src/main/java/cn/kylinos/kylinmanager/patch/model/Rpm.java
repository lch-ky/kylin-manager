package cn.kylinos.kylinmanager.patch.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "kylin_rpms")
public class Rpm {
    private static Pattern SORTABLE_REGEX_PATTERN = Pattern.compile("([A-Za-z]+|\\d+)");

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "name")
    private String name;

    @Column(name = "version")
    private String version;

    @Column(name = "release")
    private String release;

    @Column(name = "arch")
    private String arch;

    @Column(name = "epoch")
    private String epoch;

    @Column(name = "filename")
    private String filename;

    @Column(name = "sourcerpm")
    private String sourcerpm;

    @Column(name = "sha256")
    private String sha256;

    @Column(name = "version_sortable")
    private String versionSortable;

    @Column(name = "release_sortable")
    private String releaseSortable;

    @Column(name = "summary")
    private String summary;

    @Column(name = "nvrea")
    private String nvrea;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "rpm", cascade = CascadeType.MERGE)
    private Set<RpmAttr> rpmAttrSet;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "rpm", cascade = CascadeType.MERGE)
    private Set<RpmDep> rpmDepSet;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "rpm")
    private Set<RepositoryRpm> repositoryRpmSet;

    public void setName(String name) {
        this.name = name;
        this.nvrea = getNvrea();
    }

    public void setArch(String arch) {
        this.arch = arch;
        this.nvrea = getNvrea();
    }

    public void setEpoch(String epoch) {
        this.epoch = epoch;
        this.nvrea = getNvrea();
    }

    /**
     * Converts a package version to a sortable string
     *
     * See the Fedora docs for what chars are accepted
     * https://fedoraproject.org/wiki/Archive:Tools/RPM/VersionComparison
     * @param version RPM package version
     */
    public void setVersion(String version) {
        this.version = version;
        this.versionSortable = SORTABLE_REGEX_PATTERN.matcher(version)
                .results()
                .map(MatchResult::group)
                .map(chunk ->
                        chunk.matches("\\d+") ?
                                String.format("%02d-%s", chunk.length(), chunk) :
                                String.format("$%s", chunk))
                .collect(Collectors.joining("."));
        this.nvrea = getNvrea();
    }

    public void setRelease(String release) {
        this.release = release;
        this.releaseSortable = SORTABLE_REGEX_PATTERN.matcher(release)
                .results()
                .map(MatchResult::group)
                .map(chunk ->
                        chunk.matches("\\d+") ?
                                String.format("%02d-%s", chunk.length(), chunk) :
                                String.format("$%s", chunk))
                .collect(Collectors.joining("."));
        this.nvrea = getNvrea();
    }

    public String getNvrea() {
        return (this.epoch+":"+this.name+"-"+this.version+"-"+this.release+"."+this.arch);
    }

}
