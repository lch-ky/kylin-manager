package cn.kylinos.kylinmanager.patch.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "kylin_rpm_attrs")
public class RpmAttr {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "rpm_id")
    @EqualsAndHashCode.Exclude
    private Rpm rpm;

    @Column(name = "attr")
    private String attr;

    @Column(name = "content")
    private String content;

    public RpmAttr(Rpm rpm, String attr, String content) {
        this.rpm = rpm;
        this.attr = attr;
        this.content = content;
        this.createdAt = Instant.now();
        this.updatedAt = Instant.now();
    }
}
