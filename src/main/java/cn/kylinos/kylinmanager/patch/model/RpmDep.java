package cn.kylinos.kylinmanager.patch.model;

import cn.kylinos.kylinmanager.util.repoparser.model.Entry;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "kylin_rpm_deps")
public class RpmDep {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "type")
    private String type;

    @Column(name = "name")
    private String name;

    @Column(name = "flags")
    private Integer flags;

    @Column(name = "version")
    private String version;

    @Column(name = "epoch")
    private String epoch;

    @Column(name = "release")
    private String release;

    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "rpm_id")
    private Rpm rpm;

    public RpmDep(String type, Rpm rpm, Entry entry) {
        this.type = type;
        this.rpm = rpm;
        this.name = entry.getName();
        this.flags = entry.getFlags().intValue();
        this.version = entry.getVersion();
        this.epoch = entry.getEpoch();
        this.release = entry.getRelease();
        this.createdAt = Instant.now();
        this.updatedAt = Instant.now();
    }
}
