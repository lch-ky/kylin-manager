package cn.kylinos.kylinmanager.patch.model;

import cn.kylinos.kylinmanager.auth.model.User;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "kylin_products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "label", unique = true)
    private String label;

    @Column(name = "os_version")
    private String osVersion;

    @Column(name = "os_name")
    private String osName;

    @Column(name = "os_arch")
    private String osArch;

    @Column(name = "address")
    private String address;

    @Column(name = "locked")
    private Boolean locked;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
