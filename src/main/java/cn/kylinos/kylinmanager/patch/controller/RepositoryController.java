package cn.kylinos.kylinmanager.patch.controller;

import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.patch.dto.RepositoryDTO;
import cn.kylinos.kylinmanager.patch.dto.RepositoryUpdateParam;
import cn.kylinos.kylinmanager.patch.model.Repository;
import cn.kylinos.kylinmanager.patch.service.ProductService;
import cn.kylinos.kylinmanager.patch.service.RepositoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products/{productId}/repositories")
public class RepositoryController {
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ModelMapper modelMapper;

    @Operation(summary = "查询软件包组列表",
            responses = {@ApiResponse(description = "软件包组列表", content = @Content(mediaType = "application/json"))})
    @GetMapping()
    public RestResponse index(@Parameter(description = "软件包组隶属的产品源ID") @PathVariable Integer productId,
                              @Parameter(description = "分页、查询参数") SearchParam searchParam) {
        SearchResult<Repository> result = repositoryService.index(productId, searchParam);
        SearchResult<RepositoryDTO> dtoResult = result.mapResults(repository -> modelMapper.map(repository, RepositoryDTO.class));
        return RestResponse.createSuccessResponse(dtoResult);
    }

    @Operation(summary = "创建软件包组",
            responses = {@ApiResponse(description = "软件包组详情", content = @Content(mediaType = "application/json"))})
    @PostMapping()
    public RestResponse create(@Parameter(description = "软件包组隶属的产品源ID") @PathVariable Integer productId,
                               @Parameter(description = "软件包组参数") @RequestBody Repository repository) {
        repository.setProduct(productService.findProductById(productId));
        Repository repo = repositoryService.create(repository);
        RepositoryDTO repoDTO = modelMapper.map(repo, RepositoryDTO.class);
        return RestResponse.createSuccessResponse(repoDTO);
    }

    @Operation(summary = "更新软件包组详情",
            responses = {@ApiResponse(description = "软件包组详情", content = @Content(mediaType = "application/json"))})
    @PutMapping("/{repoId}")
    public RestResponse update(@Parameter(description = "软件包组隶属的产品源ID") @PathVariable Integer productId,
                               @Parameter(description = "软件包组ID") @PathVariable Integer repoId,
                               @Parameter(description = "软件包组更新参数") @RequestBody RepositoryUpdateParam param) {
        param.setId(repoId);
        Repository repo = repositoryService.update(productId, param);
        RepositoryDTO repoDTO = modelMapper.map(repo, RepositoryDTO.class);
        return RestResponse.createSuccessResponse(repoDTO);
    }

    @Operation(summary = "删除软件包组详情",
            responses = {@ApiResponse(description = "无返回值", content = @Content(mediaType = "application/json"))})
    @DeleteMapping("/{repoId}")
    public RestResponse delete(@Parameter(description = "软件包组隶属的产品源ID") @PathVariable Integer productId,
                               @Parameter(description = "软件包组ID") @PathVariable Integer repoId) {
        repositoryService.delete(productId, repoId);
        return RestResponse.createSuccessResponse(null);
    }

    @Operation(summary = "同步软件包组(异步)",
            responses = {@ApiResponse(description = "任务详情", content = @Content(mediaType = "application/json"))})
    @PostMapping("/{repoId}/sync")
    public RestResponse sync(@Parameter(description = "软件包组隶属的产品源ID") @PathVariable Integer productId,
                             @Parameter(description = "软件包组ID") @PathVariable Integer repoId) {
        return RestResponse.createSuccessResponse(repositoryService.sync(productId, repoId));
    }
}
