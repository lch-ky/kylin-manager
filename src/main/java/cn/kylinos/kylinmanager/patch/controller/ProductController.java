package cn.kylinos.kylinmanager.patch.controller;

import cn.kylinos.kylinmanager.common.RestResponse;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.patch.dto.ProductDTO;
import cn.kylinos.kylinmanager.patch.dto.ProductUpdateParam;
import cn.kylinos.kylinmanager.patch.model.Product;
import cn.kylinos.kylinmanager.patch.service.ProductService;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.shell.RunShellExecutor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper modelMapper;

    @Operation(summary = "查询产品源列表",
            responses = {@ApiResponse(description = "产品源列表", content = @Content(mediaType = "application/json"))})
    @GetMapping()
    public RestResponse index(@Parameter(description = "分页、查询参数") SearchParam searchParam) {
        SearchResult<Product> result = productService.index(searchParam);
        SearchResult<ProductDTO> dtoResult = result.mapResults(product -> {
            ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
            productDTO.setUserId(product.getUser().getId());
            productDTO.setUsername(product.getUser().getUsername());
            return productDTO;
        });
        return RestResponse.createSuccessResponse(dtoResult);
    }

    @Operation(summary = "查询产品源信息",
            responses = {@ApiResponse(description = "产品源信息", content = @Content(mediaType = "application/json"))})
    @GetMapping("/{productId}")
    public RestResponse index(@Parameter(description = "产品源ID") @PathVariable Integer productId) {
        Product product = productService.findProductById(productId);
        if (product == null) {
            return RestResponse.createErrorResponse(MsgCodes.RESPCODE_PRODUCT_NOT_FOUND);
        }
        ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
        productDTO.setUserId(product.getUser().getId());
        productDTO.setUsername(product.getUser().getUsername());
        return RestResponse.createSuccessResponse(productDTO);
    }

    @Operation(summary = "新建产品源",
            responses = {@ApiResponse(description = "新建立的产品源信息", content = @Content(mediaType = "application/json"))})
    @PostMapping()
    public RestResponse create(@Parameter(description = "产品源数据") @RequestBody Product product) {
        Product result = productService.create(product);
        ProductDTO productDTO = modelMapper.map(result, ProductDTO.class);
        productDTO.setUserId(result.getUser().getId());
        productDTO.setUsername(result.getUser().getUsername());
        return RestResponse.createSuccessResponse(productDTO);
    }

    @Operation(summary = "更新产品源",
            responses = {@ApiResponse(description = "更新后的产品源信息", content = @Content(mediaType = "application/json"))})
    @PutMapping("/{productId}")
    public RestResponse update(@Parameter(description = "产品源ID") @PathVariable() Integer productId,
                               @Parameter(description = "产品源数据") @RequestBody ProductUpdateParam productParam) {
        productParam.setId(productId);
        Product productNew = productService.update(productParam);
        ProductDTO productDTO = modelMapper.map(productNew, ProductDTO.class);
        productDTO.setUserId(productNew.getUser().getId());
        productDTO.setUsername(productNew.getUser().getUsername());
        return RestResponse.createSuccessResponse(productDTO);
    }

    @Operation(summary = "删除产品源",
            responses = {@ApiResponse(description = "无数据", content = @Content(mediaType = "application/json"))})
    @DeleteMapping("/{productId}")
    public RestResponse delete(@Parameter(description = "产品源ID") @PathVariable Integer productId) {
        productService.delete(productId);
        return RestResponse.createSuccessResponse(null);
    }

    @GetMapping("/test")
    public RestResponse test() {
        return RestResponse.createSuccessResponse(
                RunShellExecutor.runTask("wget http://10.1.163.73/test/v10/repodata/repomd.xml") );
    }
}
