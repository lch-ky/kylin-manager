package cn.kylinos.kylinmanager.patch.constant;

public class RepoConst {
    public final static String SYNC_REPO_ID_FIELD = "repositoryId";

    public final static String RPM_ATTR_LICENSE = "license";
    public final static String RPM_ATTR_CHECKSUM_TYPE = "checksumType";
    public final static String RPM_ATTR_CHECKSUM = "checksum";
    public final static String RPM_ATTR_DESC = "description";
    public final static String RPM_ATTR_PACKAGER = "packager";
    public final static String RPM_ATTR_URL = "url";
    public final static String RPM_ATTR_FILETIME = "fileTime";
    public final static String RPM_ATTR_BUILDTIME = "buildTime";
    public final static String RPM_ATTR_PACKSIZE = "packSize";
    public final static String RPM_ATTR_INSTALLED_SIZE = "installedSize";
    public final static String RPM_ATTR_ARCHIVE_SIZE = "archiveSize";
    public final static String RPM_ATTR_HREF = "href";
    public final static String RPM_ATTR_VENDOR = "vendor";
    public final static String RPM_ATTR_GROUP = "group";
    public final static String RPM_ATTR_BUILD_HOST = "buildHost";
    public final static String RPM_ATTR_HEADER_RANGE_START = "headerRangeStart";
    public final static String RPM_ATTR_HEADER_RANGE_END = "headerRangeEnd";

    public final static String RPM_DEP_REQUIRES = "requires";
    public final static String RPM_DEP_PROVIDES = "provides";
    public final static String RPM_DEP_CONFLICTS = "conflicts";
    public final static String RPM_DEP_OBSOLETES = "obsoletes";
    public final static String RPM_DEP_SUGGESTS = "suggests";
    public final static String RPM_DEP_RECOMMENDS = "recommends";
    public final static String RPM_DEP_ENHANCES = "enhances";
    public final static String RPM_DEP_SUPPLEMENTS = "supplements";
}


