package cn.kylinos.kylinmanager.patch.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class ProductDTO {
    private Integer id;

    private String name;

    private String description;

    private String label;

    private String osVersion;

    private String osName;

    private String osArch;

    private String address;

    private Boolean locked;

    private Instant createdAt;

    private Instant updatedAt;

    private Integer userId;

    private String username;
}
