package cn.kylinos.kylinmanager.patch.dto;

import cn.kylinos.kylinmanager.patch.model.Repository;
import lombok.Data;

import java.util.Optional;

@Data
public class RepositoryUpdateParam {
    private Integer id;

    private String name;

    private String description;

    private String url;

    public void updateRepository(Repository repository) {
        Optional.of(id).ifPresent(repository::setId);
        Optional.ofNullable(name).ifPresent(repository::setName);
        Optional.ofNullable(description).ifPresent(repository::setDescription);
        Optional.ofNullable(url).ifPresent(repository::setUrl);
    }
}
