package cn.kylinos.kylinmanager.patch.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class RepositoryDTO {
    private Long id;

    private String name;

    private String description;

    private String url;

    private String contentType;

    private Boolean mirrorOnSync;

    private Instant lastSyncTime;

    private String lastSyncStatus;

    private Integer productId;

    private Boolean locked;

    private Instant createdAt;

    private Instant updatedAt;
}
