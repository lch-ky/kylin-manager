package cn.kylinos.kylinmanager.patch.dto;

import cn.kylinos.kylinmanager.patch.model.Product;
import lombok.Data;

import java.util.Optional;

@Data
public class ProductUpdateParam {
    private Integer id;

    private String name;

    private String description;

    private String osVersion;

    private String osName;

    private String osArch;

    private String address;

    public void updateProduct(Product product) {
        Optional.of(id).ifPresent(product::setId);
        Optional.ofNullable(name).ifPresent(product::setName);
        Optional.ofNullable(description).ifPresent(product::setDescription);
        Optional.ofNullable(osVersion).ifPresent(product::setOsVersion);
        Optional.ofNullable(osName).ifPresent(product::setOsName);
        Optional.ofNullable(osArch).ifPresent(product::setOsArch);
        Optional.ofNullable(address).ifPresent(product::setAddress);
    }
}
