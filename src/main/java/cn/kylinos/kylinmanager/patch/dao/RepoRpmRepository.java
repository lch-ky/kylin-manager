package cn.kylinos.kylinmanager.patch.dao;

import cn.kylinos.kylinmanager.patch.model.RepositoryRpm;
import cn.kylinos.kylinmanager.patch.model.RepositoryRpmKey;
import cn.kylinos.kylinmanager.patch.model.Rpm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoRpmRepository extends JpaRepository<RepositoryRpm, RepositoryRpmKey> {
    RepositoryRpm findRepositoryRpmByRepositoryAndRpm(cn.kylinos.kylinmanager.patch.model.Repository repository, Rpm rpm);
}
