package cn.kylinos.kylinmanager.patch.dao;

import cn.kylinos.kylinmanager.patch.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoRepository extends JpaRepository<cn.kylinos.kylinmanager.patch.model.Repository, Integer>,
        JpaSpecificationExecutor<cn.kylinos.kylinmanager.patch.model.Repository>  {
    Integer countByNameAndProduct(String name, Product product);
    @Query(value = "select repo.* from kylin_repositories repo where repo.id = ?1 and repo.product_id = ?2", nativeQuery = true)
    cn.kylinos.kylinmanager.patch.model.Repository findRepositoryByIdAndProductId(Integer id, Integer productId);
}
