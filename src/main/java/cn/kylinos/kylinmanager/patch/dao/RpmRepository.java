package cn.kylinos.kylinmanager.patch.dao;

import cn.kylinos.kylinmanager.patch.model.Rpm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface RpmRepository extends JpaRepository<Rpm, Integer>, JpaSpecificationExecutor<Rpm> {
    Rpm findRpmBySha256(String sha256);
}
