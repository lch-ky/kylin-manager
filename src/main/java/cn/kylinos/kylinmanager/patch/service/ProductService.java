package cn.kylinos.kylinmanager.patch.service;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.patch.dto.ProductUpdateParam;
import cn.kylinos.kylinmanager.patch.model.Product;

public interface ProductService {
    SearchResult<Product> index(SearchParam searchParam);
    Product findProductById(Integer productId);
    Product create(Product productParam);
    Product update(ProductUpdateParam productParam);
    void delete(Integer productId);
}
