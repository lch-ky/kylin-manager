package cn.kylinos.kylinmanager.patch.service.impl;

import cn.kylinos.kylinmanager.auth.service.impl.UserUtils;
import cn.kylinos.kylinmanager.common.KylinException;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.logs.constants.LogConst;
import cn.kylinos.kylinmanager.logs.model.KylinParentLog;
import cn.kylinos.kylinmanager.logs.model.KylinSubLog;
import cn.kylinos.kylinmanager.logs.service.LogService;
import cn.kylinos.kylinmanager.patch.constant.RepoConst;
import cn.kylinos.kylinmanager.patch.dao.RepoRepository;
import cn.kylinos.kylinmanager.patch.dao.RepoRpmRepository;
import cn.kylinos.kylinmanager.patch.dto.RepositoryUpdateParam;
import cn.kylinos.kylinmanager.patch.model.Repository;
import cn.kylinos.kylinmanager.patch.model.RepositoryRpm;
import cn.kylinos.kylinmanager.patch.model.Rpm;
import cn.kylinos.kylinmanager.patch.service.RepositoryService;
import cn.kylinos.kylinmanager.springasynctask.factory.SpringTaskManager;
import cn.kylinos.kylinmanager.springasynctask.task.RepoSync;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.repocontent.RepoContentUtils;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.nio.file.Path;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
public class RepositoryServiceImpl implements RepositoryService {
    public static final Set<String> SUPPORTED_CONTENT_TYPE = new HashSet<>(1);
    static {
        SUPPORTED_CONTENT_TYPE.add("yum");
    }

    @Value("${repo.basePath}")
    private String repoBasePath;

    @Autowired
    private RepoRepository repoRepository;

    @Autowired
    private RepoRpmRepository repoRpmRepository;

    @Autowired
    private SpringTaskManager springTaskManager;

    @Autowired
    private LogService logService;

    @Override
    public SearchResult<Repository> index(Integer productId, SearchParam searchParam) {
        return AdvancedSearch.findBySearch(belongToProduct(productId), searchParam, repoRepository);
    }

    static Specification<Repository> belongToProduct(Integer productId) {
        return (repo, cq, cb) -> cb.equal(repo.get("product"), productId);
    }

    @Override
    public Repository findRepositoryById(Integer repositoryId) {
        return repoRepository.findById(repositoryId).orElse(null);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Repository create (Repository repository) {
        if (repository == null) {
            throw new KylinException(MsgCodes.RESPCODE_REPO_NOT_FOUND);
        }
        if (repository.getProduct() == null) {
            throw new KylinException(MsgCodes.RESPCODE_PRODUCT_NOT_FOUND);
        }
        // TODO URL format check
        // TODO Repo name format check
        if (repoRepository.countByNameAndProduct(repository.getName(), repository.getProduct()) > 0) {
            throw new KylinException(MsgCodes.RESPCODE_REPO_DUPLICATED, repository.getProduct().getName());
        }
        if (!SUPPORTED_CONTENT_TYPE.contains(repository.getContentType())) {
            throw new KylinException(MsgCodes.RESPCODE_UNSUPPPORT_CONTENT_TYPE, repository.getContentType());
        }
        createRepoPath(repository);
        repository.setCreatedAt(Instant.now());
        repository.setUpdatedAt(Instant.now());
        repository.setId(null);
        repository.setUser(UserUtils.getCurrentUser());
        repository.setLocked(Boolean.FALSE);
        repository.setMirrorOnSync(Boolean.FALSE);
        return repoRepository.save(repository);
    }

    public void createRepoPath(Repository repository) {
        String dirName = repository.getProduct().getLabel()+"_"+repository.getName();
        Path repoPath = Path.of(repoBasePath, dirName);
        RepoContentUtils.createRepoPath(repoPath);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String getRepoPath(Repository repository) {
        // Need to fetch again to avoid lazy loading of product
        Repository repo = findRepositoryById(repository.getId());
        return repo.getProduct().getLabel()+"_"+repo.getName();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Repository update(Integer productId, RepositoryUpdateParam repositoryUpdateParam) {
        Repository repository = repoRepository.findRepositoryByIdAndProductId(repositoryUpdateParam.getId(), productId);
        if (repository == null) {
            throw new KylinException(MsgCodes.RESPCODE_REPO_NOT_FOUND);
        }
        if (repository.getLocked()) {
            throw new KylinException(MsgCodes.RESPCODE_LOCKED_REPO_UPDATED_NOT_ALLOWED);
        }
        // TODO URL format check
        // TODO name formant check
        repositoryUpdateParam.updateRepository(repository);
        return repoRepository.save(repository);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer productId, Integer repositoryId) {
        Repository repository = repoRepository.findRepositoryByIdAndProductId(repositoryId, productId);
        if (repository == null) {
            throw new KylinException(MsgCodes.RESPCODE_REPO_NOT_FOUND);
        }
        if (repository.getLocked()) {
            throw new KylinException(MsgCodes.RESPCODE_LOCKED_REPO_DELETED_NOT_ALLOWED);
        }

        repoRepository.delete(repository);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public KylinParentLog sync(Integer productId, Integer repoId) {
        Repository repository = repoRepository.findRepositoryByIdAndProductId(repoId, productId);
        if (repository == null) {
            throw new KylinException(MsgCodes.RESPCODE_REPO_NOT_FOUND);
        }
        KylinParentLog parentLog = logService.createParentLogs(LogConst.ACTION_REPO_SYNC, LogConst.STATUS_READY,
                LogConst.RESULT_PENDING, LogConst.MODULE_REPO, null);
        KylinSubLog subLog = logService.createSubLogs(LogConst.ACTION_REPO_SYNC,  LogConst.STATUS_READY,
                LogConst.RESULT_PENDING, LogConst.MODULE_REPO, null, parentLog.getId(), null, null);
        HashMap<String, String> params = new HashMap<>(1);
        params.put(RepoConst.SYNC_REPO_ID_FIELD, repository.getId().toString());
        try {
            springTaskManager.addTask(subLog.getId(), new RepoSync(), params);
        } catch (Exception e) {
            logService.updateLogs(subLog.getId(), LogConst.STATUS_DONE, LogConst.RESULT_FAILED, e.getMessage(), e.getMessage());
            throw new KylinException(MsgCodes.RESPCODE_CREATE_TASK_FAILURE, e.getMessage());
        }
        return parentLog;
    }

    @Override
    public void addRpm(@Nonnull Repository repository, @Nonnull Rpm rpm) {
        RepositoryRpm rr = repoRpmRepository.findRepositoryRpmByRepositoryAndRpm(repository, rpm);
        if (rr == null) {
            rr = new RepositoryRpm(repository, rpm);
        } else {
            rr.setUpdatedAt(Instant.now());
        }
        repoRpmRepository.save(rr);
    }
}
