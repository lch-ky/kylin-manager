package cn.kylinos.kylinmanager.patch.service.impl;

import cn.kylinos.kylinmanager.patch.dao.RpmRepository;
import cn.kylinos.kylinmanager.patch.model.Rpm;
import cn.kylinos.kylinmanager.patch.service.RpmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
public class RpmServiceImpl implements RpmService {
    @Autowired
    private RpmRepository rpmRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Rpm createRpm(Rpm rpm) {
        Rpm checkRpm = rpmRepository.findRpmBySha256(rpm.getSha256());
        if (checkRpm != null) {
            return checkRpm;
        }
        rpm.setCreatedAt(Instant.now());
        rpm.setUpdatedAt(Instant.now());
        return rpmRepository.save(rpm);
    }
}
