package cn.kylinos.kylinmanager.patch.service.impl;

import cn.kylinos.kylinmanager.auth.service.impl.UserUtils;
import cn.kylinos.kylinmanager.common.KylinException;
import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.patch.dao.ProductRepository;
import cn.kylinos.kylinmanager.patch.dto.ProductUpdateParam;
import cn.kylinos.kylinmanager.patch.model.Product;
import cn.kylinos.kylinmanager.patch.service.ProductService;
import cn.kylinos.kylinmanager.util.MsgCodes;
import cn.kylinos.kylinmanager.util.search.AdvancedSearch;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public SearchResult<Product> index(SearchParam searchParam) {
        return AdvancedSearch.findBySearch(searchParam, productRepository);
    }

    @Override
    public Product findProductById(Integer productId) {
        return productRepository.findProductById(productId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Product create(Product productParam) {

        productParam.setLabel(UUID.randomUUID().toString());
        productParam.setLocked(Boolean.FALSE);
        productParam.setUser(UserUtils.getCurrentUser());
        productParam.setCreatedAt(Instant.now());
        productParam.setUpdatedAt(Instant.now());
        return productRepository.save(productParam);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Product update(ProductUpdateParam productParam) {
        Product product = productRepository.findProductById(productParam.getId());
        if (product == null) {
            throw new KylinException(MsgCodes.RESPCODE_PRODUCT_NOT_FOUND);
        }
        if (product.getLocked()) {
            throw new KylinException(MsgCodes.RESPCODE_LOCKED_PRODUCT_UPDATED_NOT_ALLOWED);
        }
        productParam.updateProduct(product);
        return productRepository.save(product);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer productId) {
        Product product = productRepository.findProductById(productId);
        if (product == null) {
            throw new KylinException(MsgCodes.RESPCODE_PRODUCT_NOT_FOUND);
        }
        if (product.getLocked()) {
            throw new KylinException(MsgCodes.RESPCODE_LOCKED_PRODUCT_DELETED_NOT_ALLOWED);
        }
        productRepository.deleteById(productId);
    }
}
