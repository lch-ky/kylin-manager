package cn.kylinos.kylinmanager.patch.service;

import cn.kylinos.kylinmanager.common.SearchParam;
import cn.kylinos.kylinmanager.common.SearchResult;
import cn.kylinos.kylinmanager.logs.model.KylinParentLog;
import cn.kylinos.kylinmanager.patch.dto.RepositoryUpdateParam;
import cn.kylinos.kylinmanager.patch.model.Repository;
import cn.kylinos.kylinmanager.patch.model.Rpm;

import javax.annotation.Nonnull;

public interface RepositoryService {
    SearchResult<Repository> index(Integer productId, SearchParam searchParam);
    Repository findRepositoryById(Integer repositoryId);
    Repository create(Repository repository);
    Repository update(Integer productId, RepositoryUpdateParam repositoryUpdateParam);
    void delete(Integer productId, Integer repositoryId);
    KylinParentLog sync(Integer productId, Integer repoId);

    void addRpm(@Nonnull Repository repository, @Nonnull Rpm rpm);
    String getRepoPath(Repository repository);
}
