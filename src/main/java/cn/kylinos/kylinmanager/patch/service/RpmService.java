package cn.kylinos.kylinmanager.patch.service;

import cn.kylinos.kylinmanager.patch.model.Rpm;

public interface RpmService {
    Rpm createRpm(Rpm rpm);
}
