/*
 Navicat Premium Data Transfer

 Source Server         : 172.17.127.151_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 120001
 Source Host           : 172.17.127.151:5432
 Source Catalog        : foreman
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120001
 File Encoding         : 65001

 Date: 23/07/2021 15:04:16
*/


-- ----------------------------
-- Table structure for kylin_erratum_cves
-- ----------------------------
DROP TABLE IF EXISTS "public"."kylin_erratum_cves";
CREATE TABLE "public"."kylin_erratum_cves" (
  "id" int4 NOT NULL PRIMARY KEY,
  "erratum_id" int4 NOT NULL,
  "cve_id" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "href" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Indexes structure for table kylin_erratum_cves
-- ----------------------------
CREATE UNIQUE INDEX "index_kylin_erratum_cves_on_erratum_id_and_cve_id_and_href" ON "public"."kylin_erratum_cves" USING btree (
  "erratum_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "cve_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "href" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table kylin_erratum_cves
-- ----------------------------

CREATE SEQUENCE "public"."kylin_erratum_cves_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;

ALTER TABLE "public"."kylin_erratum_cves" alter column "id" set default nextval('kylin_erratum_cves_id_seq'::regclass);