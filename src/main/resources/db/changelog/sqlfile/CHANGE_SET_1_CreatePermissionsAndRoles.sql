-- Create Permission table and permissions.id sequence for auto increment
CREATE TABLE "public"."permissions" (
                                        "id" int4 NOT NULL,
                                        "name" varchar(255) COLLATE "pg_catalog"."default",
                                        "created_at" timestamp(6),
                                        "updated_at" timestamp(6),
                                        CONSTRAINT "permissions_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."permissions"
    OWNER TO "kylin";

CREATE UNIQUE INDEX "index_permissions_on_name" ON "public"."permissions" USING btree (
                                                                                       "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
    );

CREATE SEQUENCE "public"."permissions_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;

ALTER SEQUENCE "public"."permissions_id_seq"
    OWNED BY "public"."permissions"."id";

ALTER SEQUENCE "public"."permissions_id_seq" OWNER TO "kylin";

ALTER TABLE "permissions" alter column "id" set default nextval('permissions_id_seq'::regclass);

-- Create Roles table and roles.id sequence for auto increment
CREATE TABLE "public"."roles" (
                                  "id" int4 NOT NULL,
                                  "name" varchar(255) COLLATE "pg_catalog"."default",
                                  "created_at" timestamp(6),
                                  "updated_at" timestamp(6),
                                  CONSTRAINT "roles_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."roles"
    OWNER TO "kylin";

CREATE UNIQUE INDEX "index_roles_on_name" ON "public"."roles" USING btree (
                                                                           "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
    );

CREATE SEQUENCE "public"."roles_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;

ALTER SEQUENCE "public"."roles_id_seq"
    OWNED BY "public"."roles"."id";

ALTER SEQUENCE "public"."roles_id_seq" OWNER TO "kylin";

ALTER TABLE "roles" alter column "id" set default nextval('roles_id_seq'::regclass);

-- Users - Permissions relation
CREATE TABLE "public"."permission_roles" (
   "permission_id" int4 NOT NULL,
   "role_id" int4 NOT NULL,
   CONSTRAINT "permission_roles_permission_id_fk" FOREIGN KEY ("permission_id") REFERENCES "public"."permissions" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT "permission_roles_role_id_fk" FOREIGN KEY ("role_id") REFERENCES "public"."roles" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
)
;

ALTER TABLE "public"."permission_roles"
    OWNER TO "kylin";

-- Roles - Users relation
CREATE TABLE "public"."role_users" (
     "role_id" int4 NOT NULL,
     "user_id" int4 NOT NULL,
     CONSTRAINT "role_users_role_id_fk" FOREIGN KEY ("role_id") REFERENCES "public"."permissions" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
     CONSTRAINT "role_users_user_id_fk" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
)
;

ALTER TABLE "public"."role_users"
    OWNER TO "kylin";

-- Create default permissions, roles and users
INSERT INTO "public"."permissions"("name", "created_at", "updated_at") values ('default:all', NOW(), NOW());
INSERT INTO "public"."roles"("name", "created_at", "updated_at") values ('admin', NOW(), NOW());
INSERT INTO "public"."users"("username", "password_hash", "password_salt", "created_at", "updated_at") values ('admin', 'd84c713dcbd3aa7da1ef52f096fbac8ce7620aab5b69a5a946b28d58b19ab6a957e3bfac02ceeb0de111f6169d8101917bec431672f34adde5a2dde65c62c969', 'c1f5bd33-6eae-41cf-9bd4-b0bd46b23a83', NOW(), NOW());
-- Create default user-role associations
INSERT INTO "public"."role_users"("user_id","role_id")
select "users"."id", "roles"."id" from users, roles where users.username='admin' and roles.name = 'admin' ;

-- Create default permission-role associations
INSERT INTO "public"."permission_roles"("role_id","permission_id")
select "roles"."id", "permissions"."id" from roles, permissions where roles.name = 'admin' and permissions.name = 'default:all';

