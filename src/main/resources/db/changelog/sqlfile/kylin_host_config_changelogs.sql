/*
 Navicat PostgreSQL Data Transfer

 Source Server         : 109
 Source Server Type    : PostgreSQL
 Source Server Version : 120001
 Source Host           : 192.168.56.109:5432
 Source Catalog        : foreman
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120001
 File Encoding         : 65001

 Date: 22/07/2021 17:59:23
*/


-- ----------------------------
-- Table structure for katello_configuration_records
-- ----------------------------
DROP TABLE IF EXISTS "public"."kylin_host_config_changelogs";
CREATE TABLE "public"."kylin_host_config_changelogs" (
  "id" int8  NOT NULL PRIMARY KEY,
  "name" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "config_type" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "attribute" varchar COLLATE "pg_catalog"."default",
  "baseline" text COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "flag" varchar COLLATE "pg_catalog"."default",
  "file_path" varchar COLLATE "pg_catalog"."default",
  "created_at" timestamp(6) NOT NULL,
  "updated_at" timestamp(6) NOT NULL,
  "host_id" int4,
  "cur_baseline" text COLLATE "pg_catalog"."default"
)
;

CREATE SEQUENCE "public"."kylin_host_config_changelogs_id_pkey"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;

-- ----------------------------
-- Primary Key structure for table katello_t_configuration_items
-- ----------------------------
ALTER TABLE "public"."kylin_host_config_changelogs" alter column "id" set default nextval('kylin_host_config_changelogs_id_pkey'::regclass);
