-- Create RPM table and rpms.id sequence for auto increment
CREATE TABLE "public"."kylin_repository_rpms" (
                                  "created_at" timestamp(6),
                                  "updated_at" timestamp(6),
                                  "repository_id" int4 NOT NULL,
                                  "rpm_id" int4 NOT NULL,
                                  CONSTRAINT "repo_rpms_pkey" PRIMARY KEY ("repository_id", "rpm_id")
);

ALTER TABLE "public"."kylin_repository_rpms"
    OWNER TO "kylin";
