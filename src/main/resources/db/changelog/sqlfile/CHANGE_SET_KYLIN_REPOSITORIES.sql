-- Create Repository table and repositoryes.id sequence for auto increment
CREATE TABLE "public"."kylin_repositories" (
                                  "id" int4 NOT NULL,
                                  "name" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "description" text COLLATE "pg_catalog"."default",
                                  "url" varchar(1024) COLLATE "pg_catalog"."default",
                                  "content_type" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "mirror_on_sync" bool NOT NULL DEFAULT false,
                                  "upstream_username" varchar(255) COLLATE "pg_catalog"."default",
                                  "upstream_password" text COLLATE "pg_catalog"."default",
                                  "last_sync_time" timestamp(6),
                                  "last_sync_status" varchar(255) COLLATE "pg_catalog"."default",
                                  "locked" bool NOT NULL DEFAULT false,
                                  "product_id" int4 NOT NULL,
                                  "user_id" int4 NOT NULL,
                                  "created_at" timestamp(6),
                                  "updated_at" timestamp(6),
                                  CONSTRAINT "repositories_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."kylin_repositories"
    OWNER TO "kylin";

CREATE UNIQUE INDEX "index_repositories_on_prod_name" ON "public"."kylin_repositories" USING btree (
     "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
     "product_id" ASC NULLS LAST
    );

CREATE SEQUENCE "public"."kylin_repositories_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

ALTER SEQUENCE "public"."kylin_repositories_id_seq"
    OWNED BY "public"."kylin_repositories"."id";

ALTER SEQUENCE "public"."kylin_repositories_id_seq" OWNER TO "kylin";

ALTER TABLE "kylin_repositories" alter column "id" set default nextval('kylin_repositories_id_seq'::regclass);

-- Default repo data
insert into kylin_repositories (name, description, url, content_type, mirror_on_sync, upstream_username,
                                upstream_password, last_sync_time, last_sync_status, locked, product_id, user_id, created_at,
                                updated_at)
select 'V7_x86_64', '中标麒麟高级服务器操作系统V7(x86_64)默认仓库', '', 'yum', false, null, null, null, null, true,
    "kylin_products".id, "users"."id", NOW(), NOW()
from users, kylin_products where users.id = kylin_products.user_id and kylin_products.label = 'V7_X86_64' order by users.id asc limit 1;

insert into kylin_repositories (name, description, url, content_type, mirror_on_sync, upstream_username,
                                upstream_password, last_sync_time, last_sync_status, locked, product_id, user_id, created_at,
                                updated_at)
select 'V7_aarch64', '中标麒麟高级服务器操作系统V7(aarch64)默认仓库', '', 'yum', false, null, null, null, null, true,
       "kylin_products".id, "users"."id", NOW(), NOW()
from users, kylin_products where users.id = kylin_products.user_id and kylin_products.label = 'V7_AARCH64' order by users.id asc limit 1;

insert into kylin_repositories (name, description, url, content_type, mirror_on_sync, upstream_username,
                                upstream_password, last_sync_time, last_sync_status, locked, product_id, user_id, created_at,
                                updated_at)
select 'V10SP1_x86_64', '银河麒麟高级服务器操作系统V10(SP1)(x86_64)默认仓库', '', 'yum', false, null, null, null, null, true,
       "kylin_products".id, "users"."id", NOW(), NOW()
from users, kylin_products where users.id = kylin_products.user_id and kylin_products.label = 'V10SP1_X86_64' order by users.id asc limit 1;

insert into kylin_repositories (name, description, url, content_type, mirror_on_sync, upstream_username,
                                upstream_password, last_sync_time, last_sync_status, locked, product_id, user_id, created_at,
                                updated_at)
select 'V10SP1_aarch64', '银河麒麟高级服务器操作系统V10(SP1)(aarch64)默认仓库', '', 'yum', false, null, null, null, null, true,
       "kylin_products".id, "users"."id", NOW(), NOW()
from users, kylin_products where users.id = kylin_products.user_id and kylin_products.label = 'V10SP1_AARCH64' order by users.id asc limit 1;
