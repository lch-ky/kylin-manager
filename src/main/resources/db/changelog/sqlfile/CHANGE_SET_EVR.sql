-- ----------------------------
-- Type structure for evr_array_item
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."rpmver_array"("string1" varchar);
DROP FUNCTION IF EXISTS "public"."isdigit"("ch" bpchar);
DROP FUNCTION IF EXISTS "public"."isalphanum"("ch" bpchar);
DROP FUNCTION IF EXISTS "public"."isalpha"("ch" bpchar);
DROP FUNCTION IF EXISTS "public"."evr_trigger"();
DROP FUNCTION IF EXISTS "public"."empty"("t" text);
DROP TYPE IF EXISTS "public"."evr_t";
DROP TYPE IF EXISTS "public"."evr_array_item";

CREATE TYPE "public"."evr_array_item" AS (
                                             "n" numeric,
                                             "s" text COLLATE "pg_catalog"."default"
                                         );
ALTER TYPE "public"."evr_array_item" OWNER TO "kylin";

-- ----------------------------
-- Type structure for evr_t
-- ----------------------------

CREATE TYPE "public"."evr_t" AS (
                                    "epoch" int4,
                                    "version" "public"."evr_array_item"[],
                                    "release" "public"."evr_array_item"[]
                                );
ALTER TYPE "public"."evr_t" OWNER TO "kylin";

-- ----------------------------
-- Function structure for empty
-- ----------------------------

CREATE OR REPLACE FUNCTION "public"."empty"("t" text)
    RETURNS "pg_catalog"."bool" AS $BODY$
BEGIN
    return t ~ '^[[:space:]]*$';
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;
ALTER FUNCTION "public"."empty"("t" text) OWNER TO "kylin";

-- ----------------------------
-- Function structure for evr_trigger
-- ----------------------------

CREATE OR REPLACE FUNCTION "public"."evr_trigger"()
    RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
    NEW.evr = (select ROW(coalesce(NEW.epoch::numeric,0),
                          rpmver_array(coalesce(NEW.version,'empty'))::evr_array_item[],
                          rpmver_array(coalesce(NEW.release,'empty'))::evr_array_item[])::evr_t);
    RETURN NEW;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;
ALTER FUNCTION "public"."evr_trigger"() OWNER TO "kylin";

-- ----------------------------
-- Function structure for isalpha
-- ----------------------------

CREATE OR REPLACE FUNCTION "public"."isalpha"("ch" bpchar)
    RETURNS "pg_catalog"."bool" AS $BODY$
BEGIN
    if ascii(ch) between ascii('a') and ascii('z') or
       ascii(ch) between ascii('A') and ascii('Z')
    then
        return TRUE;
    end if;
    return FALSE;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;
ALTER FUNCTION "public"."isalpha"("ch" bpchar) OWNER TO "kylin";

-- ----------------------------
-- Function structure for isalphanum
-- ----------------------------
CREATE OR REPLACE FUNCTION "public"."isalphanum"("ch" bpchar)
    RETURNS "pg_catalog"."bool" AS $BODY$
BEGIN
    if ascii(ch) between ascii('a') and ascii('z') or
       ascii(ch) between ascii('A') and ascii('Z') or
       ascii(ch) between ascii('0') and ascii('9')
    then
        return TRUE;
    end if;
    return FALSE;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;
ALTER FUNCTION "public"."isalphanum"("ch" bpchar) OWNER TO "kylin";

-- ----------------------------
-- Function structure for isdigit
-- ----------------------------

CREATE OR REPLACE FUNCTION "public"."isdigit"("ch" bpchar)
    RETURNS "pg_catalog"."bool" AS $BODY$
BEGIN
    if ascii(ch) between ascii('0') and ascii('9')
    then
        return TRUE;
    end if;
    return FALSE;
END ;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;
ALTER FUNCTION "public"."isdigit"("ch" bpchar) OWNER TO "kylin";

-- ----------------------------
-- Function structure for rpmver_array
-- ----------------------------

CREATE OR REPLACE FUNCTION "public"."rpmver_array"("string1" varchar)
    RETURNS "public"."_evr_array_item" AS $BODY$
declare
    str1 VARCHAR := string1;
    digits VARCHAR(10) := '0123456789';
    lc_alpha VARCHAR(27) := 'abcdefghijklmnopqrstuvwxyz';
    uc_alpha VARCHAR(27) := 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alpha VARCHAR(54) := lc_alpha || uc_alpha;
    one VARCHAR;
    isnum BOOLEAN;
    ver_array evr_array_item[] := ARRAY[]::evr_array_item[];
BEGIN
    if str1 is NULL
    then
        RAISE EXCEPTION 'VALUE_ERROR.';
    end if;

    one := str1;
    <<segment_loop>>
    while one <> ''
        loop
            declare
                segm1 VARCHAR;
                segm1_n NUMERIC := 0;
            begin
                -- Throw out all non-alphanum characters
                while one <> '' and not isalphanum(one)
                    loop
                        one := substr(one, 2);
                    end loop;
                str1 := one;
                if str1 <> '' and isdigit(str1)
                then
                    str1 := ltrim(str1, digits);
                    isnum := true;
                else
                    str1 := ltrim(str1, alpha);
                    isnum := false;
                end if;
                if str1 <> ''
                then segm1 := substr(one, 1, length(one) - length(str1));
                else segm1 := one;
                end if;

                if segm1 = '' then return ver_array; end if; /* arbitrary */
                if isnum
                then
                    segm1 := ltrim(segm1, '0');
                    if segm1 <> '' then segm1_n := segm1::numeric; end if;
                    segm1 := NULL;
                else
                end if;
                ver_array := array_append(ver_array, (segm1_n, segm1)::evr_array_item);
                one := str1;
            end;
        end loop segment_loop;

    return ver_array;
END ;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;
ALTER FUNCTION "public"."rpmver_array"("string1" varchar) OWNER TO "kylin";
