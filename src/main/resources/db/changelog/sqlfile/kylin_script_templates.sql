/*
 Navicat PostgreSQL Data Transfer

 Source Server         : 1
 Source Server Type    : PostgreSQL
 Source Server Version : 100005
 Source Host           : 172.17.127.57:5432
 Source Catalog        : kylin_manager
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100005
 File Encoding         : 65001

 Date: 03/08/2021 19:11:46
*/


-- ----------------------------
-- Table structure for kylin_script_templates
-- ----------------------------
DROP SEQUENCE if EXISTS "public"."kylin_script_templates_id_seq";
CREATE SEQUENCE "public"."kylin_script_templates_id_seq"
    INCREMENT 1
 MINVALUE 1
 MAXVALUE 2147483647
 START 1
 CACHE 1;

DROP TABLE IF EXISTS "public"."kylin_script_templates";
CREATE TABLE "public"."kylin_script_templates" (
  "id" int8 NOT NULL DEFAULT nextval('kylin_script_templates_id_seq'::regclass),
  "name" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "user_id" int4 NOT NULL,
  "content" text COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(6) NOT NULL,
  "updated_at" timestamp(6) NOT NULL,
  "display" bool NOT NULL
)
;

-- ----------------------------
-- Indexes structure for table kylin_script_templates
-- ----------------------------
CREATE UNIQUE INDEX "index_kylin_script_templates_name" ON "public"."kylin_script_templates" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "index_kylin_script_templates_on_user_id" ON "public"."kylin_script_templates" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table kylin_script_templates
-- ----------------------------
ALTER TABLE "public"."kylin_script_templates" ADD CONSTRAINT "kylin_script_templates_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table kylin_script_templates
-- ----------------------------
ALTER TABLE "public"."kylin_script_templates" ADD CONSTRAINT "kylin_script_templates_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
