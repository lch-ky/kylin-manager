/*
 Navicat Premium Data Transfer

 Source Server         : 214
 Source Server Type    : PostgreSQL
 Source Server Version : 120007
 Source Host           : 172.17.7.214:5432
 Source Catalog        : kylin_manager
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120007
 File Encoding         : 65001

 Date: 17/06/2021 15:29:08
*/


-- ----------------------------
-- Table structure for sub_logs
-- ----------------------------
DROP TABLE IF EXISTS "public"."kylin_sub_logs";
CREATE TABLE "public"."kylin_sub_logs" (
  "id" int4 NOT NULL PRIMARY KEY,
  "action" varchar(255) COLLATE "pg_catalog"."default",
  "status" varchar(255) COLLATE "pg_catalog"."default",
  "result" varchar(255) COLLATE "pg_catalog"."default",
  "module" varchar(255) COLLATE "pg_catalog"."default",
  "parent_id" int4,
  "rollback_flag" varchar(32) COLLATE "pg_catalog"."default",
  "detail" text COLLATE "pg_catalog"."default",
  "created_at" timestamp(6) NOT NULL,
  "updated_at" timestamp(6) NOT NULL,
  "user_id" int4,
  "detailshort" text COLLATE "pg_catalog"."default",
  "ip" varchar(255) COLLATE "pg_catalog"."default",
  "system" varchar(255) COLLATE "pg_catalog"."default",
  "version" varchar(255) COLLATE "pg_catalog"."default",
  "arch" varchar(255) COLLATE "pg_catalog"."default",
  "script_template_id" int4,
  "script_exection_id" int4,
  "script_content" text COLLATE "pg_catalog"."default",
  "script_ret" varchar(255) COLLATE "pg_catalog"."default",
  "script_creation_user" varchar(255) COLLATE "pg_catalog"."default"
)
;

CREATE SEQUENCE "public"."kylin_sub_logs_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;
-- ----------------------------
-- Primary Key structure for table sub_logs
-- ----------------------------

ALTER TABLE "public"."kylin_sub_logs" alter column "id" set default nextval('kylin_sub_logs_id_seq'::regclass);
