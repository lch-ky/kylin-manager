-- Create User table and users.id sequence for auto increment
CREATE TABLE "public"."kylin_products" (
                                  "id" int4 NOT NULL,
                                  "name" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "description" text COLLATE "pg_catalog"."default",
                                  "label" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "os_version" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "os_name" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "os_arch" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "address" varchar(255) COLLATE "pg_catalog"."default",
                                  "locked" bool NOT NULL DEFAULT false,
                                  "user_id" int4 NOT NULL,
                                  "created_at" timestamp(6),
                                  "updated_at" timestamp(6),
                                  CONSTRAINT "products_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."kylin_products"
    OWNER TO "kylin";

CREATE UNIQUE INDEX "index_products_on_label" ON "public"."kylin_products" USING btree (
  "label" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

CREATE SEQUENCE "public"."kylin_products_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

ALTER SEQUENCE "public"."kylin_products_id_seq"
    OWNED BY "public"."kylin_products"."id";

ALTER SEQUENCE "public"."kylin_products_id_seq" OWNER TO "kylin";

ALTER TABLE "kylin_products" alter column "id" set default nextval('kylin_products_id_seq'::regclass);

INSERT INTO "kylin_products"("name", "description", "label", "os_version", "os_name", "os_arch", "locked", "user_id", "created_at", "updated_at")
VALUES ('中标麒麟高级服务器操作系统V7(x86_64)', '中标麒麟高级服务器操作系统V7(x86_64)', 'V7_X86_64','V7.0', '中标麒麟高级服务器操作系统','x86_64',true, 1, NOW(), NOW()),
       ('中标麒麟高级服务器操作系统V7(aarch64)', '中标麒麟高级服务器操作系统V7(aarch64)', 'V7_AARCH64','V7.0', '中标麒麟高级服务器操作系统','aarch64',true, 1, NOW(), NOW()),
       ('银河麒麟高级服务器操作系统V10(SP1)(x86_64)', '银河麒麟高级服务器操作系统V10(SP1)(x86_64)', 'V10SP1_X86_64','V10(SP1)', '银河麒麟高级服务器操作系统','x86_64',true, 1, NOW(), NOW()),
       ('银河麒麟高级服务器操作系统V10(SP1)(aarch64)', '银河麒麟高级服务器操作系统V10(SP1)(aarch64)', 'V10SP1_AARCH64','V10(SP1)', '银河麒麟高级服务器操作系统','aarch64',true, 1, NOW(), NOW());