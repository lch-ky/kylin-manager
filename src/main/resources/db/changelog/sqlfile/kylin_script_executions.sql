/*
 Navicat PostgreSQL Data Transfer

 Source Server         : 1
 Source Server Type    : PostgreSQL
 Source Server Version : 100005
 Source Host           : 172.17.127.57:5432
 Source Catalog        : kylin_manager
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100005
 File Encoding         : 65001

 Date: 03/08/2021 19:11:54
*/


-- ----------------------------
-- Table structure for kylin_script_executions
-- ----------------------------
DROP SEQUENCE if EXISTS "public"."kylin_script_executions_id_seq";
CREATE SEQUENCE "public"."kylin_script_executions_id_seq"
    INCREMENT 1
 MINVALUE 1
 MAXVALUE 2147483647
 START 1
 CACHE 1;

DROP TABLE IF EXISTS "public"."kylin_script_executions";
CREATE TABLE "public"."kylin_script_executions" (
  "id" int8 NOT NULL DEFAULT nextval('kylin_script_executions_id_seq'::regclass),
  "host_id" int4 NOT NULL,
  "ip" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "command" text COLLATE "pg_catalog"."default" NOT NULL,
  "options" text COLLATE "pg_catalog"."default" NOT NULL,
  "user_id" int4,
  "effective_user" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "r_code" int4,
  "r_content" varchar COLLATE "pg_catalog"."default",
  "r_ret" int4,
  "r_stdout" text COLLATE "pg_catalog"."default",
  "r_stderr" text COLLATE "pg_catalog"."default",
  "duration" int4,
  "state" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6) NOT NULL,
  "updated_at" timestamp(6) NOT NULL,
  "script_log_id" int4
)
;

-- ----------------------------
-- Indexes structure for table kylin_script_executions
-- ----------------------------
CREATE INDEX "index_kylin_script_executions_on_host_id" ON "public"."kylin_script_executions" USING btree (
  "host_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "index_kylin_script_executions_on_state" ON "public"."kylin_script_executions" USING btree (
  "state" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "index_kylin_script_executions_on_user_id" ON "public"."kylin_script_executions" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table kylin_script_executions
-- ----------------------------
ALTER TABLE "public"."kylin_script_executions" ADD CONSTRAINT "kylin_script_executions_pkey" PRIMARY KEY ("id");
