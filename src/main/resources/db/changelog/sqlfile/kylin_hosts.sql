/*
 Navicat PostgreSQL Data Transfer

 Source Server         : 192.168.56.116
 Source Server Type    : PostgreSQL
 Source Server Version : 120005
 Source Host           : 192.168.56.116:5432
 Source Catalog        : kylin_manager
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120005
 File Encoding         : 65001

 Date: 04/06/2021 15:35:33
*/


-- ----------------------------
-- Table structure for kylin_hosts
-- ----------------------------
DROP TABLE IF EXISTS "public"."kylin_hosts";
CREATE TABLE "public"."kylin_hosts" (
  "id" int4 NOT NULL PRIMARY KEY,
  "ip" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "operating_system" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "version" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "architecture" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "agent_status" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "agent_version" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "agent_ca" text COLLATE "pg_catalog"."default" NOT NULL,
  "agent_token" text COLLATE "pg_catalog"."default" NOT NULL,
  "distribution" varchar(1000) COLLATE "pg_catalog"."default",
  "install_env" varchar(100) COLLATE "pg_catalog"."default",
  "install_groups" text COLLATE "pg_catalog"."default",
  "bandwidth_limit"  varchar COLLATE "pg_catalog"."default",
  "migrate_lock" varchar(255) COLLATE "pg_catalog"."default",
  "install_time" TIMESTAMPTZ,
  "created_at" TIMESTAMPTZ,
  "updated_at" TIMESTAMPTZ,
  "reserved1" text COLLATE "pg_catalog"."default",
  "reserved2" text COLLATE "pg_catalog"."default"
)
;

CREATE SEQUENCE "public"."kylin_hosts_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;
-- ----------------------------
-- Primary Key structure for table kylin_hosts
-- ----------------------------
ALTER TABLE "public"."kylin_hosts" alter column "id" set default nextval('kylin_hosts_id_seq'::regclass);
