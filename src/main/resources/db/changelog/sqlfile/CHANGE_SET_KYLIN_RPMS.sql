-- Create RPM table and rpms.id sequence for auto increment
CREATE TABLE "public"."kylin_rpms" (
                                  "id" int4 NOT NULL,
                                  "created_at" timestamp(6),
                                  "updated_at" timestamp(6),
                                  "name" varchar(255) COLLATE "pg_catalog"."default",
                                  "version" varchar(255) COLLATE "pg_catalog"."default",
                                  "release" varchar(255) COLLATE "pg_catalog"."default",
                                  "arch" varchar(255) COLLATE "pg_catalog"."default",
                                  "epoch" varchar(255) COLLATE "pg_catalog"."default",
                                  "filename" varchar(255) COLLATE "pg_catalog"."default",
                                  "sourcerpm" varchar(255) COLLATE "pg_catalog"."default",
                                  "sha256" varchar(255) COLLATE "pg_catalog"."default",
                                  "version_sortable" varchar(255) COLLATE "pg_catalog"."default",
                                  "release_sortable" varchar(255) COLLATE "pg_catalog"."default",
                                  "summary" varchar(255) COLLATE "pg_catalog"."default",
                                  "nvrea" varchar(1020) COLLATE "pg_catalog"."default",
                                  "evr" "public"."evr_t",
                                  CONSTRAINT "rpms_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."kylin_rpms"
    OWNER TO "kylin";


CREATE INDEX "index_rpms_on_name_and_arch_and_evr" ON "public"."kylin_rpms" USING btree (
                   "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
                   "arch" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
                   "evr" "pg_catalog"."record_ops" ASC NULLS LAST
    );

CREATE UNIQUE INDEX "index_rpms_on_nvrea_checksum" ON "public"."kylin_rpms" USING btree (
                "sha256" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
    );

CREATE INDEX "rpms_name_index" ON "public"."kylin_rpms" USING btree (
                 "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
    );

CREATE TRIGGER "evr_insert_trigger_rpms" BEFORE INSERT ON "public"."kylin_rpms"
    FOR EACH ROW
EXECUTE PROCEDURE "public"."evr_trigger"();

CREATE TRIGGER "evr_update_trigger_rpms" BEFORE UPDATE OF "epoch", "version", "release" ON "public"."kylin_rpms"
    FOR EACH ROW
    WHEN ((((old.epoch)::text IS DISTINCT FROM (new.epoch)::text) OR ((old.version)::text IS DISTINCT FROM (new.version)::text) OR ((old.release)::text IS DISTINCT FROM (new.release)::text)))
EXECUTE PROCEDURE "public"."evr_trigger"();

CREATE SEQUENCE "public"."kylin_rpms_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

ALTER SEQUENCE "public"."kylin_rpms_id_seq"
    OWNED BY "public"."kylin_rpms"."id";

ALTER SEQUENCE "public"."kylin_rpms_id_seq" OWNER TO "kylin";

ALTER TABLE "kylin_rpms" alter column "id" set default nextval('kylin_rpms_id_seq'::regclass);

-- Create RPM-attrs table and rpms.id sequence for auto increment
CREATE TABLE "public"."kylin_rpm_attrs" (
                                       "id" int4 NOT NULL,
                                       "created_at" timestamp(6),
                                       "updated_at" timestamp(6),
                                       "rpm_id" int4 NOT NULL,
                                       "attr" varchar(255) COLLATE "pg_catalog"."default",
                                       "content" text COLLATE "pg_catalog"."default",
                                       CONSTRAINT "rpm_attrs_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."kylin_rpm_attrs"
    OWNER TO "kylin";

CREATE SEQUENCE "public"."kylin_rpm_attrs_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

ALTER SEQUENCE "public"."kylin_rpm_attrs_id_seq"
    OWNED BY "public"."kylin_rpm_attrs"."id";

ALTER SEQUENCE "public"."kylin_rpm_attrs_id_seq" OWNER TO "kylin";

ALTER TABLE "kylin_rpm_attrs" alter column "id" set default nextval('kylin_rpm_attrs_id_seq'::regclass);

-- Create RPM-deps table and rpms.id sequence for auto increment
CREATE TABLE "public"."kylin_rpm_deps" (
                                            "id" int4 NOT NULL,
                                            "created_at" timestamp(6),
                                            "updated_at" timestamp(6),
                                            "rpm_id" int4 NOT NULL,
                                            "type" varchar(255) COLLATE "pg_catalog"."default",
                                            "name" varchar(511) COLLATE "pg_catalog"."default",
                                            "flags" int4 ,
                                            "version" varchar(255) COLLATE "pg_catalog"."default",
                                            "epoch" varchar(255) COLLATE "pg_catalog"."default",
                                            "release" varchar(255) COLLATE "pg_catalog"."default",
                                            CONSTRAINT "rpm_deps_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."kylin_rpm_deps"
    OWNER TO "kylin";

CREATE SEQUENCE "public"."kylin_rpm_deps_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;

ALTER SEQUENCE "public"."kylin_rpm_deps_id_seq"
    OWNED BY "public"."kylin_rpm_deps"."id";

ALTER SEQUENCE "public"."kylin_rpm_deps_id_seq" OWNER TO "kylin";

ALTER TABLE "kylin_rpm_deps" alter column "id" set default nextval('kylin_rpm_deps_id_seq'::regclass);