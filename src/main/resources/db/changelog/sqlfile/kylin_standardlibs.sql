/*
 Navicat Premium Data Transfer

 Source Server         : foreman
 Source Server Type    : PostgreSQL
 Source Server Version : 120007
 Source Host           : 192.168.153.8:5432
 Source Catalog        : kylin_manager
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120007
 File Encoding         : 65001

 Date: 23/06/2021 14:42:23
*/


-- ----------------------------
-- Table structure for starandlibs
-- ----------------------------
DROP TABLE IF EXISTS "public"."kylin_standardlibs";
CREATE TABLE "public"."kylin_standardlibs" (
 "id" int8 NOT NULL PRIMARY KEY,
 "name" varchar COLLATE "pg_catalog"."default",
 "version" varchar COLLATE "pg_catalog"."default",
 "cpu_arch" varchar COLLATE "pg_catalog"."default",
 "reserved1" varchar COLLATE "pg_catalog"."default",
 "reserved2" text COLLATE "pg_catalog"."default",
 "created_at" timestamp(6) NOT NULL,
 "updated_at" timestamp(6) NOT NULL
)
;

CREATE SEQUENCE "public"."kylin_standardlibs_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;
-- ----------------------------
-- Primary Key structure for table parent_logs
-- ----------------------------

ALTER TABLE "public"."kylin_standardlibs" alter column "id" set default nextval('kylin_standardlibs_id_seq'::regclass);
