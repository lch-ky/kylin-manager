/*
 Navicat Premium Data Transfer

 Source Server         : foreman
 Source Server Type    : PostgreSQL
 Source Server Version : 120007
 Source Host           : 192.168.153.8:5432
 Source Catalog        : kylin_manager
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120007
 File Encoding         : 65001

 Date: 24/06/2021 16:34:58
*/


-- ----------------------------
-- Table structure for kylin_configs
-- ----------------------------
DROP TABLE IF EXISTS "public"."kylin_configs";
CREATE TABLE "public"."kylin_configs" (
 "id" int4 NOT NULL PRIMARY KEY,
 "name" varchar COLLATE "pg_catalog"."default" NOT NULL,
 "config_type" varchar COLLATE "pg_catalog"."default" NOT NULL,
 "attribute" varchar COLLATE "pg_catalog"."default",
 "baseline" text COLLATE "pg_catalog"."default",
 "description" text COLLATE "pg_catalog"."default",
 "flag" varchar COLLATE "pg_catalog"."default",
 "file_path" varchar COLLATE "pg_catalog"."default",
 "reserved1" varchar COLLATE "pg_catalog"."default",
 "reserved2" varchar COLLATE "pg_catalog"."default",
 "created_at" timestamp(6) NOT NULL,
 "updated_at" timestamp(6) NOT NULL,
 "standard_id" int4,
 "standard_name" varchar COLLATE "pg_catalog"."default" NOT NULL
)
;

CREATE SEQUENCE "public"."kylin_configs_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;

-- ----------------------------
-- Primary Key structure for table kylin_configs
-- ----------------------------

ALTER TABLE "public"."kylin_configs" alter column "id" set default nextval('kylin_configs_id_seq'::regclass);