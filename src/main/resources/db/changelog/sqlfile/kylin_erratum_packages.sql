/*
 Navicat Premium Data Transfer

 Source Server         : 172.17.127.151_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 120001
 Source Host           : 172.17.127.151:5432
 Source Catalog        : foreman
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120001
 File Encoding         : 65001

 Date: 23/07/2021 15:04:57
*/


-- ----------------------------
-- Table structure for kylin_erratum_packages
-- ----------------------------
DROP TABLE IF EXISTS "public"."katello_erratum_packages";
CREATE TABLE "public"."kylin_erratum_packages" (
  "id" int4 NOT NULL  PRIMARY KEY,
  "cve_id" int4 NOT NULL,
  "nvrea" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "filename" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Indexes structure for table kylin_erratum_packages
-- ----------------------------
CREATE INDEX "index_erratum_packages_on_name" ON "public"."kylin_erratum_packages" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "index_erratum_packages_on_nvrea" ON "public"."kylin_erratum_packages" USING btree (
  "nvrea" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

CREATE UNIQUE INDEX "kylin_erratum_packages_eid_nvrea_n_f" ON "public"."kylin_erratum_packages" USING btree (
  "nvrea" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "filename" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table kylin_erratum_packages
-- ----------------------------
CREATE SEQUENCE "public"."kylin_erratum_packages_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;

ALTER TABLE "public"."kylin_erratum_packages" alter column "id" set default nextval('kylin_erratum_packages_id_seq'::regclass);
