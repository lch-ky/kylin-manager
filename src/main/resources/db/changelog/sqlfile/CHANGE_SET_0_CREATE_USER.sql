-- Create User table and users.id sequence for auto increment
CREATE TABLE "public"."users" (
                                  "id" int4 NOT NULL,
                                  "username" varchar(255) NOT NULL COLLATE "pg_catalog"."default",
                                  "firstname" varchar(255) COLLATE "pg_catalog"."default",
                                  "lastname" varchar(255) COLLATE "pg_catalog"."default",
                                  "mail" varchar(255) COLLATE "pg_catalog"."default",
                                  "password_hash" varchar(128) NOT NULL COLLATE "pg_catalog"."default",
                                  "password_salt" varchar(128) NOT NULL COLLATE "pg_catalog"."default",
                                  "locale" varchar(10) COLLATE "pg_catalog"."default",
                                  "active" bool NOT NULL DEFAULT true,
                                  "last_login_on" timestamp(6),
                                  "created_at" timestamp(6),
                                  "updated_at" timestamp(6),
                                  "comment" text COLLATE "pg_catalog"."default",
                                  CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

ALTER TABLE "public"."users"
    OWNER TO "kylin";

CREATE UNIQUE INDEX "index_users_on_username" ON "public"."users" USING btree (
  "username" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

CREATE SEQUENCE "public"."users_id_seq"
    INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

ALTER SEQUENCE "public"."users_id_seq"
    OWNED BY "public"."users"."id";

ALTER SEQUENCE "public"."users_id_seq" OWNER TO "kylin";

ALTER TABLE "users" alter column "id" set default nextval('users_id_seq'::regclass);