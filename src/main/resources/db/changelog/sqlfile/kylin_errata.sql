/*
 Navicat Premium Data Transfer

 Source Server         : cmmi
 Source Server Type    : PostgreSQL
 Source Server Version : 120001
 Source Host           : 192.168.153.5:5432
 Source Catalog        : foreman
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120001
 File Encoding         : 65001

 Date: 12/07/2021 17:49:53
*/


-- ----------------------------
-- Table structure for kylin_errata
-- ----------------------------
DROP TABLE IF EXISTS "public"."kylin_errata";
CREATE TABLE "public"."kylin_errata" (
  "id" int4 NOT NULL PRIMARY KEY,
  "errata_id" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "issued_date" date,
  "updated_date" date,
  "errata_type" varchar(255) COLLATE "pg_catalog"."default",
  "severity" varchar(255) COLLATE "pg_catalog"."default",
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "solution" text COLLATE "pg_catalog"."default",
  "description" text COLLATE "pg_catalog"."default",
  "summary" text COLLATE "pg_catalog"."default",
  "reboot_suggested" bool
)
;




-- ----------------------------
-- Indexes structure for table kylin_errata
-- ----------------------------
CREATE INDEX "index_kylin_errata_on_issued" ON "public"."kylin_errata" USING btree (
  "issued_date" "pg_catalog"."date_ops" ASC NULLS LAST
);

CREATE INDEX "index_kylin_errata_on_updated" ON "public"."kylin_errata" USING btree (
  "updated_date" "pg_catalog"."date_ops" ASC NULLS LAST
);


CREATE SEQUENCE "public"."kylin_errata_id_seq"
    INCREMENT 1
    MINVALUE  1
    MAXVALUE 2147483647
    START 1
    CACHE 1;
-- ----------------------------
-- Primary Key structure for table kylin_errata
-- ----------------------------
ALTER TABLE "public"."kylin_errata" alter column "id" set default nextval('kylin_errata_id_seq'::regclass);