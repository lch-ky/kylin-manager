mkdir -p /opt/kylin-manager/etc
mkdir -p /opt/kylin-manager/heartbeat/
echo -n {0} > /opt/kylin-manager/etc/client.ip
echo -n {1} > /opt/kylin-manager/heartbeat/host.txt
mountpath=/tmp/mnt
echo $mountpath
mkdir -p $mountpath
cd /tmp
mount -o loop client-lastest.iso $mountpath
cd $mountpath
sh install.sh
/opt/miniconda/bin/python /opt/kylin-manager/usr/bin/kylin-agent/py_agent/gen_ca.py
umount $mountpath
rm -rf $mountpath
rm -f /tmp/client-lastest.iso;
systemctl restart kylin-agent

