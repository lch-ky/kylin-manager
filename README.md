# KylinSystemUnifiedManagerApplication

## 研发环境搭建

### 搭建准备
- OS：V7/V10SP1
- 安装Java11：yum install java-11-openjdk java-11-openjdk-devel 
- 安装Postgresql，推荐12.x版本（可借用foreman环境）：yum install postgresql
- 安装Redis，推荐5.x版本（可借用foreman环境）:yum install redis
- 安装Maven，推荐3.5.x版本：yum install maven，如果yum安装的Maven版本低于3.1.0或者仓库没有Maven，参考下面的免安装方式配置

### 配置Maven中央仓库镜像
**不配置下载速度太慢**
- 打开Maven配置文件，在Maven安装目录下面的`conf/settings.xml`
- 在`<mirrors>`节点下面添加以下内容
  
```xml
<mirror>
    <id>aliyunmaven</id>
    <mirrorOf>central</mirrorOf>
    <name>阿里云公共仓库</name>
    <url>https://maven.aliyun.com/repository/public</url>
</mirror>
```
此处配置的为阿里云的中央仓库镜像，其他镜像的配置方式大同小异。
- 保存后可用`mvn install`拉取依赖包
  
### 数据库配置
- 创建数据库`kylin_manager`
- 创建数据库用户`kylin`，密码`qwer1234!@#$`
- 向kylin用户赋予kylin_manager数据库权限

执行以下SQL，可用命令行执行`sudo -u postgres psql -f init.sql`
```sql
SELECT 'CREATE DATABASE kylin_manager'
        WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'kylin_manager')\gexec
DO
$do$
BEGIN
   IF NOT EXISTS ( SELECT FROM pg_roles
                   WHERE  rolname = 'kylin') THEN
        CREATE USER kylin WITH ENCRYPTED PASSWORD 'qwer1234!@#$';
        GRANT ALL PRIVILEGES ON DATABASE kylin_manager TO kylin;
        GRANT ALL PRIVILEGES ON DATABASE kylin_manager TO postgres;
   END IF;
END
$do$;
```

### 命令行搭建
- 下载源码，切换develop分支
- 进入项目根目录, `mvn spring-boot:run -Dspring.profiles.active=dev -Ddebug -Dfile.encoding=UTF-8`

### IDEA搭建
- 下载源码，切换develop分支
- 用IDEA打开项目
- `Ctrl+N`/`Command+O`呼出类搜索框，搜索`cn.kylinos.kylinmanager.KylinManagerApplication`。运行这个类的main方法即可。

### Maven免安装方式
- 已经安装Java 1.7+版本
- 下载二进制发布包，[下载地址](https://mirrors.bfsu.edu.cn/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz)
- 解压缩到固定目录，例如/opt: `tar -zxf apache-maven-3.6.3-bin.tar.gz -C /opt `
- 配置环境变量，编辑~/.bash_profile，在PATH定义中添加Maven的bin目录，例如：PATH=/opt/apache-maven-3.6.3-bin/bin:$PATH:$HOME/bin
- 配置环境变量，编辑~/.bash_profile，增加MAVEN_HOME环境变量：MAVEN_HOME=/opt/apache-maven-3.6.3-bin    export MAVEN_HOME
- 执行环境变量脚本：source ~/.bash_profile
- 终端中使用mvn -v确认Maven版本

### 查看API的方式
- http://host:8080/swagger-ui/index.html?url=/v3/api-docs

```text
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /opt/apache-maven-3.6.3
Java version: 11.0.6, vendor: Oracle Corporation, runtime: /usr/lib/jvm/java-11-openjdk-11.0.6.10-4.ky10.ky10.aarch64
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "4.19.90-21.2.ky10.aarch64", arch: "aarch64", family: "unix"
```
